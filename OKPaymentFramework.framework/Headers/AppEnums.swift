//
//  AppEnums.swift
//  OKPaymentFramework
//
//  Created by Ashish on 11/19/18.
//  Copyright © 2018 Ashish. All rights reserved.
//

import UIKit

 public enum URLType {
    case production, testing
}

 public enum ApplicationType {
    case okTaxi, okEcommerce
}

 public enum PaymentType {
    case bonusPoint, okWallet
}
