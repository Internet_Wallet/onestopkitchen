/*
 See LICENSE folder for this sample’s licensing information.
 
 Abstract:
 WatermarkPage is a PDFPage subclass that implements custom drawing.
 */

import Foundation
import PDFKit

/**
 WatermarkPage subclasses PDFPage so that it can override the draw(with box: to context:) method.
 This method is called by PDFDocument to draw the page into a PDFView. All custom drawing for a PDF
 page should be done through this mechanism.
 
 Custom drawing methods should always be thread-safe and call the super-class method. This is needed
 to draw the original PDFPage content. Custom drawing code can execute before or after this super-class
 call, though order matters! If your graphics run before the super-class call, they are drawn below the
 PDFPage content. Conversely, if your graphics run after the super-class call, they are drawn above the
 PDFPage.
 */
@available(iOS 11.0, *)
class WatermarkPage: PDFPage {
    
    // 3. Override PDFPage custom draw
    /// - Tag: OverrideDraw
    override func draw(with box: PDFDisplayBox, to context: CGContext) {
        
        // Draw original content
        super.draw(with: box, to: context)
        
        // Draw rotated overlay string
        UIGraphicsPushContext(context)
        context.saveGState()
        
        let pageBounds = self.bounds(for: box)
        context.translateBy(x: 0.0, y: pageBounds.size.height)
        context.scaleBy(x: 1.0, y: -1.0)
        context.rotate(by: CGFloat.pi / 4.0)
        
        let string: NSString = "1 Stop Mart   1 Stop Mart"
        let attributes = [
            NSAttributedString.Key.foregroundColor: UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5),
            NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 40)
        ]
        
        string.draw(at: CGPoint(x:120, y:20), withAttributes: attributes)
        
        context.restoreGState()
        UIGraphicsPopContext()
        
    }
}
