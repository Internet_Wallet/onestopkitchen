//
//  RopeColorVC.swift
//  VMart
//
//  Created by Shobhit Singhal on 3/14/19.
//  Copyright © 2019 Shobhit. All rights reserved.
//

import UIKit

protocol RopeColorDelegate {
    func updateRopeColor(data:String)
}

class RopeColorVC: UIViewController {
    @IBOutlet weak var ropeColorTableView: UITableView!
    @IBOutlet weak var ropeBGView: UIView!
    
    let colorArray = ["Black", "Red", "Blue", "Green", "Yellow"]
    var delegate:RopeColorDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ropeBGView.layer.borderWidth = 1
        ropeBGView.layer.borderColor = UIColor.gray.cgColor
    }
    
    @IBAction func dismissTapAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension RopeColorVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40.0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return colorArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
        cell.textLabel?.font = UIFont(name: appFont, size: 15.0)!
        cell.textLabel?.text = colorArray[indexPath.row].localized
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.updateRopeColor(data: colorArray[indexPath.row])
        self.dismiss(animated: true, completion: nil)
    }
}
