//
//  AppDelegate.swift
//  VMart
//
//  Created by Shobhit Singhal on 10/15/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit
import Reachability
import CoreData
import CoreTelephony
import UserNotifications
import SwiftyJSON
import MobileRTC
import Firebase
import IQKeyboardManagerSwift

//import PGW

import Quickblox
import QuickbloxWebRTC
import SVProgressHUD

struct CredentialsConstant {
    static let applicationID:UInt = 4 //78528
    static let authKey = "AquOguJUMPBEMMk"//"aMhnd9AwENBacEd"
    static let authSecret = "mGvE8NQVKpfh2ms"//"nvhxgwfPVNfUeZH"
    static let accountKey = "LA-ECHmoSCFVh33NYpTh"//"-hqwbWxxYC9sd11XneN6"
    static let apiEndPoint = "https://apiokfintech.quickblox.com"
    static let chatEndPoint = "chatokfintech.quickblox.com"
}

struct TimeIntervalConstant {
    static let answerTimeInterval: TimeInterval = 60.0
    static let dialingTimeInterval: TimeInterval = 5.0
}

struct AppDelegateConstant {
    static let enableStatsReports: UInt = 1
}
//


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate , MobileRTCAuthDelegate {
    // Quickblox
    lazy private var backgroundTask: UIBackgroundTaskIdentifier = {
        let backgroundTask = UIBackgroundTaskIdentifier.invalid
        return backgroundTask
    }()
    
    
    var isCalling = false {
        didSet {
            if UIApplication.shared.applicationState == .background,
                isCalling == false {
                disconnect()
            }
        }
    }
    
    //Not used...Removed now
    var SmsBankMobileNumber : String? = "+959252077892"
    
    var window: UIWindow?
    var scheme: String!
    var currentLanguage = ""
    var currentFont = ""
    var localBundle: Bundle?
    let networkInfo = CTTelephonyNetworkInfo()
    let gcmMessageIDKey = "gcm.message_id"
    var FCMToken = ""
    
    //    let kSDKAppKey = "zBRr8IzHpbavdPBhtYcCZ1oEOIxnZehncoFy"
    //    let kSDKAppSecret = "uvABRY6CKlK0OUj2HSuLaSJmYo6dWieo0idi"
    //    let kSDKDomain = "zoom.us"
    
    
    
    /// Saved shortcut item used as a result of an app launch, used later when app is activated.
    var launchedShortcutItem: UIApplicationShortcutItem?
    
    enum ShortcutIdentifier: String {
        
        case Dynamic
        
        // MARK: Initializers
        init?(fullNameForType: String) {
            guard let last = fullNameForType.components(separatedBy: ".").last else { return nil }
            
            self.init(rawValue: last)
        }
        
        // MARK: Properties
        var type: String {
            return Bundle.main.bundleIdentifier! + ".\(self.rawValue)"
        }
    }
    
    public var availableSIM: Bool {
        return (CTTelephonyNetworkInfo().subscriberCellularProvider?.mobileNetworkCode == nil ) ? false : true // sim detection
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysHide
        self.configureQuickBlox()
        self.setInitialCurrentLang()
        self.notifySimChange()
        self.getCurrentSimChangeStatus()
        _ = TownshipManager.shared
        _ = VMGeoLocationManager.shared
        
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        //        MobileRTC.shared()?.setMobileRTCDomain(kSDKDomain)
        //        // Step 2: Get Auth Service
        //        let authService = MobileRTC.shared()?.getAuthService()
        //        print("MobileRTC Version: \(String(describing: MobileRTC.shared()?.mobileRTCVersion()))")
        //        if ((authService) != nil) {
        //            // Step 3: Setup Auth Service
        //            authService?.delegate        = self;
        //            authService?.clientKey       = kSDKAppKey;
        //            authService?.clientSecret    = kSDKAppSecret;
        //            authService?.sdkAuth()
        //        }
        self.createShortcutItemsWithIcons()
        self.shortcutMenuActions(item: launchOptions?[UIApplication.LaunchOptionsKey.shortcutItem])
        return true
    }
    
    
    
    // basic configure quickblox
    func configureQuickBlox(){
        QBSettings.applicationID = CredentialsConstant.applicationID;
        QBSettings.authKey = CredentialsConstant.authKey
        QBSettings.authSecret = CredentialsConstant.authSecret
        QBSettings.accountKey = CredentialsConstant.accountKey
        QBSettings.apiEndpoint = CredentialsConstant.apiEndPoint
        QBSettings.chatEndpoint = CredentialsConstant.chatEndPoint
        // enabling carbons for chat
        QBSettings.carbonsEnabled = true
        QBSettings.autoReconnectEnabled = true
        QBSettings.logLevel = QBLogLevel.debug
        QBSettings.enableXMPPLogging()
        
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        
        QBRTCConfig.setAnswerTimeInterval(TimeIntervalConstant.answerTimeInterval)
        QBRTCConfig.setDialingTimeInterval(TimeIntervalConstant.dialingTimeInterval)
        QBRTCConfig.setLogLevel(QBRTCLogLevel.verbose)
        
        if AppDelegateConstant.enableStatsReports == 1 {
            QBRTCConfig.setStatsReportTimeInterval(1.0)
        }
        
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.clear)
        QBRTCClient.initializeRTC()
    }
    
    private func setInitialCurrentLang() {
        if UserDefaults.standard.value(forKey: "currentLanguage") == nil {
            currentLanguage = "my"
            appLang = "Myanmar"
            UserDefaults.standard.set("my", forKey: "currentLanguage")
            UserDefaults.standard.set("Zawgyi-One", forKey: "appFont")
            UserDefaults.standard.synchronize()
            
            self.currentFont = (UserDefaults.standard.object(forKey: "appFont") as? String)!
            appFont = self.currentFont
            if let path = Bundle.main.path(forResource: "my", ofType: "lproj") {
                localBundle = Bundle(path: path)
            }
        }else if let lang = UserDefaults.standard.string(forKey: "currentLanguage"), lang == "en"{
            UserDefaults.standard.set("Zawgyi-One", forKey: "appFont")
            UserDefaults.standard.set("en", forKey: "currentLanguage")
            self.currentFont = (UserDefaults.standard.object(forKey: "appFont") as? String)!
            appFont = self.currentFont
            appLang = "English"
            if let path = Bundle.main.path(forResource: "en", ofType: "lproj") {
                localBundle = Bundle(path: path)
            }
        }else if let lang = UserDefaults.standard.string(forKey: "currentLanguage"), lang == "my"{
            UserDefaults.standard.set("Zawgyi-One", forKey: "appFont")
            UserDefaults.standard.set("my", forKey: "currentLanguage")
            self.currentFont = (UserDefaults.standard.object(forKey: "appFont") as? String)!
            appFont = self.currentFont
            appLang = "Myanmar"
            if let path = Bundle.main.path(forResource: "my", ofType: "lproj") {
                localBundle = Bundle(path: path)
            }
        }else {
            if let lang = UserDefaults.standard.string(forKey: "currentLanguage"), let path =  Bundle.main.path(forResource: lang, ofType: "lproj") {
                currentLanguage = lang
                UserDefaults.standard.set("uni", forKey: "currentLanguage")
                UserDefaults.standard.set("Myanmar3", forKey: "appFont")
                // UserDefaults.standard.synchronize()
                self.currentFont = (UserDefaults.standard.object(forKey: "appFont") as? String)!
                appFont = self.currentFont
                appLang = "Unicode"
                localBundle = Bundle(path: path)
                if let path = Bundle.main.path(forResource: "uni", ofType: "lproj") {
                    localBundle = Bundle(path: path)
                }
            }
        }
    }
    
    //MARK: CHECK SIM IN PHONE
    
    func notifySimChange() {
        networkInfo.subscriberCellularProviderDidUpdateNotifier = { carrier in
            self.getCurrentSimChangeStatus()
        }
    }
    
    func onMobileRTCAuthReturn(_ returnValue: MobileRTCAuthError) {
        print("onMobileRTCAuthReturn \(returnValue)")
        if (returnValue != MobileRTCAuthError_Success) {
            let errorMessage = "SDK authentication failed, error code: \(returnValue)"
            print(errorMessage)
        }
    }
    func getCurrentSimChangeStatus() {
        let (currentNetworkCode, currentCarrierName) = getNetworkCode()
        let device = UIDevice()
        if device.screenType == .iPhone_XSMax {
            if currentNetworkCode == "" && currentCarrierName == "" {
                deleteAllObjects()
                simChangeNotification()
                return
            }
        }else {
            if currentNetworkCode == "" {
                deleteAllObjects()
                simChangeNotification()
                return
            }
        }
        let previousNetWorkCode = UserDefaults.standard.value(forKey: "MobileNetworkCode") as? String ?? ""
        let previousCarrierName = UserDefaults.standard.value(forKey: "MobileCarrierName") as? String ?? ""
        if currentCarrierName != "" && currentNetworkCode != "" && previousNetWorkCode != "" && previousCarrierName != "" {
            if currentNetworkCode != previousNetWorkCode && currentCarrierName != previousCarrierName
            {
                deleteAllObjects()
                simChangeNotification()
                UIApplication.shared.applicationIconBadgeNumber = 0
                UserDefaults.standard.set(0, forKey: "ReceivedCount")
            }
        }
    }
    
    func deleteAllObjects(){
        UserDefaults.standard.set(false, forKey: "REGITRATION_OPENED")
        let entitesByName = self.persistentContainer.managedObjectModel.entitiesByName
        for (name, _) in entitesByName {
            deleteAllObjectsForEntity(entity: name)
        }
    }
    
    func deleteAllObjectsForEntity(entity: String){
        let managedContext  =  self.persistentContainer.viewContext
        let fetchReq = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchReq)
        do {
            try managedContext.execute(deleteRequest)
            try managedContext.save()
        } catch {
        }
    }
    
    func simChangeNotification() {
        DispatchQueue.main.async {
            UserDefaults.standard.set(false, forKey: "LoginStatus")
            let content = UNMutableNotificationContent()
            content.title = NSString.localizedUserNotificationString(forKey: "Notification", arguments: nil)
            content.body = NSString.localizedUserNotificationString(forKey: "You have changed the sim card in your device or ejected".localized, arguments: nil)
            content.sound = UNNotificationSound.default
            content.categoryIdentifier = "simChangeaAtionCategory"
            let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 0.5, repeats: false)
            let request = UNNotificationRequest.init(identifier: "notify-test", content: content, trigger: trigger)
            let center = UNUserNotificationCenter.current()
            center.add(request)
        }
    }
    
    
    func checkSIMAvailability() {
        let device = UIDevice()
        let (currentNetworkCode, currentCarrierName) = getNetworkCode()
        if device.screenType == .iPhone_XSMax {
            DispatchQueue.main.asyncAfter(deadline: .now() + 5.0 ) {
                if currentNetworkCode == "" && currentCarrierName == ""{
                    UIView.animate(withDuration: 1.0, animations: {
                        DispatchQueue.main.async {
                            UserDefaults.standard.set(false, forKey: "LoginStatus")
                        }
                    }, completion: { (status) in
                        self.deleteAllObjects()
                        self.simChangeNotification()
                    })
                }
            }
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 5.0 ) {
                if currentNetworkCode == "" {
                    UIView.animate(withDuration: 1.0, animations: {
                        DispatchQueue.main.async {
                            UserDefaults.standard.set(false, forKey: "LoginStatus")
                        }
                    }, completion: { (status) in
                        self.deleteAllObjects()
                        self.simChangeNotification()
                    })
                }
            }
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        application.applicationIconBadgeNumber = 0
        /*
         // Logging out from chat.
         ChatManager.instance.disconnect()
         // Logging out from chat.
         if isCalling == false {
         disconnect()
         }
         */
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Logging in to chat.
        registerForRemoteNotifications()
        /*
         ChatManager.instance.connect { (error) in
         if let error = error {
         SVProgressHUD.showError(withStatus: error.localizedDescription)
         return
         }
         }
         
         // Logging in to chat.
         if QBChat.instance.isConnected == true {
         return
         }
         connect { (error) in
         if let error = error {
         SVProgressHUD.showError(withStatus: error.localizedDescription)
         return
         }
         SVProgressHUD.showSuccess(withStatus: "Connected")
         }
         */
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        self.checkSIMAvailability()
        guard let shortcutItem = launchedShortcutItem else { return }
        _ = handleShortcutItem(item: shortcutItem)
        launchedShortcutItem = nil
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        /*
         ChatManager.instance.disconnect()
         // Logging out from chat.
         disconnect()
         */
    }
    
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any)-> Bool {
        scheme = url.scheme
        let urlComponents = NSURLComponents(url: url, resolvingAgainstBaseURL: false)
        if let items = urlComponents?.queryItems as [NSURLQueryItem]? {
            if scheme == "VMART" {
                if let item1 = items.first, let mobNo = item1.value {
                    UserDefaults.standard.set(mobNo, forKey: "OKRegisteredNum")
                }
                if let item2 = items.last, let uuid = item2.value {
                    UserDefaults.standard.set(uuid, forKey: "OKRegisteredDeviceId")
                }
                if !UserDefaults.standard.bool(forKey: "LoginStatus") {
                    checkOKDollarLogin()
                }
            }
        }
        return true
    }
    
    
    func monthCode(month: String) -> String {
        if month.localizedLowercase.contains(find: "jan") || month.localizedLowercase == "jan" {
            return "1"
        }else if month.localizedLowercase.contains(find: "feb") || month.localizedLowercase == "feb" {
             return "2"
        }else if month.localizedLowercase.contains(find: "mar") || month.localizedLowercase == "mar" {
             return "3"
        }else if month.localizedLowercase.contains(find: "apr") || month.localizedLowercase == "apr"{
             return "4"
        }else if month.localizedLowercase.contains(find: "may") || month.localizedLowercase == "may"{
             return "5"
        }else if month.localizedLowercase.contains(find: "jun") || month.localizedLowercase == "jun"{
             return "6"
        }else if month.localizedLowercase.contains(find: "jul") || month.localizedLowercase == "jul"{
             return "7"
        }else if month.localizedLowercase.contains(find: "aug") || month.localizedLowercase == "aug"{
             return "8"
        }else if month.localizedLowercase.contains(find: "sep") || month.localizedLowercase == "sep"{
             return "9"
        }else if month.localizedLowercase.contains(find: "oct") || month.localizedLowercase == "oct"{
             return "10"
        }else if month.localizedLowercase.contains(find: "nov") || month.localizedLowercase == "nov"{
             return "11"
        }else if month.localizedLowercase.contains(find: "dec") || month.localizedLowercase == "dec"{
             return "12"
        }else {
            return ""
        }
    }
    
    
    private func checkOKDollarLogin() {
        let aPIManager = APIManager()
        //hit getProfile api
        if let uCode = UserDefaults.standard.value(forKey: "OKRegisteredNum") as? String, let uuid = UserDefaults.standard.value(forKey: "OKRegisteredDeviceId") as? String {
            let urlStr   = String.init(format: "https://www.okdollar.co/RestService.svc/RetrieveProfile?mobilenumber=%@&simid=%@", uCode,uuid)
            if let ur = URL(string: urlStr) {
                let params = Dictionary<String,String>()
                aPIManager.genericClass(url: ur, param: params as AnyObject, httpMethod: "GET", handle: { (response, success, _) in
                    if success {
                        DispatchQueue.main.async {
                            if let dataDict = response as? Dictionary<String,AnyObject> {
                                if let dic = dataDict["Data"] as? String {
                                    guard let profile = AppUtility.convertToDictionary(text: dic) else { return }
                                    if let profileInfo = profile["ProfileDetails"] as? [Dictionary<String,Any>] {
                                        println_debug(profileInfo.first)
                                        if let details = profileInfo.first {
                                            println_debug(details)
                                            RegistrationModel.share.ProfilePictureUrl = details["ProfilePic"] as? String ?? ""
                                            RegistrationModel.share.MobileNumber = details["Mobilenumber"] as? String ?? ""
                                            RegistrationModel.share.Username = details["Mobilenumber"] as? String ?? ""
                                            let st = details["State"] as? String ?? ""
                                            let town = details["Township"] as? String ?? ""
                                            let (state,township,city) = self.getStateTownship(statecode: st, townshipCode: town)
                                            RegistrationModel.share.Address1 = township
                                            RegistrationModel.share.Address2 = state
                                            RegistrationModel.share.City = city
                                            RegistrationModel.share.State = state
                                            RegistrationModel.share.Country = details["Country"] as? String ?? ""
                                            
                                            let DOB = details["DOB"] as? String ?? ""
                                            if DOB != "" {
                                                let date = DOB.components(separatedBy: "-")
                                                RegistrationModel.share.DateOfBirthDay = date[0]
                                                RegistrationModel.share.DateofBirthMonth =  self.monthCode(month: date[1])
                                                RegistrationModel.share.DateOfBirthYear = date[2]
                                            }
                                            
                                            let name =  details["Name"] as? String ?? ""
                                            let fName = name.components(separatedBy: ",")
                                            RegistrationModel.share.Firstname = fName[1]
                                            RegistrationModel.share.Lastname = fName[1]
                                            RegistrationModel.share.Email = details["EmailId"] as? String ?? ""
                                            let gender = details["Gender"] as? Int ?? 0
                                            
                                            if gender == 1 {
                                                RegistrationModel.share.Gender = "M"
                                            }else {
                                                RegistrationModel.share.Gender = "F"
                                            }
                                            
                                            let (n_Code, n_name) = appDelegate.getNetworkCode()
                                            RegistrationModel.share.Msid1 = n_Code
                                            RegistrationModel.share.NetworkID = n_Code
                                            RegistrationModel.share.NetworkOperator = n_Code
                                            RegistrationModel.share.NetworkOperatorName = n_name
                                            RegistrationModel.share.NetworkSignal = n_Code
                                            RegistrationModel.share.SIMOperator = n_Code
                                            RegistrationModel.share.SIMOperatorName = n_name
                                            
                                            if let ipAddress = AppUtility.getIPAddress() {
                                                RegistrationModel.share.IPAddress = ipAddress
                                            }
                                            RegistrationModel.share.Latitude = VMGeoLocationManager.shared.currentLatitude
                                            RegistrationModel.share.Longitude = VMGeoLocationManager.shared.currentLongitude
                                            RegistrationModel.share.DeviceInfo = RegistrationModel.share.wrapDeviceInfoData()
                                            var paramDic = [String: Any]()
                                            paramDic = RegistrationModel.share.wrapData()
                                            
                                            let urlString = String(format: "%@/customer/registerorlogin", APIManagerClient.sharedInstance.base_url)
                                            guard let url = URL(string: urlString) else { return }
                                            let params = AppUtility.JSONStringFromAnyObject(value: paramDic as AnyObject)
                                            
                                            aPIManager.genericClass(url: url, param: params as AnyObject, httpMethod: "POST", header: true) { (response, success, _) in
                                                if success {
                                                    let json = JSON(response)
                                                    println_debug(json)
                                                    if let code = json["StatusCode"].int, code == 200 {
                                                        UserDefaults.standard.set(false, forKey: "GuestLogin")
                                                        UserDefaults.standard.set(true, forKey: "RegistrationDone")
                                                        UserDefaults.standard.synchronize()
                                                        if let loginInfoDic  = json["Data"].dictionaryObject {
                                                            let vmTemoModel = VMTempModel()
                                                            vmTemoModel.wrapDataModel(JSON: loginInfoDic)
                                                        }
                                                    }else {
                                                    }
                                                }else {
                                                    println_debug("Registration Failed")
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                })
            }
        }
    }
    
    private func getStateTownship(statecode: String, townshipCode : String)-> (String,String,String) {
        var stateName = ""
        var townshipName = ""
        var cityName = ""
        let states = TownshipManager.allLocationsList
        for state in states {
            if state.stateOrDivitionCode == statecode {
                stateName = state.stateOrDivitionNameEn
                let township = state.townshipArryForAddress
                for town in township {
                    if town.townShipCode == townshipCode {
                        townshipName = town.townShipNameEN
                        
                        if town.GroupName == "" {
                            cityName = town.cityNameEN
                        }else {
                            if town.isDefaultCity == "1" {
                                cityName = town.DefaultCityNameEN
                            }else {
                                cityName = town.cityNameEN
                            }
                        }
                        break
                    }
                }
            }
        }
        return (stateName,townshipName,cityName)
    }
    
    
    // MARK: - Core Data stack
    @available(iOS 10.0, *)
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "OneStopMart")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func setSeletedlocaLizationLanguage(language : String) {
        currentLanguage = language
        if let path = Bundle.main.path(forResource: currentLanguage, ofType: "lproj") {
            localBundle = Bundle(path: path)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "stringLocalised"), object: nil)
        }
        
        if currentLanguage == "my"{
            UserDefaults.standard.set("Zawgyi-One", forKey: "appFont")
            self.currentFont = (UserDefaults.standard.object(forKey: "appFont") as? String)!
            appFont = self.currentFont
            appLang = "Myanmar"
        }else  if currentLanguage == "en"{
            UserDefaults.standard.set("Zawgyi-One", forKey: "appFont")
            // UserDefaults.standard.synchronize()
            self.currentFont = (UserDefaults.standard.object(forKey: "appFont") as? String)!
            appFont = self.currentFont
            appLang = "English"
        }else {
            UserDefaults.standard.set("Myanmar3", forKey: "appFont")
            self.currentFont = (UserDefaults.standard.object(forKey: "appFont") as? String)!
            appFont = self.currentFont
            appLang = "Unicode"
        }
    }
    
    
    func getStringAccordingToLanguage(language : String, key: String) -> String {
        if let path = Bundle.main.path(forResource: language, ofType: "lproj") {
            let bundle  = Bundle(path: path)
            return NSLocalizedString(key, tableName: "Localization", bundle: bundle!, value: "", comment: "")
        }
        return key
    }
    
    func getSelectedLanguage() -> String {
        return currentLanguage
    }
    
    func getlocaLizationLanguage(key : String) -> String {
        if let bundl = localBundle {
            return NSLocalizedString(key, tableName: "Localization", bundle: bundl, value: "", comment: "")
        } else {
            return key
        }
    }
    
    
    func getNetworkCode() -> (String, String) {
        let networkInfo = CTTelephonyNetworkInfo.init()
        let carrier     = networkInfo.subscriberCellularProvider
        let networkCode = carrier?.mobileNetworkCode ?? ""
        let carrierName = carrier?.carrierName ?? ""
        return (networkCode, carrierName)
    }
    
    
    //MARK: - Connect/Disconnect
    func connect(completion: QBChatCompletionBlock? = nil) {
        let currentUser = Profile()
        
        guard currentUser.isFull == true else {
            completion?(NSError(domain: LoginConstant.chatServiceDomain,
                                code: LoginConstant.errorDomaimCode,
                                userInfo: [
                                    NSLocalizedDescriptionKey: "Please enter your login and username."
            ]))
            return
        }
        if QBChat.instance.isConnected == true {
            completion?(nil)
        } else {
            QBSettings.autoReconnectEnabled = true
            QBChat.instance.connect(withUserID: currentUser.ID, password: currentUser.password, completion: completion)
        }
    }
    
    func disconnect(completion: QBChatCompletionBlock? = nil) {
        QBChat.instance.disconnect(completionBlock: completion)
    }
    
    
    private func registerForRemoteNotifications() {
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.sound, .alert, .badge], completionHandler: { granted, error in
            if let error = error {
                debugPrint("[AppDelegate] requestAuthorization error: \(error.localizedDescription)")
                return
            }
            center.getNotificationSettings(completionHandler: { settings in
                if settings.authorizationStatus != .authorized {
                    return
                }
                DispatchQueue.main.async(execute: {
                    UIApplication.shared.registerForRemoteNotifications()
                })
            })
        })
    }
    /*
     func checkAppUpgrade(urlString: String) {
     if let okDollarUrl = URL(string: urlString) {
     if UIApplication.shared.canOpenURL(okDollarUrl) {
     UIApplication.shared.open(okDollarUrl, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:])
     , completionHandler: nil)
     } else {
     self.navigateToTaxiAppStoreLink()
     }
     
     UserDefaults.standard.set(currentVersion, forKey: "VersionOfLastRun")
     UserDefaults.standard.synchronize()
     }
     }
     */
    
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        println_debug("=======> Did receive")
        if let aps = response.notification.request.content.userInfo["aps"] as? [String: Any] {
            println_debug("=======>\(aps)")
        }
        
        // QuickBlox
        let userInfo = response.notification.request.content.userInfo
        if UIApplication.shared.applicationState == .active {
            return
        }
        
        center.removeAllDeliveredNotifications()
        center.removeAllPendingNotificationRequests()
        
        guard let dialogID = userInfo["SA_STR_PUSH_NOTIFICATION_DIALOG_ID".localized] as? String,
            dialogID.isEmpty == false else {
                return
        }
        // calling dispatch async for push notification handling to have priority in main queue
        DispatchQueue.main.async {
            if let chatDialog = ChatManager.instance.storage.dialog(withID: dialogID) {
                self.openChat(chatDialog)
            } else {
                ChatManager.instance.loadDialog(withID: dialogID, completion: { (loadedDialog: QBChatDialog?) -> Void in
                    guard let dialog = loadedDialog else {
                        return
                    }
                    self.openChat(dialog)
                })
            }
        }
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        println_debug("=======> willPresent")
        if let aps = notification.request.content.userInfo["aps"] as? [String: Any] {
            // self.notificationHandle(userInfo: aps)
            println_debug("=======>\(aps)")
            // self.checkCICONotificationCases(userInfo: aps, actionIdentifier: nil)
        }
        completionHandler([.alert, .badge, .sound])
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    
    // the FCM registration token.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
        var deviceTokenStr = ""
        print("%@",deviceTokenStr)
        let tokenData = deviceToken as NSData
        var token1 = tokenData.description
        
        // remove any characters once you have token string if needed
        token1 = token1.replacingOccurrences(of: " ", with: "")
        token1 = token1.replacingOccurrences(of: "<", with: "")
        token1 = token1.replacingOccurrences(of: ">", with: "")
        token1 = token1.uppercased()
        print("\nToken1 : \(token1)\n")
        deviceTokenStr = token1
        
        // ...register device token with our Time Entry API server via REST
        Messaging.messaging().apnsToken = deviceToken
        // Quickblox chat
        guard let identifierForVendor = UIDevice.current.identifierForVendor else {
            return
        }
        
        let deviceIdentifier = identifierForVendor.uuidString
        let subscription = QBMSubscription()
        subscription.notificationChannel = .APNS
        subscription.deviceUDID = deviceIdentifier
        subscription.deviceToken = deviceToken
        QBRequest.createSubscription(subscription, successBlock: { response, objects in
        }, errorBlock: { response in
            debugPrint("[AppDelegate] createSubscription error: \(String(describing: response.error))")
        })
        //
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        print(userInfo)
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        print(userInfo)
    }
    
    
    
    //MARK: Help
    func openChat(_ chatDialog: QBChatDialog) {
        guard let window = window,
            let navigationController = window.rootViewController as? UINavigationController else {
                return
        }
        var controllers = [UIViewController]()
        for controller in navigationController.viewControllers {
            controllers.append(controller)
            if controller is DialogsViewController {
                let storyboard = UIStoryboard(name: "Chat", bundle: nil)
                let chatController = storyboard.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
                chatController.dialogID = chatDialog.id
                controllers.append(chatController)
                navigationController.setViewControllers(controllers, animated: true)
                return
            }
        }
    }
    
    
    
    
    func openCart() {
        guard let window = window,
            let navigationController = window.rootViewController as? UINavigationController else {
                return
        }
        var controllers = [UIViewController]()
        for controller in navigationController.viewControllers {
            controllers.append(controller)
            if controller is Cart {
                let HomeCatStoryboard =  UIStoryboard(name: "Main", bundle: nil)
                if let HomeCatController = HomeCatStoryboard.instantiateViewController(withIdentifier: "HomeCategories_ID") as? HomeCategories {
                    let mainViewController   = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "CartRoot")
                    let drawerController     = KYDrawerController(drawerDirection: .left, drawerWidth: 280)
                    drawerController.mainViewController = UINavigationController(rootViewController: mainViewController)
                    drawerController.drawerViewController = HomeCatController
                    controllers.append(drawerController)
                    navigationController.setViewControllers(controllers, animated: true)
                    return
                }
            }
        }
    }
}

extension AppDelegate : MessagingDelegate {
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        self.FCMToken = fcmToken
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        let aPIManager = APIManager()
               aPIManager.registerDeviceId(self.FCMToken,onSuccess:{
               }, onError: { message in
          })
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
}


extension AppDelegate{
    
    func handleShortcutItem(item: UIApplicationShortcutItem) -> Bool {
        var handled = false
        // Verify that the provided shortcutItem's type is one handled by the application.
        guard ShortcutIdentifier(fullNameForType: item.type) != nil else { return false }
        guard let shortCutType = item.type as String? else { return false }
        
        switch shortCutType {
        case ShortcutIdentifier.Dynamic.type:
            handled = true
        default:
            print("Shortcut Item Handle func")
        }
        return handled
        
    }
    
    func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
        self.shortcutMenuActions(item: shortcutItem)
        DispatchQueue.main.asyncAfter(deadline: (.now() + 1.0)) {
            NotificationCenter.default.post(name: Notification.Name("ShortCutMenuAction"), object: nil, userInfo: nil)
        }
        
    }
    
    func getTopMostViewController() -> UIViewController? {
        var topMostViewController = UIApplication.shared.keyWindow?.rootViewController
        while let presentedViewController = topMostViewController?.presentedViewController {
            topMostViewController = presentedViewController
        }
        return topMostViewController
    }
    
    // MARK: Dynamic Shortcut Items (dynamic)
    func createShortcutItemsWithIcons() {
        
        // create some icons with my own images
        let icon1 = UIApplicationShortcutIcon.init(templateImageName: "iCon1")
        let icon2 = UIApplicationShortcutIcon.init(templateImageName: "iCon2")
        let icon3 = UIApplicationShortcutIcon.init(templateImageName: "iCon3")
        let icon4 = UIApplicationShortcutIcon.init(templateImageName: "dashboard_facePay")
        
        
        // create dynamic shortcut items
        let item1 = UIMutableApplicationShortcutItem.init(type: "com.cgm.Search", localizedTitle: "Search", localizedSubtitle: "", icon: icon1, userInfo: nil)
        let item2 = UIMutableApplicationShortcutItem.init(type: "com.cgm.Barcode", localizedTitle: "Barcode", localizedSubtitle: "", icon: icon2, userInfo: nil)
        let item3 = UIMutableApplicationShortcutItem.init(type: "com.cgm.Wishlist", localizedTitle: "Wishlist", localizedSubtitle: "", icon: icon3, userInfo: nil)
        let item4 = UIMutableApplicationShortcutItem.init(type: "com.cgm.Cart", localizedTitle: "Cart", localizedSubtitle: "", icon: icon4, userInfo: nil)
        
        // add all items to an array
        let items = [item1, item2, item3, item4] as Array
        UIApplication.shared.shortcutItems = items
    }
    
    
    
    
    private func shortcutMenuActions(item: Any?) {
        // Shortcut menu functions
        if item != nil {
            if (item as AnyObject).type == "com.cgm.Search" {
                UserDefaults.standard.setValue("com.cgm.Search", forKey: "UIApplicationLaunchOptionsKey.shortcutItem")
                UserDefaults.standard.synchronize()
            } else if (item as AnyObject).type == "com.cgm.Barcode" {
                UserDefaults.standard.setValue("com.cgm.Barcode", forKey: "UIApplicationLaunchOptionsKey.shortcutItem")
                UserDefaults.standard.synchronize()
            } else if (item as AnyObject).type == "com.cgm.Wishlist" {
                UserDefaults.standard.setValue("com.cgm.Wishlist", forKey: "UIApplicationLaunchOptionsKey.shortcutItem")
                UserDefaults.standard.synchronize()
            } else if (item as AnyObject).type == "com.cgm.ScanQR" {
                UserDefaults.standard.setValue("com.cgm.ScanQR", forKey: "UIApplicationLaunchOptionsKey.shortcutItem")
                UserDefaults.standard.synchronize()
            } else if (item as AnyObject).type == "com.cgm.Cart" {
                UserDefaults.standard.setValue("com.cgm.Cart", forKey: "UIApplicationLaunchOptionsKey.shortcutItem")
                UserDefaults.standard.synchronize()
            }
        } else {
            println_debug("We've launched properly.")
        }
    }
    
}
