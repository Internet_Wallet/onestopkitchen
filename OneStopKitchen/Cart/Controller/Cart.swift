//
//  Cart.swift
//  VMart
//
//  Created by ANTONY on 26/10/2018.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit

class Cart: MartBaseViewController {
    
    var dictionaryArray =               NSMutableArray ()
    var aPIManager = APIManager()
    var shoppingCartInfoArray =         [ShoppingCartInfo]()
    var checkOutGuestflag = false
    var flag = Bool ()
    var textFieldDic =  [Int:AnyObject]()
    var selectedIndex = IndexPath()
    var allBillingAddresses = [GenericBillingAddress]()
    var newBillingAddress: GenericBillingAddress?
    var defaultSelectedAddress: GenericBillingAddress?
    static var PickupAddress: Store?
    var warningCheckArr = [Bool]()
    var block1: DispatchWorkItem?
    var block2: DispatchWorkItem?
    var ticalFromService = "1"
     var senderTag = 0
    var productAvailbilityCheck : [[String : Any]] = [[String : Any]]()
    
    @IBOutlet var cartTbl: UITableView!
    @IBOutlet weak var CartDataView: UIView!
    @IBOutlet weak var CartDataNotFoundView: UIView!
    @IBOutlet var cartBarButton: UIBarButtonItem!
    
    
    @IBOutlet var infoLbl: UILabel!{
        didSet {
            self.infoLbl.font = UIFont(name: appFont, size: 15.0)
            self.infoLbl.text = self.infoLbl.text?.localized
            self.infoLbl.textColor = UIColor.black
            
        }
    }
    
    @IBOutlet weak var moveToPaymentBtn: UIButton! {
        didSet {
            self.moveToPaymentBtn.setTitle("Continue".localized, for: .normal)
            moveToPaymentBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet weak var continueBtn: UIButton! {
        didSet {
            continueBtn.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1), UIColor.init(red: 20.0/255.0, green: 185.0/255.0, blue: 85.0/255.0, alpha: 1.0)])
            self.continueBtn.setTitle("Continue Shopping".localized, for: .normal)
            self.continueBtn.layer.cornerRadius = 4
            self.continueBtn.layer.masksToBounds = true
            continueBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    func custom(){
        
        let HomeCatStoryboard =  UIStoryboard(name: "Main", bundle: nil)
        if let HomeCatController = HomeCatStoryboard.instantiateViewController(withIdentifier: "HomeCategories_ID") as? HomeCategories {
            let mainViewController   = self
            let drawerController     = KYDrawerController(drawerDirection: .left, drawerWidth: 280)
            drawerController.mainViewController = UINavigationController(rootViewController: mainViewController)
            
            if let Nav = drawerController.mainViewController as? UINavigationController{
                Nav.navigationBar.applyNavigationGradient(colors: [UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1), UIColor.init(red: 20.0/255.0, green: 185.0/255.0, blue: 85.0/255.0, alpha: 1.0)])
                
            }
            
            drawerController.drawerViewController = HomeCatController
            //UIApplication.shared.keyWindow?.rootViewController = drawerController
            
            
            self.setUPNav(controller: drawerController)
            
        }
        
    }
    
    
    func setUPNav(controller: UIViewController){
        guard let window = UIApplication.shared.keyWindow else {
            return
        }
        
        
        // Set the new rootViewController of the window.
        // Calling "UIView.transition" below will animate the swap.
        window.rootViewController = controller
        
        // A mask of options indicating how you want to perform the animations.
        let options: UIView.AnimationOptions = .transitionCrossDissolve
        
        // The duration of the transition animation, measured in seconds.
        let duration: TimeInterval = 1.0
        
        // Creates a transition animation.
        // Though `animations` is optional, the documentation tells us that it must not be nil. ¯\_(ツ)_/¯
        UIView.transition(with: window, duration: duration, options: options, animations: {}, completion:
            { completed in
                // maybe do something on completion here
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UserDefaults.standard.set(0, forKey: "shippingaddress")
        UserDefaults.standard.synchronize()
        flag = false
        if AppUtility.isConnectedToNetwork() {
            self.dictionaryArray = []
            self.getShoppingCartDetails()
        } else {
            AppUtility.showInternetErrorToast(AppConstants.InternetErrorText.message, view: self.view)
        }
        println_debug(UserDefaults.standard.bool(forKey: "LoginStatus"))
        NotificationCenter.default.addObserver(self, selector: #selector(reloadAllAddress), name: NSNotification.Name("ReloadAddressAfterAdd"), object: nil)
        
    }
    
    
    @objc func reloadAllAddress() {
        DispatchQueue.main.async {
            self.newBillingAddress = nil
            self.allBillingAddresses.removeAll()
            self.getBillForm()
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                let indexpath = IndexPath(item: 0, section: 0)
                self.cartTbl.reloadRows(at: [indexpath], with: .none)
            }
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "ReloadAddressAfterAdd"), object: nil)
        }}
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "My Cart".localized
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white,NSAttributedString.Key.font: UIFont(name: appFont, size: 17)!]
         getBillForm()
         DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
             self.cartTbl.reloadData()
         }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    private func getBillForm() {
        AppUtility.showLoading(self.view)
        self.aPIManager.loadBillingAddressForm( onSuccess: {[weak self] billingAddress in
            if let viewLoc = self?.view {
                AppUtility.hideLoading(viewLoc)
            }
            if let newBillingAddress = billingAddress.newBillingAddress {
                self?.newBillingAddress = newBillingAddress
                self?.allBillingAddresses.append(contentsOf: billingAddress.existingBillingAddresses)
                self?.allBillingAddresses.append(newBillingAddress)
            }
            if let allBillAddr = self?.allBillingAddresses {
                for billingArrdess in allBillAddr {
                    if billingArrdess.id == 0 {
                        
                    } else {
                        let title = "\(billingArrdess.firstName ?? "") \(billingArrdess.lastName ?? ""), \(billingArrdess.address1 ?? ""), \(billingArrdess.stateProvinceName ?? "") \(billingArrdess.zipPostalCode ?? ""), \(billingArrdess.cityName ?? ""), \(billingArrdess.countryName ?? "")"
                        println_debug(title)
                    }
                }
                if allBillAddr.count > 0 {
                    VMLoginModel.shared.addressAdded = true
                }
            }
            self?.defaultSelectedAddress = self?.allBillingAddresses.first ?? nil
            }, onError: { [weak self] message in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                }
        })
    }
    
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func proceedToLogin(_ notification: Notification) {
        
    }
    
    @objc func proceedToRegister(_ notification: Notification) {
        
    }
    
    @objc func proceedToCheckOut(_ notification: Notification) {
        AppUtility.showLoading(self.view)
        let finalDicArray = NSMutableArray ()
        for key in self.textFieldDic.keys {
            let parameters:Dictionary<String,AnyObject> = ["value": (self.textFieldDic[key] ?? "" as AnyObject), //this line will crash the app
                "key":String(format: "checkout_attribute_%d",self.shoppingCartInfoArray[0].checkoutAttributes[key].Id) as AnyObject]
            finalDicArray.add(parameters)
        }
        finalDicArray.addObjects(from: self.dictionaryArray as [AnyObject])
        aPIManager.loadProceedToCheckout(parameters: finalDicArray, onSuccess:{ [weak self] in
            if let viewLoc = self?.view {
                AppUtility.hideLoading(viewLoc)
            }
            //            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            //           let checkOutView = mainStoryBoard.instantiateViewController(withIdentifier: "CheckOutViewController") as! CheckOutViewController
            //            centerControllersObject.pushNewController(controller: checkOutView)
            }, onError: { [weak self]
                message in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    AppUtility.showToast(message,view: viewLoc)
                }
        })
    }
    
    func getShoppingCartDetails() {
        AppUtility.showLoading(self.view)
        self.aPIManager.loadShoppingCart(onSuccess:{ [weak self] getProducts in
            self?.shoppingCartInfoArray = getProducts
            if self?.shoppingCartInfoArray[0].Items.count == 0 {
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                }
                self?.CartDataNotFoundView.isHidden = false
                self?.CartDataView.isHidden = true
                return
            } else {
                self?.CartDataNotFoundView.isHidden = true
                self?.CartDataView.isHidden = false
            }
            //                self.customNavBarView.updateShoppingCartCount()
            if !(self?.shoppingCartInfoArray[0].GiftCardBox_Display ??  false) {
                //                    self.hideGiftCardBox()
            }
            self?.flag = true
            //                self.contentTableViewHeightConstant.constant = CGFloat(self.shoppingCartInfoArray[0].Items.count)  * 150.0
            //                self.totalAttributsViewHeight = 0
            for i in 0  ..< (self?.shoppingCartInfoArray[0].checkoutAttributes.count)! {
                if let productAttributes = self?.shoppingCartInfoArray[0].checkoutAttributes[i] {
                    if productAttributes.AttributeControlType == 1 {
                        //                        self.totalAttributsViewHeight = self.totalAttributsViewHeight + 65
                    } else if productAttributes.AttributeControlType == 2 {
                        //                        self.totalAttributsViewHeight = self.totalAttributsViewHeight + 85
                    } else if productAttributes.AttributeControlType == 3 {
                        //                        self.totalAttributsViewHeight = self.totalAttributsViewHeight + 41 + 44 * productAttributes.Values.count
                    } else if productAttributes.AttributeControlType == 4 {
                        //                        self.totalAttributsViewHeight = self.totalAttributsViewHeight + 65
                        //                        self.textFieldStringRowIndex = i
                        //self.textFieldDic[i] = self.shoppingCartInfoArray[0].checkoutAttributes[i].DefaultValue as AnyObject
                    }
                }
            }
            //                self.attributsTableViewHeightConstant.constant = CGFloat(self.totalAttributsViewHeight)
            if self?.shoppingCartInfoArray[0].checkoutAttributes.count == 0 {
                //                    self.attributsTableViewHeightConstant.constant = 0
                //                    self.attributeTableContainerBottomConstraint.constant = 0
                //                    self.attributeTableTopConstraint.constant = 0
                //                    self.attributeTableBottomConstraint.constant = 0
                //                    self.attributeTableContainer.isHidden = true
            }
            //            UserDefaults.standard.setValue(String(self?.shoppingCartInfoArray[0].Count ?? 0), forKey: "badge")
            if self?.shoppingCartInfoArray[0].Count == 0{
                UserDefaults.standard.set("", forKey: "badge")
            }
            else{
                UserDefaults.standard.set(self?.shoppingCartInfoArray[0].Count, forKey: "badge")
            }
            UserDefaults.standard.synchronize()
            
            DispatchQueue.main.async {
                
                
                UIView.animate(withDuration: 0.4, animations: {
                    self?.navigationItem.rightBarButtonItem?.customView?.transform = CGAffineTransform(scaleX: 1.4, y: 1.4) }, completion: { (finish: Bool) in
                        UIView.animate(withDuration: 0.4, animations: {
                            self?.navigationItem.rightBarButtonItem?.customView?.transform = CGAffineTransform.identity
                        }, completion: ({finished in
                            //                            self?.cartBarButton = UIBarButtonItem.init(badge: UserDefaults.standard.value(forKey: "badge") as? String, title: "", target: self, action: #selector(self?.cartAction(_:)))
                            self?.cartBarButton = UIBarButtonItem.init(badge: UserDefaults.standard.string(forKey: "badge") ?? "", title: "", target: self, action: #selector(self?.cartAction(_:)))
                            self?.navigationItem.rightBarButtonItem  = self?.cartBarButton
                        }))
                })
            }
            self?.checkOutForGuest()
            }, onError: { [weak self] message in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    AppUtility.showToast(message,view: viewLoc)
                }
        })
    }
    
    func checkOutForGuest() {
        self.aPIManager.checkOutForGuest( onSuccess:{ [weak self] checkOutGuest in
            self?.checkOutGuestflag = checkOutGuest
            DispatchQueue.main.async {
                //                self?.continueButtonState()
                self?.warningCheckArr = Array(repeating: false, count: self!.shoppingCartInfoArray[0].Items.count)
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    
                }
                self?.cartTbl.delegate = self
                self?.cartTbl.dataSource = self
                self?.cartTbl.reloadData()
            }
            if APIManagerClient.sharedInstance.shoppingCartCount == 0 {
                if let viewLoc = self?.view {
                    AppUtility.showToast("Cart is Empty".localized,view: viewLoc)
                }
                //                    centerControllersObject.popTopViewController()
            } else {
                //                    self.containerScrollView.isHidden = false;
            }
            
            //                self.contentTableView.reloadData()
            //                self.attributesTableView.reloadData()
            //                self.roundRectToAllView(self.attributeTableContainer)
            //                self.roundRectToAllView(self.discountView)
            //                self.roundRectToAllView(self.giftCardView)
            //                self.roundRectToAllView(self.costView)
            }, onError: { [weak self] message in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    AppUtility.showToast(message,view: viewLoc)
                }
        })
    }
    
    func continueButtonState(index: IndexPath){
        //        if ((self.shoppingCartInfoArray[0].OrderTotalResponse?.value(forKey: "MinOrderTotalAmount").safelyWrappingString())!.replacingOccurrences(of: "MMK", with: "")) > (self.shoppingCartInfoArray[0].orderTotal.safelyWrappingString().replacingOccurrences(of: "MMK", with: "").replacingOccurrences(of: ",", with: "")){
        //            self.moveToPaymentBtn.isEnabled = false
        //            self.moveToPaymentBtn.alpha = 0.5
        //            AppUtility.showToastlocal(message: "Increase Your Order in this location".localized, view: self.view)
        //        }
        if let dataValue =  self.shoppingCartInfoArray[0].Items[index.row] as? CheckOutItems{
            if (dataValue.Warnings?.count)! > 0{
                self.moveToPaymentBtn.isEnabled = false
                self.moveToPaymentBtn.backgroundColor = UIColor.darkGray
                self.moveToPaymentBtn.alpha = 0.5
            }
            else{
                self.moveToPaymentBtn.isEnabled = true
                self.moveToPaymentBtn.backgroundColor = UIColor(red: 13.0/255.0, green: 177.0/255.0, blue: 75.0/255.0, alpha: 1.0)
                self.moveToPaymentBtn.alpha = 1.0
            }
        }
        
    }
    
    @objc func deleteBtnAction(_ sender:UIButton!) {
        print(sender.tag)
        
        senderTag = sender.tag
        AppUtility.showLoading(self.view)
        let parameters:Dictionary<String,AnyObject> = ["value": self.shoppingCartInfoArray[0].Items[sender.tag].Id as AnyObject,"key":"removefromcart" as AnyObject]
        let dicArray = NSMutableArray ()
        dicArray.add(parameters)
        
        // changes avaneesh
        DispatchQueue.main.async {
            let finalDicArray = NSMutableArray ()
            let parameters:Dictionary<String,AnyObject> = ["value": self.shoppingCartInfoArray[0].Items[sender.tag].Id as AnyObject, "key":"addtocart_1.EnteredQuantity" as AnyObject]
            finalDicArray.add(parameters)
            finalDicArray.addObjects(from: self.dictionaryArray as [AnyObject])
            AppUtility.showLoading(self.view)
            let tical = self.shoppingCartInfoArray[0].Items[sender.tag].AttributeInfo.components(separatedBy: ":")
            var ticalvalue = ""
            if tical.count>0{
                if tical.indices.contains(1){
                    ticalvalue = tical[1].replacingOccurrences(of: " ", with: "")
                }
            }
            self.aPIManager.loadAddProductToCart(2, productId: self.shoppingCartInfoArray[0].Items[sender.tag].ProductId, tical: ticalvalue, parameters: finalDicArray, onSuccess:{ [weak self] priceValue in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    let parameters:Dictionary<String,AnyObject> = ["value": self?.shoppingCartInfoArray[0].Items[sender.tag].Id as AnyObject,
                                                                   "key":"removefromcart" as AnyObject]
                    let dicArray = NSMutableArray ()
                    dicArray.add(parameters)
                    self?.removeProduct(dicArray: dicArray)
                    //  AppUtility.showToastlocal(message: "Product moved to wishlist".localized, view: viewLoc)
                    //AppUtility.showToastCustomBlackInView(message: "Successfully added to wish list".localized,controller: controller)
                }
                }, onError: { [weak self] message in
                    if let viewLoc = self?.view {
                        AppUtility.hideLoading(viewLoc)
                        AppUtility.showToastlocal(message: message ?? "", view: viewLoc)
                    }
            })
        }
        /***************************************************/
        //self.removeProduct(dicArray: dicArray)
    }
    
    func removeProduct(dicArray: NSMutableArray) {
        var ticalvalue = ""
        if self.shoppingCartInfoArray[0].Items.indices.contains(senderTag){
            let tical = self.shoppingCartInfoArray[0].Items[senderTag].AttributeInfo.components(separatedBy: ":")
                              
                              if tical.count>0{
                                  if tical.indices.contains(1){
                                      ticalvalue = tical[1].replacingOccurrences(of: " ", with: "")
                                  }
                              }
        }
       
        
        aPIManager.DeleteAndUpdate(ticalValue: ticalvalue, dicArray, onSuccess: { [weak self] updateCart in
            self?.shoppingCartInfoArray = updateCart
            
            if let ShopingCartInfo = self?.shoppingCartInfoArray as? [ShoppingCartInfo] ,  ShopingCartInfo.count > 0 {
                //                UserDefaults.standard.setValue(String(self?.shoppingCartInfoArray[0].Count ?? 0), forKey: "badge")
            
                
                
                if self?.shoppingCartInfoArray[0].Count == 0{
                    UserDefaults.standard.set("", forKey: "badge")
                }
                else{
                    UserDefaults.standard.set(self?.shoppingCartInfoArray[0].Count, forKey: "badge")
                }
                UserDefaults.standard.synchronize()
                DispatchQueue.main.async {
                    
                    
                    UIView.animate(withDuration: 0.4, animations: {
                        self?.navigationItem.rightBarButtonItem?.customView?.transform = CGAffineTransform(scaleX: 1.4, y: 1.4) }, completion: { (finish: Bool) in
                            UIView.animate(withDuration: 0.4, animations: { [weak self] in
                                self?.navigationItem.rightBarButtonItem?.customView?.transform = CGAffineTransform.identity
                            }, completion: ( { [weak self] finished in
                                //                                self!.navigationItem.rightBarButtonItem = UIBarButtonItem.init(badge: UserDefaults.standard.value(forKey: "badge") as? String, title: "", target: self, action: #selector(self?.cartAction(_:)))
                                DispatchQueue.main.async {
                                    self?.navigationItem.rightBarButtonItem = UIBarButtonItem.init(badge: UserDefaults.standard.string(forKey: "badge") ?? "", title: "", target: self, action: #selector(self?.cartAction(_:)))
                                }
                            }))
                    })
                    
                }
                
            }
            if let viewLoc = self?.view {
                AppUtility.hideLoading(viewLoc)
            }
            if self?.shoppingCartInfoArray[0].Items.count == 0 {
                self?.CartDataNotFoundView.isHidden = false
                self?.CartDataView.isHidden = true
                return
            } else {
                self?.CartDataNotFoundView.isHidden = true
                self?.CartDataView.isHidden = false
            }
            if self?.shoppingCartInfoArray[0].Items.count == 0 {
                //                centerControllersObject.popTopViewController()
            }
            self?.cartTbl.reloadData()
            }, onError: { [weak self] message in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    AppUtility.showToast(message,view: viewLoc)
                }
        })
    }
    
    @objc func plusBtnAction(_ sender:UIButton!) {
        let indexBtn   = IndexPath(row: sender.tag, section: 0)
        //        let cell        = self.contentTableView.cellForRow(at: indexPath) as! ItemsTableViewCell
        self.shoppingCartInfoArray[0].Items[sender.tag].Quantity += 1
        //        cell.incrementLbl.text = String(format: "%d", self.shoppingCartInfoArray[0].Items[sender.tag].Quantity)
        let parameters:Dictionary<String,AnyObject> = ["value": self.shoppingCartInfoArray[0].Items[sender.tag].Quantity as AnyObject,
                                                       "key": String(format: "itemquantity%d",self.shoppingCartInfoArray[0].Items[sender.tag].Id ) as AnyObject]
        let dicArray = NSMutableArray ()
        self.cartTbl.reloadData()
        dicArray.add(parameters)
        self.block1?.cancel()
        self.block1 = DispatchWorkItem {
            AppUtility.showLoading(self.view)
            
            
            let tical = self.shoppingCartInfoArray[0].Items[sender.tag].AttributeInfo.components(separatedBy: ":")
            var ticalvalue = ""
            if tical.count>0{
                if tical.indices.contains(1){
                    ticalvalue = tical[1].replacingOccurrences(of: " ", with: "")
                }
            }
            
            self.aPIManager.DeleteAndUpdate(ticalValue: ticalvalue, dicArray, onSuccess: { [weak self] updateCart in
                
                self?.shoppingCartInfoArray = updateCart
                //            UserDefaults.standard.setValue(String(self?.shoppingCartInfoArray[0].Count ?? 0), forKey: "badge")
                
                if let errorArray = self?.shoppingCartInfoArray[0].errorList , errorArray.count > 0 {
                    let message = self?.shoppingCartInfoArray[0].errorList?[0] as! String
                    self?.showAlert(alertTitle: "", description: message)
                }
                
                if self?.shoppingCartInfoArray[0].Count == 0{
                    UserDefaults.standard.set("", forKey: "badge")
                }
                else{
                    UserDefaults.standard.set(self?.shoppingCartInfoArray[0].Count, forKey: "badge")
                }
                
                UserDefaults.standard.synchronize()
                DispatchQueue.main.async {
                    
                    UIView.animate(withDuration: 0.4, animations: {
                        self?.navigationItem.rightBarButtonItem?.customView?.transform = CGAffineTransform(scaleX: 1.4, y: 1.4) }, completion: { (finish: Bool) in
                            UIView.animate(withDuration: 0.4, animations: {
                                self?.navigationItem.rightBarButtonItem?.customView?.transform = CGAffineTransform.identity
                            }, completion: ({finished in
                                //                            self!.navigationItem.rightBarButtonItem = UIBarButtonItem.init(badge: UserDefaults.standard.value(forKey: "badge") as? String, title: "", target: self, action: #selector(self?.cartAction(_:)))
                                self!.navigationItem.rightBarButtonItem = UIBarButtonItem.init(badge: UserDefaults.standard.string(forKey: "badge") ?? "", title: "", target: self, action: #selector(self?.cartAction(_:)))
                            }))
                    })
                    self?.cartTbl.reloadData()
                    self?.continueButtonState(index: indexBtn)
                }
                //            self.customNavBarView.updateShoppingCartCount()
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                }
                }, onError: {[weak self] message in
                    if let viewLoc = self?.view {
                        AppUtility.hideLoading(viewLoc)
                        //                    AppUtility.showToast(message,view: viewLoc)
                        AppUtility.showToastlocal(message: message!.localized, view: viewLoc)
                    }
            })
            
        }
        // execute task in 2 seconds
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: self.block1!)
    }
    
    @objc func minusBtnAction(sender:UIButton) {
        if self.shoppingCartInfoArray[0].Items[sender.tag].Quantity > 0 {
            let indexBtn       = IndexPath(row: sender.tag, section: 0)
            //            let cell            = self.contentTableView.cellForRow(at: indexPath) as! ItemsTableViewCell
            self.shoppingCartInfoArray[0].Items[sender.tag].Quantity -= 1
            //            cell.incrementLbl.text = String(format: "%d", self.shoppingCartInfoArray[0].Items[sender.tag].Quantity)
            let parameters:Dictionary<String,AnyObject> = ["value": self.shoppingCartInfoArray[0].Items[sender.tag].Quantity as AnyObject,
                                                           "key": String(format: "itemquantity%d",self.shoppingCartInfoArray[0].Items[sender.tag].Id ) as AnyObject]
            let dicArray = NSMutableArray ()
            dicArray.add(parameters)
            self.cartTbl.reloadData()
            self.block2?.cancel()
            self.block2 = DispatchWorkItem {
                AppUtility.showLoading(self.view)
                let tical = self.shoppingCartInfoArray[0].Items[sender.tag].AttributeInfo.components(separatedBy: ":")
                           var ticalvalue = ""
                           if tical.count>0{
                               if tical.indices.contains(1){
                                   ticalvalue = tical[1].replacingOccurrences(of: " ", with: "")
                               }
                           }
                self.aPIManager.DeleteAndUpdate(ticalValue: ticalvalue, dicArray, onSuccess: { [weak self] updateCart in
                    self?.shoppingCartInfoArray = updateCart
                    //                UserDefaults.standard.setValue(String(self?.shoppingCartInfoArray[0].Count ?? 0), forKey: "badge")
                    
                    if self?.shoppingCartInfoArray[0].Count == 0{
                        UserDefaults.standard.set("", forKey: "badge")
                    }
                    else{
                        UserDefaults.standard.set(self?.shoppingCartInfoArray[0].Count, forKey: "badge")
                    }
                    UserDefaults.standard.synchronize()
                    
                    DispatchQueue.main.async {
                        
                        UIView.animate(withDuration: 0.4, animations: {
                            self?.navigationItem.rightBarButtonItem?.customView?.transform = CGAffineTransform(scaleX: 1.4, y: 1.4) }, completion: { (finish: Bool) in
                                UIView.animate(withDuration: 0.4, animations: {
                                    self?.navigationItem.rightBarButtonItem?.customView?.transform = CGAffineTransform.identity
                                }, completion: ({finished in
                                    //                                self!.navigationItem.rightBarButtonItem = UIBarButtonItem.init(badge: UserDefaults.standard.value(forKey: "badge") as? String, title: "", target: self, action: #selector(self?.cartAction(_:)))
                                    self!.navigationItem.rightBarButtonItem = UIBarButtonItem.init(badge: UserDefaults.standard.string(forKey: "badge") ?? "", title: "", target: self, action: #selector(self?.cartAction(_:)))
                                }))
                        })
                        
                    }
                    if self?.shoppingCartInfoArray[0].Items.count == 0 {
                        self?.CartDataNotFoundView.isHidden = false
                        self?.CartDataView.isHidden = true
                        //                    return
                    } else {
                        self?.CartDataNotFoundView.isHidden = true
                        self?.CartDataView.isHidden = false
                        self?.cartTbl.reloadData()
                        self?.continueButtonState(index: indexBtn)
                    }
                    
                    
                    //                self.customNavBarView.updateShoppingCartCount()
                    if let viewLoc = self?.view {
                        AppUtility.hideLoading(viewLoc)
                    }
                    }, onError: { [weak self] message in
                        if let viewLoc = self?.view {
                            AppUtility.hideLoading(viewLoc)
                            //                        AppUtility.showToast(message,view: viewLoc)
                            AppUtility.showToastlocal(message: message!.localized, view: viewLoc)
                        }
                })
            }
            // execute task in 2 seconds
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: self.block2!)
        }
    }
    
    func addToWishListBtnAct(_ sender: AnyObject) {
        senderTag = sender.tag ?? 0
        let finalDicArray = NSMutableArray ()
        let parameters:Dictionary<String,AnyObject> = ["value": self.shoppingCartInfoArray[0].Items[sender.tag].Id as AnyObject, "key":"addtocart_1.EnteredQuantity" as AnyObject]
        finalDicArray.add(parameters)
        finalDicArray.addObjects(from: self.dictionaryArray as [AnyObject])
        AppUtility.showLoading(self.view)
        let tical = self.shoppingCartInfoArray[0].Items[sender.tag].AttributeInfo.components(separatedBy: ":")
        var ticalvalue = ""
        if tical.count>0{
            if tical.indices.contains(1){
                ticalvalue = tical[1].replacingOccurrences(of: " ", with: "")
            }
        }
        self.aPIManager.loadAddProductToCart(2, productId: self.shoppingCartInfoArray[0].Items[sender.tag].ProductId, tical: ticalvalue, parameters: finalDicArray, onSuccess:{ [weak self] priceValue in
            if let viewLoc = self?.view {
                AppUtility.hideLoading(viewLoc)
                let parameters:Dictionary<String,AnyObject> = ["value": self?.shoppingCartInfoArray[0].Items[sender.tag].Id as AnyObject,
                                                               "key":"removefromcart" as AnyObject]
                let dicArray = NSMutableArray ()
                dicArray.add(parameters)
                AppUtility.showToastlocal(message: "Moved to wishlist".localized, view: viewLoc)
                self?.removeProduct(dicArray: dicArray)
                //AppUtility.showToastCustomBlackInView(message: "Successfully added to wish list".localized,controller: controller)
            }
            }, onError: { [weak self] message in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    AppUtility.showToastlocal(message: message ?? "", view: viewLoc)
                }
        })
    }
    
    private func hasConnectivity() -> Bool {
        
        let status = Reachability2.instance.networkConnectionStatus()
        guard status != NetworkConnectionStatus.notConnection else {
            showAlertView(message: UsersAlertConstant.checkInternet)
            //   AppUtility.showInternetErrorToast(AppConstants.InternetErrorText.message, view: self.view)
            return false
        }
        return true
    }
    
    private func showAlertView(message: String?) {
        let alertController = UIAlertController(title: "1 Stop Mart", message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: UsersAlertConstant.okAction, style: .default,
                                                handler: nil))
        //  self.dismiss(animated: true, completion: nil)
        present(alertController, animated: true)
    }
    
    
    func createDictForCheckProduct() -> NSMutableArray {
        
        let finalDicArray = NSMutableArray ()
        for (_,item) in self.shoppingCartInfoArray[0].Items.enumerated() {
            var parameters = Dictionary<String, AnyObject>()
            if let cgmID = item.cGMItemID , !cgmID.isEmpty , cgmID != "NULL" {
                let ticalTitle = item.Sku
                if (ticalTitle == "TIC") || ((ticalTitle?.contains("TIC")) != nil){
                if let attributes = item.AttributeInfo.components(separatedBy: ":") as? [String] , attributes.count > 1 {
                    if let ticalQuantity = attributes[1] as? String , ticalQuantity.count > 0{
                        let ticalValue = ticalQuantity.replacingOccurrences(of: " ", with: "")
                        let totalTical = item.Quantity * (NSInteger(ticalValue) ?? 1)
                         parameters["qty"] = totalTical as AnyObject
                     }
                   else {
                          parameters["qty"] = "1" as AnyObject
                      }
                   }
                else {
                     parameters["qty"] = item.Quantity as AnyObject
                    }
                }
                else {
                     parameters["qty"] = item.Quantity as AnyObject
                }
                  parameters["productName"] = (item.ProductName ) as AnyObject
                  parameters["isAvailabe"] = false as AnyObject
                  parameters["availableQty"] = 0 as AnyObject
                  parameters["productBarcode"] = "\(item.ProductId)" as AnyObject
                  parameters["erpProductId"] = (item.cGMItemID ?? "") as AnyObject
                  finalDicArray.add(parameters)
                }
            }
        print("finalDicArray from add to cart : \(finalDicArray)")
        return finalDicArray
    }
    
    
    func checkForTheItems( itemsA : ECOMProduct) -> Bool {
        var isProductAvailable = false
        let availbleQuantity = itemsA.availableQty
        let productID = NSInteger.init(itemsA.productBarcode ?? "1")
        for (_ , product) in self.shoppingCartInfoArray[0].Items.enumerated() {
            if product.ProductId == productID {
                if let productQuantity = itemsA.qty as? Int {
                        if productQuantity <= availbleQuantity {
                            isProductAvailable = true
                        }
                        else {
                            isProductAvailable = false
                        }
                   }
              }
        }
        return isProductAvailable
    }
    
    
    func checkForQuantity() {
                    let finalDicArray = self.createDictForCheckProduct()
                    AppUtility.showLoading(self.view)
                    self.aPIManager.checkProductsOnERPServer(parameters: finalDicArray, onSuccess: { (products) in
                        if let viewLoc = self.view {
                            AppUtility.hideLoading(viewLoc)
                        }
                        var isAllOk : Bool = true
                        for (_ , itemValue) in products.enumerated() {
                             if let productInfo = itemValue as? ECOMProduct {
                                let isProductAvailable = self.checkForTheItems ( itemsA : productInfo)
                                if isProductAvailable {
                                    let productInfo = ["isProductAvailable":true ,"productID": (itemValue.productBarcode) ?? 1 , "AvailableQuantity":productInfo.availableQty] as [String : Any]
                                    self.productAvailbilityCheck.append(productInfo)
                                }
                                else {
                                    let productInfo = ["isProductAvailable":false ,"productID":(itemValue.productBarcode) ?? 1 ,"AvailableQuantity":productInfo.availableQty] as [String : Any]
                                    self.productAvailbilityCheck.append(productInfo)
                                }
                               }
                           }
                        for (_ , itemsData) in self.productAvailbilityCheck.enumerated() {
                            if let isNotAvailable  = itemsData["isProductAvailable"] as? Bool , isNotAvailable  == false {
                                isAllOk = false
                            }
                        }
                        if isAllOk {
                            self.paymentAction()
                        }
                        else {
                            //  self.showAlert(alertTitle: "Information".localized, description: "One or more product Out Of Stock! \n Please review your cart.".localized)
                            
                            self.sAlertController.ShowSAlert(title:  "Information".localized, withDescription: "One or more product Out Of Stock! \n Please review your cart.".localized , onController: self)
                            let okAlertAction = SAlertAction()
                            okAlertAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                                DispatchQueue.main.async {
                                    self.cartTbl.reloadData()
                                }
                            })
                            self.sAlertController.addAction(action: [okAlertAction])
                        }
                    }) { (message) in
                        if let viewLoc = self.view {
                            AppUtility.hideLoading(viewLoc)
                            let msg = message?.components(separatedBy: "\n")
                            let errorMsg = msg?[0]
                            self.showAlert(alertTitle: "Information".localized, description: errorMsg ?? "")
                        }
                    }
                }
    
    
    func paymentAction()  {
        
        if hasConnectivity(){
            print("wwwwwww -\n",self.warningCheckArr)
            var checkWarning = false
            for obj in warningCheckArr {
                if obj {
                    checkWarning = true
                }
            }
            if let totalOrderalue = self.shoppingCartInfoArray[0].orderTotal, let minimumOrder = self.shoppingCartInfoArray[0].MinOrderTotalAmount {
                if let totalOrder = Int(totalOrderalue.replacingOccurrences(of: ",", with: "").replacingOccurrences(of: " MMK", with: ""))! as? Int {
                    if  totalOrder < minimumOrder
                    {
                        let numberFormatter = NumberFormatter()
                        numberFormatter.numberStyle = .decimal
                        if let valueOne = minimumOrder as? Int{
                            let formattedNumber = numberFormatter.string(from: NSNumber(value:valueOne))
                            if let hasValue = formattedNumber{
                                DispatchQueue.main.async {
                                    let alertVC = SAlertController()
                                    alertVC.ShowSAlert(title: "", withDescription: "Minimum order value must be ".localized + " \(hasValue) " + "MMK".localized, onController: self)
                                    let tryAgain = SAlertAction()
                                    tryAgain.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                                        
                                    })
                                    alertVC.addAction(action: [tryAgain])
                                }
                                return
                            }
                        }
                    }
                }
            }
            if checkWarning {
                DispatchQueue.main.async {
                    let alertVC = SAlertController()
                    alertVC.ShowSAlert(title: "Information".localized, withDescription: "Buying is disabled for some product".localized, onController: self)
                    let tryAgain = SAlertAction()
                    tryAgain.action(name: "Ok".localized, AlertType: .defualt, withComplition: {
                        DispatchQueue.main.async {
                            AppUtility.showLoading(self.view)
                            let dicArray = NSMutableArray ()
                            
                            for (index,obj) in self.warningCheckArr.enumerated() {
                                if obj {
                                    print("iiiiiiiiiiiiii - ",index)
                                    self.warningCheckArr.remove(at: index)
                                    let parameters:Dictionary<String,AnyObject> = ["value": self.shoppingCartInfoArray[0].Items[index].Id as AnyObject,"key":"removefromcart" as AnyObject]
                                    dicArray.add(parameters)
                                }
                            }
                            print(dicArray)
                            self.removeProduct(dicArray: dicArray)
                        }
                    })
                    alertVC.addAction(action: [tryAgain])
                }
                return
            }
            
            if Cart.PickupAddress != nil {
                if UserDefaults.standard.bool(forKey: "GuestLogin") {
                    if let _ = self.defaultSelectedAddress {
                        self.performSegue(withIdentifier: "CartToPaymentSegue", sender: self)
                         UserDefaults.standard.set("Pickup Address", forKey: "shipadd")
                    }else {
                        DispatchQueue.main.async {
                            if let addAddressVC = UIStoryboard(name: "Cart", bundle: nil).instantiateViewController(withIdentifier: "AddAddress_ID") as? AddAddress {
                                 addAddressVC.screenFrom = "Cart"
                                 UserDefaults.standard.set("Pickup Address", forKey: "shipadd")
                                self.navigationController?.pushViewController(addAddressVC, animated: true)
                            }
                        }
                    }
                }else {
                    if UserDefaults.standard.bool(forKey: "LoginStatus") {
                        if let addressCheck = VMLoginModel.shared.addressAdded, addressCheck == true {
                            self.performSegue(withIdentifier: "CartToPaymentSegue", sender: self)
                        } else {
                            DispatchQueue.main.async {
                                if let addAddressVC = UIStoryboard(name: "Cart", bundle: nil).instantiateViewController(withIdentifier: "AddAddress_ID") as? AddAddress {
                                    addAddressVC.screenFrom = "Cart"
                                    self.navigationController?.pushViewController(addAddressVC, animated: true)
                                }
                            }
                        }
                    }else {
                        DispatchQueue.main.async {
                            self.navigateLoginAndRegistration()
                        }
                    }
                }
            }else {
                if UserDefaults.standard.bool(forKey: "GuestLogin") {
                    if let _ = self.defaultSelectedAddress {
                        self.performSegue(withIdentifier: "CartToPaymentSegue", sender: self)
                        UserDefaults.standard.set("Delivery Address", forKey: "shipadd")
                    } else {
                        DispatchQueue.main.async {
                            if let addAddressVC = UIStoryboard(name: "Cart", bundle: nil).instantiateViewController(withIdentifier: "AddAddress_ID") as? AddAddress {
                                addAddressVC.screenFrom = "Cart"
                                UserDefaults.standard.set("Delivery Address", forKey: "shipadd")
                                self.navigationController?.pushViewController(addAddressVC, animated: true)
                            }
                        }
                    }
                }else {
                    if UserDefaults.standard.bool(forKey: "LoginStatus") {
                        if UserDefaults.standard.bool(forKey: "RegistrationDone") {
                            if UserDefaults.standard.integer(forKey: "shippingaddress") != 0 {
                                if Cart.PickupAddress == nil {
                                    DispatchQueue.main.async {
                                        let alertVC = SAlertController()
                                        alertVC.ShowSAlert(title: "".localized, withDescription: UsersPickupPoint.pointMessage.localized, onController: self)
                                        let YesAction = SAlertAction()
                                        YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                                            self.performSegue(withIdentifier: "selectAddressSeque", sender: self)
                                        })
                                        alertVC.addAction(action: [YesAction])
                                    }
                                }
                                else {
                                    guard ((self.defaultSelectedAddress?.id) != nil) else {
                                        return
                                    }
                                    AppUtility.showLoading(self.view)
                                    self.aPIManager.saveBillingOrShippingAddress(1, from: (self.defaultSelectedAddress?.id)!,
                                                                                 onSuccess: {
                                                                                    DispatchQueue.main.async {
                                                                                        UserDefaults.standard.set("Delivery Address", forKey: "shipadd")
                                                                                        self.performSegue(withIdentifier: "CartToPaymentSegue", sender: self)
                                                                                        AppUtility.hideLoading(self.view)
                                                                                    }
                                    },                    onError: { message in
                                        AppUtility.hideLoading(self.view)
                                    })
                                }
                            }else {
                                //condition for delivery address
                                if ((self.defaultSelectedAddress?.id) != nil) {
                                    let CorrectAddress  = AppUtility.checkForCorrectAddress(address: self.defaultSelectedAddress)
                                    
                                    if CorrectAddress {
                                         UserDefaults.standard.set("Delivery Address", forKey: "shipadd")
                                        self.performSegue(withIdentifier: "CartToPaymentSegue", sender: self)
                                    }
                                    else {
                                        DispatchQueue.main.async {
                                            let alertVC = SAlertController()
                                            alertVC.ShowSAlert(title: "".localized, withDescription: UsersPickupPoint.defaultAddressMessage.localized, onController: self)
                                            let YesAction = SAlertAction()
                                            YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                                                
                                            })
                                            alertVC.addAction(action: [YesAction])
                                        }
                                    }
                                }else {
                                    DispatchQueue.main.async {
                                        if let addAddressVC = UIStoryboard(name: "Cart", bundle: nil).instantiateViewController(withIdentifier: "AddAddress_ID") as? AddAddress {
                                            addAddressVC.screenFrom = "Cart"
                                            self.navigationController?.pushViewController(addAddressVC, animated: true)
                                        }
                                    }
                                }
                            }
                        }else {
                            self.navigateLoginAndRegistration()
                        }
                    }else {
                        self.navigateLoginAndRegistration()
                    }
                }
                
            }
        }
        
    }
    
    
    
    @IBAction func continueToPaymentAction(_ sender: UIButton) {
        
        self.productAvailbilityCheck.removeAll()
       // self.checkForQuantity()
        self.paymentAction()
    }
    
    private func navigateLoginAndRegistration() {
        let regitration_Opened = UserDefaults.standard.value(forKey: "REGITRATION_OPENED") as? Bool ?? false
        if regitration_Opened {
            DispatchQueue.main.async {
                if let registrationVC = UIStoryboard(name: "VMartLogin", bundle: Bundle.main).instantiateViewController(withIdentifier: "VMRegistrationViewController_ID") as? VMRegistrationViewController {
                    registrationVC.screenFrom = "Cart"
                    registrationVC.modalPresentationStyle = .fullScreen
                    if let number = UserDefaults.standard.value(forKey: "MOBILE_NUMBER") as? String {
                        RegistrationModel.share.MobileNumber = number
                    }
                    registrationVC.continueAsGuest = true
                    registrationVC.delegate = self
                    self.navigationController?.pushViewController(registrationVC, animated: true)
                }
            }
        }else {
            if let loginVC = UIStoryboard(name: "VMartLogin", bundle: nil).instantiateViewController(withIdentifier: "VMLoginViewController_ID") as? VMLoginViewController {
                loginVC.screenFrm = "Cart"
                loginVC.asGuest = true
                self.navigationController?.pushViewController(loginVC, animated: true)
            }
        }
    }
    
    
    @IBAction func continueShopAction(_ sender: UIButton) {
        //        self.dismiss(animated: true, completion: nil)
        //        let viewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ViewController")
        //        UIApplication.shared.keyWindow?.rootViewController = viewController
        self.performSegue(withIdentifier: "unwindToHome", sender: self)
    }
    
    @IBAction func cartAction(_ sender: UIBarButtonItem) {
        //self.performSegue(withIdentifier: "unwindToHome", sender: self)
    }
    
    @IBAction func searchAction(_ sender: UIBarButtonItem) {
        
        let viewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchProducts")
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    @IBAction func editAddressAction(_ sender: UIButton) {
        //defaultAddress
        if let editAddressVC = self.storyboard?.instantiateViewController(withIdentifier: "AddAddress_ID") as? AddAddress {
            editAddressVC.screenFrom = "Edit Address"
            editAddressVC.delegate = self
            editAddressVC.defaultAddress = defaultSelectedAddress
            self.navigationController?.pushViewController(editAddressVC, animated: true)
        }
    }
}


extension Cart: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //        return self.subCategoriesArray.count
        if section == 1 {
            if flag {
                return self.shoppingCartInfoArray[0].Items.count
            }
        } else {
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : CustomTableViewCell?
        switch indexPath.section {
        case 0:  cell = tableView.dequeueReusableCell(withIdentifier: "addressCell", for: indexPath) as? CustomTableViewCell
        cell?.wrapData(addressModel: self.defaultSelectedAddress)
            break
        case 1:  cell = tableView.dequeueReusableCell(withIdentifier: "itemCell", for: indexPath) as? CustomTableViewCell
        cell?.wrapCellData(Data: self.shoppingCartInfoArray[0].Items[indexPath.row], indexPath: indexPath,availabilityArray: self.productAvailbilityCheck)
        
        if self.shoppingCartInfoArray[0].Items[indexPath.row].AvailableQuantity == 0 {
            DispatchQueue.main.async {
                let alertVC = SAlertController()
                alertVC.ShowSAlert(title: "Information".localized, withDescription: "Out of stock".localized, onController: self)
                let tryAgain = SAlertAction()
                tryAgain.action(name: "Ok".localized, AlertType: .defualt, withComplition: {
                    DispatchQueue.main.async {
                        AppUtility.showLoading(self.view)
                        let dicArray = NSMutableArray ()
                        
                        for (index,obj) in self.warningCheckArr.enumerated() {
                            if obj {
                                print("iiiiiiiiiiiiii - ",index)
                                self.warningCheckArr.remove(at: index)
                                let parameters:Dictionary<String,AnyObject> = ["value": self.shoppingCartInfoArray[0].Items[index].Id as AnyObject,"key":"removefromcart" as AnyObject]
                                dicArray.add(parameters)
                            }
                        }
                        print(dicArray)
                        self.senderTag = indexPath.row
                        self.removeProduct(dicArray: dicArray)
                    }
                })
                alertVC.addAction(action: [tryAgain])
            }
        }
        
        
        if (self.shoppingCartInfoArray[0].Items[indexPath.row].Warnings?.count)! > 0{
            self.warningCheckArr[indexPath.row] = true
        } else {
            self.warningCheckArr[indexPath.row] = false
        }
            break
        case 2:  cell = tableView.dequeueReusableCell(withIdentifier: "chooseCell", for: indexPath) as? CustomTableViewCell
        //        cell?.pickPointlocBtn.tag = indexPath.section
        cell?.buttonStateManage(locationModel: Cart.PickupAddress,indexpath: indexPath, selectedIndPath: self.selectedIndex)
        
        //        selectedIndex = IndexPath(row: 0, section: indexPath.row)
        //        cell?.defaultSetDelivery(sender: (cell?.deliveryBtn)!)
            break
        case 3:  cell = tableView.dequeueReusableCell(withIdentifier: "selectedaddressCell", for: indexPath) as? CustomTableViewCell
        cell?.wraplocationData(locationModel: Cart.PickupAddress)
        //cell?.pickPointlocBtn.tag = indexPath.section
            break
        default: cell = tableView.dequeueReusableCell(withIdentifier: "paymentCell", for: indexPath) as? CustomTableViewCell
        cell?.paymentCellUpdate(paymentData: self.shoppingCartInfoArray[0])
            break
        }
        cell?.Delegate = self
        return cell!
    }
    
    func TapSubcategoary(indexPath: IndexPath){
        
        guard let productDetailvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProductDetails") as? ProductDetails else { return }
        productDetailvc.productId = Int(self.shoppingCartInfoArray[0].Items[indexPath.item].ProductId)
        productDetailvc.productName = self.shoppingCartInfoArray[0].Items[indexPath.item].ProductName as String
        self.navigationController?.pushViewController(productDetailvc, animated: true)
        
        
    }
    
    
    //    {
    
    //        self.categoryId = self.subCategoriesArray[indexPath.row].idd
    //        self.categoryName = self.subCategoriesArray[indexPath.row].name
    //        if AppUtility.isConnectedToNetwork() {
    //            self.loadSubCategories()
    //        } else {
    //            AppUtility.showInternetErrorToast(AppConstants.InternetErrorText.message, view: self.view)
    //        }
    
    //    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        if indexPath.section == 1{
        //            self.TapSubcategoary(indexPath: indexPath)
        //        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            // return UITableView.automaticDimension
            if UserDefaults.standard.bool(forKey: "LoginStatus") {
                return (defaultSelectedAddress != nil) ? UITableView.automaticDimension : 0
            }else {
                return 0
            }
        case 1:
            if let cell = tableView.cellForRow(at: indexPath) as? CustomTableViewCell {
                cell.productImgHeightConstraint.constant = cell.contentView.bounds.height-65
            }
            return UITableView.automaticDimension
                    //return 200
        case 2:
            return self.returnsize(index: 1, indexPath: indexPath)
        case 3:
            return  (Cart.PickupAddress != nil) ? UITableView.automaticDimension : 0
        default:
            return UITableView.automaticDimension
        }
    }
    
    func returnsize(index: Int,indexPath: IndexPath) -> CGFloat {
        if indexPath == selectedIndex && Cart.PickupAddress == nil{
            return 165 //Size you want to increase to
        } else {
            return 122 // Default Size
        }
    }
    
}

extension Cart: CustomCellDelegate {
    func btnEditAddressClick(sender: UIButton) {
        self.performSegue(withIdentifier: "selectAddressSeque", sender: self)
    }
    
    func deliveryBtnActionHandler(sender: UIButton) {
        DispatchQueue.main.async {
            if sender.tag == 2 || self.isObjectNotNil(object: Cart.PickupAddress) {
                Cart.PickupAddress = nil
                let indexSection = NSIndexSet(index: 3)
                self.cartTbl.reloadSections(indexSection as IndexSet, with: .none)
                self.selectedIndex = IndexPath(row: 0, section: sender.tag)
                self.cartTbl.reloadRows(at: [self.selectedIndex], with: .none)
            } else {
                self.selectedIndex = IndexPath(row: 0, section: sender.tag)
                self.cartTbl.reloadRows(at: [self.selectedIndex], with: .none)
            }
        }
    }
    
    func AddWishListBtnActionHandler(sender: UIButton){
        // self.addToWishListBtnAct(sender)
    }
    
    func plusBtnActionHandler(sender: UIButton) {
        self.plusBtnAction(sender)
    }
    
    func minusBtnActionHandler(sender: UIButton) {
        self.minusBtnAction(sender: sender)
    }
    
    func deleteCartItemAction(sender: UIButton) {
        DispatchQueue.main.async {
            let alertVC = SAlertController()
            alertVC.ShowSAlert(title: "".localized, withDescription: "Are you want to remove this item?".localized, onController: self)
            let YesAction = SAlertAction()
            YesAction.action(name: "Yes".localized, AlertType: .defualt, withComplition: {
                self.deleteBtnAction(sender)
            })
            let cancelAction = SAlertAction()
            cancelAction.action(name: "Save for later".localized, AlertType: .defualt, withComplition: {
                self.addToWishListBtnAct(UIButton())
            })
            alertVC.addAction(action: [YesAction,cancelAction])
        }
        
    }
    
    func btnChangeAddressClick(sender: UIButton) {
        self.performSegue(withIdentifier: "CartToAddAddrerssSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "CartToAddAddrerssSegue" {
            if let rootViewController = segue.destination as? addressCollection {
                rootViewController.delegate = self
                rootViewController.allBillingAddresses = self.allBillingAddresses
            }
        } else if segue.identifier == "CartToAddBillingAddressSegue" {
            if let addAddressVC = segue.destination.children[0] as? AddAddress {
                addAddressVC.delegate = self
                
                self.editAddressAction(UIButton())
                
                
            }
        } else if segue.identifier == "selectAddressSeque"{
            if let pickupVC = segue.destination as? Location {
                pickupVC.Delegate = self
            }
        }
    }
    
    
    
}

extension Cart: LocationDelegate {
    func SelectedPickupLocationClick(object: Store) {
        AppUtility.showLoading(self.view)
        
        self.aPIManager.saveStoreAddress(object.storeId.integerValue, onSuccess: {
            AppUtility.hideLoading(self.view)
            DispatchQueue.main.async {
                if self.isObjectNotNil(object: object) {
                    Cart.PickupAddress = object
                } else {
                    Cart.PickupAddress = nil
                }
                println_debug(object)
                let indexSection = NSIndexSet(index: 3)
                self.cartTbl.reloadSections(indexSection as IndexSet, with: .automatic)
            }
            
        }, onError: { message in
            AppUtility.hideLoading(self.view)
            AppUtility.showToast(message,view: self.view)
            AppUtility.showToastlocal(message: message!.localized, view: self.view)
        })
    }
    
    func isObjectNotNil(object:AnyObject!) -> Bool {
        if let _:AnyObject = object {
            return true
        }
        return false
    }
}

extension Cart : AddBillingAddressDelegate {
    func AddressTapped(indexPath: IndexPath) {
        println_debug(indexPath)
        
        AppUtility.showLoading(self.view)
        self.aPIManager.saveBillingOrShippingAddress(1, from: self.allBillingAddresses[indexPath.row].id!,
                                                     onSuccess: {
                                                        AppUtility.hideLoading(self.view)
                                                        DispatchQueue.main.async {
                                                            self.defaultSelectedAddress = self.allBillingAddresses[indexPath.row]
                                                            let indexSection = NSIndexSet(index: 0)
                                                            self.cartTbl.reloadSections(indexSection as IndexSet, with: .none)
                                                        }
        },                    onError: { message in
            AppUtility.hideLoading(self.view)
        })
    }
    
    func addBillingAddressTapped(indexPath: IndexPath,count:Int) {
        if let editAddressVC = self.storyboard?.instantiateViewController(withIdentifier: "AddAddress_ID") as? AddAddress {
            editAddressVC.screenFrom = "Add Address"
            editAddressVC.delegate = self
            editAddressVC.defaultAddress = defaultSelectedAddress
            self.navigationController?.pushViewController(editAddressVC, animated: true)
        }
    }
    
    func pickupLocationTapped(sender: UIButton) {
        self.performSegue(withIdentifier: "selectAddressSeque", sender: self)
    }
}

extension Cart : RegistrationDelegate {
    
    func backToMainPage() {
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK: step 1 Add Protocol here.
protocol CustomCellDelegate: class {
    
    func btnEditAddressClick(sender: UIButton)
    func btnChangeAddressClick(sender: UIButton)
    func deleteCartItemAction(sender: UIButton)
    func plusBtnActionHandler(sender: UIButton)
    func minusBtnActionHandler(sender: UIButton)
    //    func AddWishListBtnActionHandler(sender: UIButton)
    func deliveryBtnActionHandler(sender: UIButton)
    func pickupLocationTapped(sender: UIButton)
    
}

class CustomTableViewCell: UITableViewCell {
    
    @IBOutlet var viewOne: UIView!
    @IBOutlet var viewTwo: UIView!
    @IBOutlet var productDiscountPercentageLbl: UILabel!{
        didSet {
            self.productDiscountPercentageLbl.font = UIFont(name: appFont, size: 15.0)
            self.productDiscountPercentageLbl.text = self.productDiscountPercentageLbl.text?.localized
            
        }
    }
    @IBOutlet var oldPriceLbl: UILabel!{
        didSet {
            self.oldPriceLbl.font = UIFont(name: appFont, size: 15.0)
            self.oldPriceLbl.text = self.oldPriceLbl.text?.localized
            
        }
    }
    @IBOutlet var productImgHeightConstraint: NSLayoutConstraint!
    
    
    @IBOutlet var colorLbl: UILabel! {
        didSet {
            self.colorLbl.font = UIFont(name: appFont, size: 15.0)
            self.colorLbl.text = self.colorLbl.text?.localized
            
        }
    }
    
    @IBOutlet var outofstockLabel: UILabel!{
        didSet {
            self.outofstockLabel.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet var warningLbl: UILabel! {
        didSet {
            self.warningLbl.font = UIFont(name: appFont, size: 15.0)
            self.warningLbl.text = self.warningLbl.text?.localized
            
        }
    }
    
    @IBOutlet var taxLbl: UILabel! {
        didSet {
            self.taxLbl.font = UIFont(name: appFont, size: 15.0)
            self.taxLbl.text = self.taxLbl.text?.localized
            
        }
    }
    
    @IBOutlet var taxValueLbl: UILabel! {
        didSet {
            self.taxValueLbl.font = UIFont(name: appFont, size: 15.0)
            self.taxValueLbl.text = self.taxValueLbl.text?.localized
            
        }
    }
    
    @IBOutlet var deliveryDetailLbl: UILabel! {
        didSet {
            self.deliveryDetailLbl.font = UIFont(name: appFont, size: 15.0)
            self.deliveryDetailLbl.text = self.deliveryDetailLbl.text?.localized
            
        }
    }
    @IBOutlet var pickupPointLocationLbl: UILabel! {
        didSet {
            self.pickupPointLocationLbl.font = UIFont(name: appFont, size: 15.0)
            self.pickupPointLocationLbl.text = self.pickupPointLocationLbl.text?.localized
            
        }
    }
    @IBOutlet var paymentDetailLbl: UILabel! {
        didSet {
            self.paymentDetailLbl.font = UIFont(name: appFont, size: 15.0)
            self.paymentDetailLbl.text = self.paymentDetailLbl.text?.localized
            
        }
    }
    @IBOutlet var productAmountLbl: UILabel! {
        didSet {
            self.productAmountLbl.font = UIFont(name: appFont, size: 15.0)
            self.productAmountLbl.text = self.productAmountLbl.text?.localized
            
            
        }
    }
    @IBOutlet var shippingChargeLbl: UILabel! {
        didSet {
            self.shippingChargeLbl.font = UIFont(name: appFont, size: 15.0)
            self.shippingChargeLbl.text = self.shippingChargeLbl.text?.localized
            
        }
    }
    @IBOutlet var totalPayLbl: UILabel! {
        didSet {
            self.totalPayLbl.font = UIFont(name: appFont, size: 15.0)
            self.totalPayLbl.text = self.totalPayLbl.text?.localized
            
        }
    }
    @IBOutlet var addressNameLbl: UILabel! {
        didSet {
            self.addressNameLbl.font = UIFont(name: appFont, size: 15.0)
            self.addressNameLbl.text = self.addressNameLbl.text?.localized
            
        }
    }
    @IBOutlet var addressLabel: UILabel! {
        didSet {
            self.addressLabel.font = UIFont(name: appFont, size: 15.0)
            self.addressLabel.text = self.addressLabel.text?.localized
            
        }
    }
    @IBOutlet var addressPhLabel: UILabel! {
        didSet {
            self.addressPhLabel.font = UIFont(name: appFont, size: 15.0)
            self.addressPhLabel.text = self.addressPhLabel.text?.localized
            
        }
    }
    @IBOutlet var LocaddressNameLbl: UILabel! {
        didSet {
             self.LocaddressNameLbl.font = UIFont(name: appFont, size: 15.0)
            self.LocaddressNameLbl.text = self.LocaddressNameLbl.text?.localized
           
        }
    }
    @IBOutlet var LocaddressLabel: UILabel! {
        didSet {
            self.LocaddressLabel.font = UIFont(name: appFont, size: 15.0)
            self.LocaddressLabel.text = self.LocaddressLabel.text?.localized
            
        }
    }
    @IBOutlet var LocaddressPhLabel: UILabel! {
        didSet {
            self.LocaddressPhLabel.font = UIFont(name: appFont, size: 15.0)
            self.LocaddressPhLabel.text = self.LocaddressPhLabel.text?.localized
            
        }
    }
    @IBOutlet var ProductNameLbl: UILabel! {
        didSet {
            self.ProductNameLbl.font = UIFont(name: appFont, size: 15.0)
            self.ProductNameLbl.text = self.ProductNameLbl.text?.localized
            
        }
    }
    @IBOutlet var ProductNameLbl1: UILabel! {
        didSet {
            self.ProductNameLbl1.font = UIFont(name: appFont, size: 15.0)
            self.ProductNameLbl1.text = self.ProductNameLbl1.text?.localized
            
        }
    }
    @IBOutlet var UnitPriceLbl: UILabel! {
        didSet {
            self.UnitPriceLbl.font = UIFont(name: appFont, size: 15.0)
            self.UnitPriceLbl.text = self.UnitPriceLbl.text?.localized
            
        }
    }
    @IBOutlet var UnitPriceLbl1: UILabel! {
        didSet {
            self.UnitPriceLbl1.font = UIFont(name: appFont, size: 15.0)
            self.UnitPriceLbl1.text = self.UnitPriceLbl1.text?.localized
            
        }
    }
    @IBOutlet var subCategoriesLbl: UILabel! {
        didSet {
            self.subCategoriesLbl.font = UIFont(name: appFont, size: 15.0)
            self.subCategoriesLbl.text = self.subCategoriesLbl.text?.localized
            
        }
    }
    @IBOutlet var incrementLbl: UILabel! {
        didSet {
            self.incrementLbl.font = UIFont(name: appFont, size: 15.0)
            self.incrementLbl.text = self.incrementLbl.text?.localized
            
        }
    }
    @IBOutlet var subTotalLbl: UILabel! {
        didSet {
            self.subTotalLbl.font = UIFont(name: appFont, size: 15.0)
            self.subTotalLbl.text = self.subTotalLbl.text?.localized
            
            //            self.subTotalLbl.amountAttributedString()
        }
    }
    @IBOutlet var shippingLbl: UILabel! {
        didSet {
            self.shippingLbl.font = UIFont(name: appFont, size: 15.0)
            self.shippingLbl.text = self.shippingLbl.text?.localized
            
            //            self.shippingLbl.amountAttributedString()
        }
    }
    
    //    @IBOutlet var taxLbl: UILabel! {
    //        didSet {
    //            self.taxLbl.text = self.taxLbl.text?.localized
    //        }
    //    }
    
    @IBOutlet var totalPriceLabel: UILabel! {
        didSet {
            self.totalPriceLabel.font = UIFont(name: appFont, size: 15.0)
            self.totalPriceLabel.text = self.totalPriceLabel.text?.localized
            
            //            self.totalPriceLabel.amountAttributedString()
            
        }
    }
    @IBOutlet var deliveryDateLabel: UILabel! {
        didSet {
            self.deliveryDateLabel.font = UIFont(name: appFont, size: 15.0)
            self.deliveryDateLabel.text = self.deliveryDateLabel.text?.localized
            
        }
    }
    @IBOutlet var itemImageView: UIImageView!
    @IBOutlet var btnChangeAddress: UIButton! {
        didSet {
            btnChangeAddress.setTitle((btnChangeAddress.titleLabel?.text ?? "").localized, for: .normal)
            btnChangeAddress.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var plusBtn: UIButton!
    @IBOutlet var minusBtn: UIButton!
    @IBOutlet var deleteBtn: UIButton! {
        didSet {
            deleteBtn.setTitle((deleteBtn.titleLabel?.text ?? "").localized, for: .normal)
            deleteBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var AddWishListBtn: UIButton! {
        didSet {
            AddWishListBtn.setTitle((AddWishListBtn.titleLabel?.text ?? "").localized, for: .normal)
            AddWishListBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var deliveryBtn: UIButton! {
        didSet {
            deliveryBtn.setImage(UIImage(named: "act_radio"), for: .normal)
            deliveryBtn.setTitle(("On Door Delivery".localized), for: .normal)
            deliveryBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var pickupBtn: UIButton! {
        didSet {
            pickupBtn.setTitle(("On Pickup Point".localized), for: .normal)
            pickupBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet var pickPointlocBtn: UIButton! {
        didSet {
            pickPointlocBtn.setTitle((pickPointlocBtn.titleLabel?.text ?? "").localized, for: .normal)
            pickPointlocBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    weak var Delegate: CustomCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func btnEditAddressClick(sender: UIButton) {
        self.Delegate?.btnEditAddressClick(sender: sender)
    }
    
    @IBAction func btnChangeAddressClick(sender: UIButton) {
        self.Delegate?.btnChangeAddressClick(sender: sender)
    }
    
    @IBAction func deleteBtnActionClick(sender: UIButton) {
        self.Delegate?.deleteCartItemAction(sender: sender)
    }
    
    @IBAction func plusBtnActionClick(sender: UIButton) {
        self.Delegate?.plusBtnActionHandler(sender: sender)
    }
    
    @IBAction func minusBtnActionClick(sender: UIButton) {
        self.Delegate?.minusBtnActionHandler(sender: sender)
        
    }
    
    @IBAction func AddWishListActionClick(sender: UIButton) {
        //       self.Delegate?.AddWishListBtnActionHandler(sender: sender)
    }
    
    @IBAction func deliveryActionClick(sender: UIButton) {
        if sender == deliveryBtn {
            deliveryBtn.isSelected = true
            pickupBtn.isSelected = false
            pickPointlocBtn.isHidden = true
        } else if sender == pickupBtn {
            deliveryBtn.isSelected = false
            pickupBtn.isSelected = true
            pickPointlocBtn.isHidden = false
        }
        self.Delegate?.deliveryBtnActionHandler(sender: sender)
    }
    
    @IBAction func deliveryBtnAction(sender: UIButton) {
        self.deliveryBtn.setImage(UIImage(named: "act_radio"), for: .normal)
        self.pickupBtn.setImage(UIImage(named: "radio"), for: .normal)
        UserDefaults.standard.set(0, forKey: "shippingaddress")
        UserDefaults.standard.set("Delivery Address", forKey: "shipadd")
        UserDefaults.standard.synchronize()
        self.Delegate?.deliveryBtnActionHandler(sender: sender)
    }
    
    @IBAction func pickupBtnAction(sender: UIButton) {
        DispatchQueue.main.async {
            self.deliveryBtn.setImage(UIImage(named: "radio"), for: .normal)
            self.pickupBtn.setImage(UIImage(named: "act_radio"), for: .normal)
        }
        UserDefaults.standard.set(1, forKey: "shippingaddress")
        UserDefaults.standard.set("Pickup Address", forKey: "shipadd")
        UserDefaults.standard.synchronize()
        self.Delegate?.deliveryBtnActionHandler(sender: sender)
    }
    
    @IBAction func pickupBtnClick(sender: UIButton) {
        self.Delegate?.pickupLocationTapped(sender: sender)
    }
    
    //    func defaultSetDelivery(sender: UIButton) {
    //        deliveryBtn.isSelected = true
    //        pickupBtn.isSelected = false
    //        pickPointlocBtn.isHidden = true
    //    }
    
    func wrapCellData(Data: CheckOutItems, indexPath: IndexPath, availabilityArray : [[String : Any]]) {
        DispatchQueue.main.async {
            self.productImgHeightConstraint.constant = self.contentView.bounds.height-65
        }
        if let imageUrl = Data.ImageUrl {
            self.itemImageView.sd_setImage(with: URL(string: imageUrl))
            //            AppUtility.getData(from: URL(string: Data.ImageUrl ?? "")!) { data, response, error in
            //                guard let data = data, error == nil else { return }
            //                print("Download Finished")
            //                DispatchQueue.main.async() {
            //                    self.itemImageView.image = UIImage(data: data)
            //                }
            //            }
            
            //            AppUtility.NKPlaceholderImage(image: UIImage(named: Data.ImageUrl ?? ""), imageView: self.itemImageView, imgUrl: Data.ImageUrl ?? "") { (image) in }
            
            
            
            
        }
        if Data.Quantity as NSInteger == 1{
            self.minusBtn.isEnabled = false
            self.minusBtn.alpha = 0.6
        }
        else{
            self.minusBtn.isEnabled = true
            self.minusBtn.alpha = 1.0
        }
        if Data.Quantity as NSInteger == 99{
            self.plusBtn.isEnabled = false
            self.plusBtn.alpha = 0.6
        }
        else{
            self.plusBtn.isEnabled = true
            self.plusBtn.alpha = 1.0
        }
        
        if availabilityArray.count > 0 {
            let currentProductID = Data.ProductId
            var productInfoCheck : Dictionary<String,Any> = Dictionary<String,Any>()
            for (_, item) in availabilityArray.enumerated() {
                if let productInfoData = item as? [String : Any] {
                    if let productID = productInfoData["productID"] as? String {
                        if currentProductID == NSInteger(productID) {
                            productInfoCheck.updateValue(productInfoData["isProductAvailable"] ?? false, forKey: "isProductAvailable")
                            productInfoCheck.updateValue(productInfoData["productID"] ?? 1, forKey: "productID")
                            productInfoCheck.updateValue(productInfoData["AvailableQuantity"] ?? 0 , forKey: "AvailableQuantity")
                        }
                    }
                }
            }
            
            if let availableP = productInfoCheck["isProductAvailable"] as? Bool , !availableP {
                let Quantity = (productInfoCheck["AvailableQuantity"] as? Int) ?? 0
                if productInfoCheck.count > 0 && Quantity > 0 {
                    if (Data.Sku == "TIC") || ((Data.Sku?.contains("TIC")) != nil) {
                        if let attributes = Data.AttributeInfo.components(separatedBy: ":") as? [String] , attributes.count > 1 {
                            if let ticalQuantity = attributes[1] as? String {
                                let ticalValue = ticalQuantity.replacingOccurrences(of: " ", with: "")
                                 let tical = NSInteger(ticalValue) ?? 1
                                 self.outofstockLabel.isHidden = false
                                 self.outofstockLabel.text = "Max Available Quantity : \(Quantity/tical)"
                             }
                        }
                        else {
                            self.outofstockLabel.isHidden = false
                            self.outofstockLabel.text = "Max Available Quantity : \(Quantity)"
                        }
                    }
                    else {
                        self.outofstockLabel.isHidden = false
                        self.outofstockLabel.text = "Max Available Quantity : \(Quantity)"
                    }
                }
                else {
                    self.outofstockLabel.isHidden = false
                    self.outofstockLabel.text = "Out of stock"
                }
            }
            else {
                self.outofstockLabel.isHidden = true
            }
        }
        else {
            self.outofstockLabel.isHidden = true
        }
        
        self.ProductNameLbl.text = Data.ProductName as String
        self.incrementLbl.text = String(format: "%d",Data.Quantity as NSInteger)
        self.UnitPriceLbl.text = Data.UnitPrice as String
        self.colorLbl.text = (Data.AttributeInfo as String).replacingOccurrences(of: "<br />", with: "\n")
        if (Data.Warnings?.count)! > 0{
            if let warning = Data.Warnings![0] as? String{
                self.warningLbl.text =  warning
            }
        }
        else{
            self.warningLbl.text =  ""
        }
        self.deliveryDateLabel.text = String.init(format: "%@: %@", "Delivery Time".localized, Data.DeliveryDays ?? "24 Hours")
        self.plusBtn.tag = indexPath.row
        self.minusBtn.tag = indexPath.row
        self.deleteBtn.tag = indexPath.row
        //       self.AddWishListBtn.tag = indexPath.row
        println_debug(Data.ImageUrl ?? "")
        println_debug(Data.ProductName)
        println_debug(Data.UnitPrice)
        println_debug(Data.Quantity)
        self.ProductNameLbl1.text = Data.ProductName as String
        //        self.UnitPriceLbl1.text = Data.UnitPrice as String
        //        self.UnitPriceLbl1.amountAttributedString()
        self.updatePrice(Data: Data)
        
        
    }
    
    func updatePrice(Data: CheckOutItems) {
        let (Price,_,_) = AppUtility.getPriceOldPriceDiscountPrice(price: Data.UnitPrice as String, oldPrice: Data.OldPrice as String, priceWithDiscount: "")
        
        var productPrice = Data.UnitPrice.replacingOccurrences(of: "MMK", with: "")
        productPrice = productPrice + mmkText
        let attrString = NSMutableAttributedString(string: productPrice)
        let nsRange = NSString(string: productPrice).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
        attrString.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange)
        self.UnitPriceLbl1.attributedText = attrString
        
        var oldPrice = Data.OldPrice.replacingOccurrences(of: "MMK", with: "")
        oldPrice = oldPrice + mmkText
        
        if Price == "" {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           var productPrice2 = Data.UnitPrice.replacingOccurrences(of: "MMK", with: "")
            productPrice2 = productPrice2 + mmkText
            let attrStr = NSMutableAttributedString(string: productPrice2)
            let nsRange = NSString(string: productPrice2).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
            attrStr.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange)
            self.UnitPriceLbl1.attributedText = attrStr
            
            let attrString = NSMutableAttributedString(string: oldPrice, attributes: [NSAttributedString.Key.strikethroughStyle: NSUnderlineStyle.single.rawValue])
            let nsRange2 = NSString(string: oldPrice).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
            attrString.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange2)
            self.oldPriceLbl.attributedText = attrString
        }
        
        if Data.DiscountPercentage != 0 {
            self.productDiscountPercentageLbl.text = "\((Data.DiscountPercentage) ?? 0)% off - \(Data.DiscountOfferName ?? "")"
        }
    }
    
    func wrapData(addressModel: GenericBillingAddress?) {
        if let model = addressModel {
            self.addressNameLbl.text = model.firstName ?? ""
            
            var houseNo = "\(model.houseNo?.replacingOccurrences(of: "House No ".localized, with: "") ?? "")"
            var roomNo = "\(model.roomNo?.replacingOccurrences(of: "Room No ".localized, with: "") ?? "")"
            var floorNo = "\(model.floorNo?.replacingOccurrences(of: "Floor No ".localized, with: "") ?? "")"
            var address1 = "\(model.address1 ?? "")"
            var address2 = "\(model.address2 ?? "")"
            var city = "\(model.cityName ?? "")"
            var state = "\(model.stateProvinceName ?? "")"
            
            if (model.houseNo != "" && model.houseNo != nil) && (model.roomNo != "" && model.roomNo != nil) && (model.floorNo != "" && model.floorNo != nil){
                if houseNo != "" {
                    houseNo = ""
                    houseNo.append("House No ".localized + ": \(model.houseNo?.replacingOccurrences(of: "House No ", with: "").replacingOccurrences(of: "အိမ္ယာနံပါတ္", with: "").replacingOccurrences(of: "အိမ်အမှတ်", with: "") ?? "")")
                   // houseNo.replacingOccurrences(of: "အိမ္ယာနံပါတ္", with: "")
                    houseNo.append(", ")
                }
                if roomNo != "" {
                    roomNo = ""
                    roomNo.append("Room No ".localized+": \(model.roomNo?.replacingOccurrences(of: "Room No ", with: "").replacingOccurrences(of: "အခန္းနံပါတ္", with: "").replacingOccurrences(of: "အခန်းအမှတ်", with: "") ?? "")")
                    roomNo.append(", ")
                }
                if floorNo != "" {
                    floorNo = ""
                    floorNo.append("Floor No ".localized + ": \(model.floorNo?.replacingOccurrences(of: "Floor No ", with: "").replacingOccurrences(of: "အထပ္နံပါတ္", with: "").replacingOccurrences(of: "အထပ်နံပါတ်", with: "") ?? "")")
                    floorNo.append(", ")
                    
                }
                if address1 != "" {
                    address1 = ""
                    address1.append("\(model.address1 ?? "")")
                    address1.append(", ")
                }
                if address2 != "" {
                    address2 = ""
                    address2.append("\(model.address2 ?? "")")
                    address2.append(", ")
                }
                if city != "" {
                    city = ""
                    city.append("\(model.cityName ?? "")")
                    city.append(", ")
                }
                if state != "" {
                    state = ""
                    state.append("\(model.stateProvinceName ?? "")")
                }
                self.addressLabel.text = "\(houseNo)\(roomNo)\(floorNo)\n\(address1)\(address2)\n\(city)\(state)"
            }else{
                self.addressLabel.text = "\(address1)\(address2)\n\(city)\(state)"
            }
            
            if let number = model.phoneNumber, number.hasPrefix("0095") {
                self.addressPhLabel.text = (number as NSString).replacingCharacters(in: NSRange(location: 0, length: 4), with: "+95 ")
            }else {
                self.addressPhLabel.text = model.phoneNumber ?? ""
            }
        }
    }
    
    func buttonStateManage(locationModel: Store?, indexpath:IndexPath, selectedIndPath: IndexPath){
        self.deliveryBtn.tag = indexpath.row
        self.pickupBtn.tag = indexpath.section
        if indexpath == selectedIndPath && Cart.PickupAddress == nil {
            self.deliveryBtn.setImage(UIImage(named: "radio"), for: .normal)
            self.pickupBtn.setImage(UIImage(named: "act_radio"), for: .normal)
        }
        else {
            if (locationModel != nil) {
                self.deliveryBtn.setImage(UIImage(named: "radio"), for: .normal)
                self.pickupBtn.setImage(UIImage(named: "act_radio"), for: .normal)
            } else {
                self.deliveryBtn.setImage(UIImage(named: "act_radio"), for: .normal)
                self.pickupBtn.setImage(UIImage(named: "radio"), for: .normal)
            }
        }
        
        //        if (locationModel != nil){
        //            self.pickPointlocBtn.isHidden = true
        //            self.pickupBtn.setImage(UIImage(named: "act_radio"), for: .normal)
        //            self.deliveryBtn.setImage(UIImage(named: "radio"), for: .normal)
        //        }
        //        else{
        //            self.pickPointlocBtn.isHidden = false
        //            //            self.pickupBtn.setImage(UIImage(named: "radio"), for: .normal)
        //            //            self.deliveryBtn.setImage(UIImage(named: "act_radio"), for: .normal)
        //        }
    }
    
    func wraplocationData(locationModel: Store?) {
        if let model = locationModel {
            self.LocaddressNameLbl.text = model.name as String
            self.LocaddressLabel.text = "\(model.storeDescription)"
            //            self.LocaddressPhLabel.text = "\(model.city as String) Township"
            println_debug(model)
            
            
        }
    }
    
    func paymentCellUpdate(paymentData: ShoppingCartInfo) {
        self.subTotalLbl.text = paymentData.subTotal ?? ""
        self.subTotalLbl.amountAttributedString()
        if paymentData.shipping == "0 MMK"{
            self.shippingLbl.text = "Free".localized
        }
        else{
            
            // changes avaneesh
            
            if let amount = AppUtility.getTheAmountFromString(amount: paymentData.shipping!) as? Double, Int(amount) ?? -1 > 0 {
                self.shippingLbl.text = paymentData.shipping  ?? "0"
                self.shippingLbl.amountAttributedString()
            }
            else {
                self.shippingLbl.text = "Free".localized
            }
        }
        
        if let ta1xStr = paymentData.tax?.replacingOccurrences(of: " MMK", with: "") {
            if ta1xStr == "0" {
                self.taxValueLbl.text = ""
                self.taxLbl.text = ""
            } else {
                self.taxValueLbl.text = paymentData.tax ?? ""
                self.taxValueLbl.amountAttributedString()
            }
        }
        
        //        self.totalPriceLabel.text    = paymentData.orderTotal  ?? ""
        
        var productPrice = paymentData.orderTotal?.replacingOccurrences(of: "MMK", with: "")
        productPrice = productPrice! + mmkText
        
        let attrString = NSMutableAttributedString(string: productPrice!)
        let nsRange = NSString(string: productPrice!).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
        attrString.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange)
        self.totalPriceLabel.attributedText = attrString
        
        //        self.totalPriceLabel.amountAttributedString()
    }
}

extension Cart: VMAddNewAddressProtocol {
    func addNewAddress() {
        self.newBillingAddress = nil
        self.allBillingAddresses.removeAll()
        self.getBillForm()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            let indexpath = IndexPath(item: 0, section: 0)
            self.cartTbl.reloadRows(at: [indexpath], with: .none)
        }
    }
}
