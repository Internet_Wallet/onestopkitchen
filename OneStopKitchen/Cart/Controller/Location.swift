//
//  Location.swift
//  VMart
//
//  Created by ANTONY on 27/10/2018.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit

class Location: MartBaseViewController {
    
    var aPIManager          : APIManager?
    var allAvailableStores = [Store]()
    weak var Delegate: LocationDelegate?
    
    @IBOutlet var locationTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Select Pickup Point Location".localized
        self.aPIManager = APIManager()
        self.loadLocation()
        // Do any additional setup after loading the view.
    }
    
    func loadLocation(){
        AppUtility.showLoading(self.view)
        self.aPIManager?.getStores(onSuccess: { [weak self] stores in
            if let viewLoc = self?.view {
                AppUtility.hideLoading(viewLoc)
            }
            self?.allAvailableStores = stores
            self?.locationTableView.reloadData()
            if stores.count <= 0 {
                //                AppUtility.showToast("Location Not Found!", view: self.view)
                // AppUtility.showToastCustomBlackInView(message: "There is No Any Pickup Point at Your Location.".localized, controller: self!)
            }
            }, onError: { [weak self] (message) in
                if let viewLoc = self?.view, let controller = self {
                    AppUtility.hideLoading(viewLoc)
                    AppUtility.showToastCustomBlackInView(message: message ?? "", controller: controller)
                }
        })
    }
}

extension Location: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if self.allAvailableStores.count == 0 {
            let noDataLabel = UILabel(frame: tableView.bounds)
            noDataLabel.text = "There is No Any Pickup Point at Your Location.".localized
            noDataLabel.font = UIFont(name: appFont , size: 18)
            noDataLabel.textAlignment = .center
            noDataLabel.numberOfLines = 5
            tableView.backgroundView = noDataLabel
        } else {
            tableView.backgroundView = nil
        }
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.allAvailableStores.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LocationCell", for: indexPath) as! LocationTableViewCell
        cell.wrapData(object: self.allAvailableStores[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.SelectedPickupLocation(object: self.allAvailableStores[indexPath.row])
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }
    
    func SelectedPickupLocation(object:Store) {
        self.Delegate?.SelectedPickupLocationClick(object: object)
    }
}

protocol LocationDelegate: class {
    func SelectedPickupLocationClick(object:Store)
}

class LocationTableViewCell: UITableViewCell {
    
    @IBOutlet var placeLbl: UILabel! {
        didSet {
            self.placeLbl.font = UIFont(name: appFont, size: 15.0)
            self.placeLbl.text = self.placeLbl.text?.localized
            
        }
    }
    @IBOutlet var addressLbl: UILabel! {
        didSet {
            self.addressLbl.font = UIFont(name: appFont, size: 15.0)
            addressLbl?.text = addressLbl?.text?.localized
            
        }
    }
    @IBOutlet var locationLbl: UILabel! {
        didSet {
            self.locationLbl.font = UIFont(name: appFont, size: 15.0)
            locationLbl?.text = locationLbl?.text?.localized
            
        }
    }
    
    @IBOutlet var KmLbl: UILabel! {
        didSet {
            self.KmLbl.font = UIFont(name: appFont, size: 15.0)
            KmLbl?.text = KmLbl?.text?.localized
            
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func wrapData(object:Store) {
        self.placeLbl.text = object.name as String
        self.addressLbl.text = object.storeDescription as String
        self.locationLbl.text = (object.openingHours) as String
        self.KmLbl.text = (object.Distance) as String
        println_debug(object)
    }
}
