//
//  ProductSubCategories.swift
//  VMart
//
//  Created by Shobhit Singhal on 10/16/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit

//struct SubCategoryClass {
//    static var subCategoriesArray : [FeaturedProductsAndCategory]!
//}

class ProductSubCategories: MartBaseViewController {
    
    var aPIManager                  = APIManager()
    var apiManagerClient            : APIManagerClient!
    var ParentCategoryIdState                  : Bool!
    var PreviousCategoryId                  : NSInteger!
    var categoryId                  : NSInteger!
    var subCategoriesArray : [FeaturedProductsAndCategory] = []
    var PreviouSubCategoriesArray          : [FeaturedProductsAndCategory] = []
    var categoryName: String = ""
    var initialTouchPoint: CGPoint = CGPoint(x: 0,y: 0)
    var Root : String? = ""
     var block: DispatchWorkItem?
    @IBOutlet var cartBarButton: UIBarButtonItem!
    @IBOutlet weak var subCategoriesTableView: UITableView!
    @IBOutlet weak var subCategoriesCollectionView: UICollectionView!
    @IBOutlet weak var backShadedView: UIView!
    @IBOutlet weak var subCategoriesContainerView: UIView!
    @IBOutlet weak var dismissRightView: UIView!
    @IBOutlet weak var backBottomBtn: UIButton!{
        didSet {
            //            backBottomBtn.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
            backBottomBtn.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1), UIColor.init(red: 20.0/255.0, green: 185.0/255.0, blue: 85.0/255.0, alpha: 1.0)])
            backBottomBtn.setTitle((backBottomBtn.titleLabel?.text ?? "").localized, for: .normal)
            backBottomBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //        self.cartBarButton = UIBarButtonItem.init(badge: UserDefaults.standard.value(forKey: "badge") as? String, title: "", target: self, action: #selector(self.cartAction(_:)))
        self.cartBarButton = UIBarButtonItem.init(badge: UserDefaults.standard.string(forKey: "badge") ?? "", title: "", target: self, action: #selector(self.cartAction(_:)))
        self.navigationItem.rightBarButtonItem  = self.cartBarButton
        self.navigationItem.setMarqueLabelInNavigation(TextStr: self.navigationItem.title!, count: self.navigationItem.title?.count ?? 40)
        if AppUtility.isConnectedToNetwork() {
            self.backFromProductListing()
        } else {
            AppUtility.showInternetErrorToast(AppConstants.InternetErrorText.message, view: self.view)
        }
        if Root == "dashboard" {
            self.subCategoriesTableView.isHidden = true
            self.subCategoriesTableView.superview?.isHidden = true
            // self.disappearSubCategoriesContainerView()
            
        }
        else {
            self.subCategoriesTableView.isHidden = false
        }
        
        
        self.subMenuHideAutomatically()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        subCategoriesArray = []
        self.subCategoriesCollectionView.register(UINib(nibName: "SubCategoriesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SubCategoryCell")
        subCategoriesCollectionView.delegate = self
        subCategoriesCollectionView.dataSource = self
        subCategoriesTableView.delegate = self
        subCategoriesTableView.dataSource = self
        subCategoriesTableView.estimatedRowHeight = 44
        subCategoriesTableView.rowHeight = UITableView.automaticDimension
        self.title = categoryName.localized
        //        self.subCategoriesTableView.layer.cornerRadius = 10
        //        self.subCategoriesTableView.layer.shadowOpacity = 0.40
        //        self.subCategoriesTableView.layer.shadowOffset = CGSize(width: 0, height: 0)
        //        self.subCategoriesTableView.layer.shadowRadius = 6
        //        self.subCategoriesTableView.layer.shadowColor = UIColor.black.cgColor
        //        self.subCategoriesTableView.layer.masksToBounds = false
        self.backShadedView.layer.cornerRadius = 0
        self.backShadedView.layer.shadowOpacity = 0.40
        self.backShadedView.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.backShadedView.layer.shadowRadius = 6
        self.backShadedView.layer.shadowColor = UIColor.black.cgColor
        self.backShadedView.layer.masksToBounds = false
        self.backBottomBtn.layer.cornerRadius = 0
        self.apiManagerClient = APIManagerClient.sharedInstance
        //self.categoryId =  1065
        if self.categoryId != nil{
            if AppUtility.isConnectedToNetwork() {
                self.ParentCategoryIdState = true
                self.loadSubCategories()
            } else {
                AppUtility.showInternetErrorToast(AppConstants.InternetErrorText.message, view: self.view)
            }
        }
    }
    
    @IBAction func cartAction(_ sender: UIBarButtonItem) {
        let viewController = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "CartRoot")
        self.present(viewController, animated: true, completion: nil)
    }
    
    @IBAction func searchAction(_ sender: UIBarButtonItem) {
        
        let viewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchProducts")
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    @IBAction func btnBackClick(_ sender: UIButton) {
        if self.ParentCategoryIdState {
            self.dismiss(animated: true, completion: nil)
        }  else {
            self.ParentCategoryIdState = true
            self.categoryId = self.PreviousCategoryId
            if AppUtility.isConnectedToNetwork() {
                self.loadSubCategories()
            } else {
                AppUtility.showInternetErrorToast(AppConstants.InternetErrorText.message, view: self.view)
            }
        }
    }
    
    func backFromProductListing() {
        if self.ParentCategoryIdState{
            //            self.dismiss(animated: true, completion: nil)
        }  else {
            self.ParentCategoryIdState = true
            self.categoryId = self.PreviousCategoryId
            if AppUtility.isConnectedToNetwork() {
                self.loadSubCategories()
            } else {
                AppUtility.showInternetErrorToast(AppConstants.InternetErrorText.message, view: self.view)
            }
        }
    }
    
    @IBAction func dismissSubCategoryListViewTapAction(_ sender: Any) {
        self.disappearSubCategoriesContainerView()
    }
    
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func disappearSubCategoriesContainerView() {
        
        
        
        self.subCategoriesContainerView.animateTo(frame: CGRect(x:  (-self.subCategoriesContainerView.frame.width) + 20, y: self.subCategoriesContainerView.frame.origin.y, width: self.subCategoriesContainerView.frame.width, height: self.subCategoriesContainerView.frame.height), withDuration: 0.7,completion:{(finished : Bool)  in
            if (finished) {
                
            }
        })
        
        
        //                self.dismissRightView.isHidden = true
        //                UIView.animate(withDuration: 0.20, animations: {
        //                    self.subCategoriesContainerView.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        //                    self.subCategoriesContainerView.alpha = 0.0;
        //                }, completion:{(finished : Bool)  in
        //                    if (finished) {
        //                        //self.subCategoriesContainerView.removeFromSuperview()
        //                    }
        //                })
    }
    
    func loadSubCategories() {
        AppUtility.showLoading(self.view)
        //        subCategoriesArray.removeAll()
        self.aPIManager.loadFeaturedProductsAndCategory(self.categoryId, onSuccess: { [weak self] getSubCategories in
            //            self?.subCategoriesArray = getSubCategories
            if let viewLoc = self?.view {
                AppUtility.hideLoading(viewLoc)
            }
            if getSubCategories.count > 0 {
                self?.subCategoriesArray = getSubCategories
                self?.PreviouSubCategoriesArray = getSubCategories
                self?.subCategoriesTableView.reloadData()
                self?.subCategoriesCollectionView.reloadData()
                self?.subCategoriesTableView.animate()
            } else {
                self?.ParentCategoryIdState = true
                self?.loadCategoryDetails()
            }
            },onError: { [weak self] message in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                }
        })
        
    }
    
    func loadCategoryDetails() {
        AppUtility.showLoading(self.view)
        var paramDic            = [String:AnyObject]()
        paramDic["pagenumber"]  = 1 as AnyObject
        self.aPIManager.loadCategoryDetails(false, categoryId: self.categoryId, params: paramDic, onSuccess:{ [weak self] getProducts in
            if let viewLoc = self?.view {
                AppUtility.hideLoading(viewLoc)
            }
            if getProducts.productsInfo.count == 0 {
                self?.showAlert(alertTitle: "Information".localized, description: "No products found".localized)
            } else {
                self?.performSegue(withIdentifier: "SubCategoryToProductCollectionSegue", sender: self)
            }
            }, onError: { [weak self] message in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                }
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SubCategoryToProductCollectionSegue" {
            if let controller = segue.destination as? UINavigationController {
                if let vc = controller.viewControllers.first as? ProductCollections {
                    vc.categoryId = self.categoryId
                    vc.categoryName = self.categoryName
                    vc.PreviouSubCategoriesArray = self.PreviouSubCategoriesArray
                }
            }
        }
    }
    
    @IBAction func swipeLeft(_ sender: UISwipeGestureRecognizer) {
        if sender.direction == UISwipeGestureRecognizer.Direction.left && self.subCategoriesContainerView.frame.origin.x == 0{
            println_debug("Swipe Left")
            self.subCategoriesContainerView.animateTo(frame: CGRect(x: (-self.subCategoriesContainerView.frame.width) + 20, y: self.subCategoriesContainerView.frame.origin.y, width: self.subCategoriesContainerView.frame.width, height: self.subCategoriesContainerView.frame.height), withDuration: 0.7)
        }
    }
    
    @IBAction func swipeRight(_ sender: UISwipeGestureRecognizer) {
        if sender.direction == UISwipeGestureRecognizer.Direction.right && self.subCategoriesContainerView.frame.origin.x != 0 {
            println_debug("Swipe Right")
            self.subCategoriesContainerView.animateTo(frame: CGRect(x: 0, y: self.subCategoriesContainerView.frame.origin.y, width: self.subCategoriesContainerView.frame.width, height: self.subCategoriesContainerView.frame.height), withDuration: 0.7)
        }
        self.subMenuHideAutomatically()
        
    }
    
    func subMenuHideAutomatically(){
        
            self.block?.cancel()
            self.block = DispatchWorkItem {
             self.subCategoriesContainerView.animateTo(frame: CGRect(x: (-self.subCategoriesContainerView.frame.width) + 20, y: self.subCategoriesContainerView.frame.origin.y, width: self.subCategoriesContainerView.frame.width, height: self.subCategoriesContainerView.frame.height), withDuration: 0.7)
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(3), execute: self.block!)
            
        
    }
    
    
    
    @IBAction func panGestureRecognizerHandler(_ sender: UIPanGestureRecognizer) {
        self.subCategoriesContainerView.fadeIn()
    }
}

extension ProductSubCategories: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subCategoriesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SubCategoryCell", for: indexPath) as! SubCategoriesTableViewCell
        cell.subCategoriesLbl.text = subCategoriesArray[indexPath.row].name
        return cell
    }
    
    func TapSubcategoary(indexPath: IndexPath)  {
        self.ParentCategoryIdState = false
        self.PreviousCategoryId = self.categoryId
        self.categoryId = subCategoriesArray[indexPath.row].idd
        self.categoryName = subCategoriesArray[indexPath.row].name
        if AppUtility.isConnectedToNetwork() {
            self.loadSubCategories()
        } else {
            AppUtility.showInternetErrorToast(AppConstants.InternetErrorText.message, view: self.view)
        }
        self.subMenuHideAutomatically()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.TapSubcategoary(indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension ProductSubCategories: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return subCategoriesArray.count
        //        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = subCategoriesCollectionView.dequeueReusableCell(withReuseIdentifier: "SubCategoryCell", for: indexPath) as! SubCategoriesCollectionViewCell
        let currentObject = subCategoriesArray[indexPath.row]
        cell.updateCellWithData(FeaturedProductsAndCategoryData: currentObject, index : indexPath.row)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.TapSubcategoary(indexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemWidth = HomeConstant.getItemWidth(boundWidth: collectionView.bounds.size.width)
        return CGSize(width: itemWidth, height: itemWidth + 20)
    }
}

class SubCategoriesTableViewCell: UITableViewCell {
    @IBOutlet var subCategoriesLbl: UILabel!{
        didSet {
            self.subCategoriesLbl.font = UIFont(name: appFont, size: 15.0)
            self.subCategoriesLbl.text = self.subCategoriesLbl.text?.localized
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func wrapSubCategoary(name:String){
        self.subCategoriesLbl.text = name
    }
}
