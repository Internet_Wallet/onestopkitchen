//
//  CustomAudio.swift
//  VMart
//
//  Created by Mohit on 7/18/19.
//  Copyright © 2019 Shobhit. All rights reserved.
//

import UIKit
import AVFoundation

protocol AudioProcess : class {
    func updateTheAudioFile (audioURL : URL)
}

class CustomAudio: MartBaseViewController  {
    
    weak var delegateAudio : AudioProcess?
    
    var countdownTimer: Timer!
    var meterTimer:Timer!
    
    // Audio Recording
    var recordingSession : AVAudioSession!
    var audioRecorder    :AVAudioRecorder!
    var audioPlayer      :AVAudioPlayer!
    var settings         = [String : Int]()
    
    @IBOutlet weak var timerLabel: UILabel!{
        didSet{
            self.timerLabel.font = UIFont(name: appFont, size: 15.0)
            self.timerLabel.text = self.timerLabel.text?.localized
        }
    }
    @IBOutlet weak var timerSeekBar: UIProgressView!
    @IBOutlet var ViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var btnHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet var PlayPauseButton: UIButton! {
        didSet{
            PlayPauseButton.isSelected = false
        }
        
    }
    @IBOutlet var recordButton: UIButton! {
        didSet{
            recordButton.isSelected = false
        }
        
    }
    @IBOutlet var bottomView: UIView!{
        didSet{
            bottomView.isHidden = true
        }
    }
    @IBOutlet var dismissButton: UIButton!{
        didSet{
            dismissButton.isHidden = true
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // force any pending operations to finish
        self.ViewHeightConstraint.constant = 0
        self.btnHeightConstraint.constant = 15
        self.AudioSessionConfigure()
        self.view.layoutIfNeeded()
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func dismissTapAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: {
            self.delegateAudio?.updateTheAudioFile(audioURL: self.directoryURL()! as URL)
        })
    }
    
    
    @IBAction func startAndStopRecordAction(_ sender: UIButton) {
        print(sender.isSelected)
        
        if sender.isSelected {
            sender.setImage(UIImage.init(named: "microphone"), for: UIControl.State.normal)
            dismissButton.isHidden = false
            sender.isSelected = false
            self.finishRecording(success: true)
            
        }else{
            
            UIView.animate(withDuration: 0.2, animations: { () -> Void in
                sender.setImage(UIImage.init(named: "stop"), for: UIControl.State.normal)
                self.dismissButton.isHidden = true
                self.ViewHeightConstraint.constant = 0
                self.btnHeightConstraint.constant = 15
                self.bottomView.isHidden = true
                //                sender.isHidden = true
                self.view.layoutIfNeeded()
            })
            sender.isSelected = true
            if audioRecorder == nil {
                self.startRecording()
                meterTimer = Timer.scheduledTimer(timeInterval: 0.1, target:self, selector:#selector(self.startTimer(timer:)), userInfo:nil, repeats:true)
            }
        }
        
    }
    
    
    @IBAction func playAndPauseAudioAction (_ sender: UIButton) {
        
        if sender.isSelected {
            
            UIView.animate(withDuration: 0.2, animations: { () -> Void in
                sender.setImage(UIImage.init(named: "play"), for: UIControl.State.normal)
                self.view.layoutIfNeeded()
            })
            sender.isSelected = false
            self.pauseRecording()
            
        }else{
            UIView.animate(withDuration: 0.2, animations: { () -> Void in
                sender.setImage(UIImage.init(named: "pause"), for: UIControl.State.normal)
                self.view.layoutIfNeeded()
            })
            sender.isSelected = true
            self.preparePlayer()
            self.playRecording()
        }
    }
    
}

extension CustomAudio{
    
    @objc func startTimer (timer: Timer) {
        if audioRecorder != nil {
            if audioRecorder.isRecording
            {
                let min = Int(audioRecorder.currentTime / 60)
                let sec = Int(audioRecorder.currentTime.truncatingRemainder(dividingBy: 60))
                let totalTimeString = String(format: "%02d:%02d", min, sec)
                timerLabel.text = totalTimeString
                audioRecorder.updateMeters()
            }
        }
        else {
            if audioPlayer != nil  || audioPlayer.isPlaying {
                let min = Int(audioPlayer.currentTime / 60)
                let sec = Int(audioPlayer.currentTime.truncatingRemainder(dividingBy: 60))
                let totalTimeString = String(format: "%02d:%02d", min, sec)
                timerLabel.text = totalTimeString
                timerSeekBar.progress = Float(audioPlayer.currentTime) / Float(audioPlayer.duration)
                // increment the counter
            }
        }
        
    }
    
}

extension CustomAudio: AVAudioRecorderDelegate , AVAudioPlayerDelegate {
    
    
    func playRecording() { self.audioPlayer.play() }
    
    func pauseRecording() { self.audioPlayer.pause() }
    
    func preparePlayer() {
        var error: NSError?
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: directoryURL()! as URL)
        } catch let error1 as NSError {
            error = error1
            audioPlayer = nil
        }
        if let err = error {
            print("AVAudioPlayer error: \(err.localizedDescription)")
        } else {
            audioPlayer.delegate = self
            audioPlayer.prepareToPlay()
            audioPlayer.volume = 10.0
        }
        meterTimer = Timer.scheduledTimer(timeInterval: 0.1, target:self, selector:#selector(self.startTimer(timer:)), userInfo:nil, repeats:true)
    }
    
    
    func AudioSessionConfigure(){
        recordingSession = AVAudioSession.sharedInstance()
        do {
            try recordingSession.setCategory(AVAudioSession.Category.playAndRecord)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        print("Allow")
                    } else {
                        print("Dont Allow")
                    }
                }
            }
        } catch {
            print("failed to record!")
        }
        
        // Audio Settings
        
        settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
    }
    
    
    func directoryURL() -> NSURL? {
        let fileManager = FileManager.default
        let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectory = urls[0] as NSURL
        let soundURL = documentDirectory.appendingPathComponent("sound.m4a")
        print(soundURL!)
        return soundURL as NSURL?
    }
    
    
    func startRecording() {
        let audioSession = AVAudioSession.sharedInstance()
        do {
            audioRecorder = try AVAudioRecorder(url: self.directoryURL()! as URL,
                                                settings: settings)
            audioRecorder.delegate = self
            audioRecorder.prepareToRecord()
        } catch {
            finishRecording(success: false)
        }
        do {
            try audioSession.setActive(true)
            audioRecorder.record()
        } catch {
        }
    }
    
    func finishRecording(success: Bool) {
        audioRecorder.stop()
        audioRecorder = nil
        meterTimer.invalidate()
        if success {
            //self.showAlert(alertTitle: "Audio", description: "Successfully record")
            print(success)
            self.view.layoutIfNeeded() // force any pending operations to finish
            UIView.animate(withDuration: 0.2, animations: { () -> Void in
                self.ViewHeightConstraint.constant = 52
                self.btnHeightConstraint.constant = 69
                self.bottomView.isHidden = false
                //                sender.isHidden = true
                self.view.layoutIfNeeded()
            })
        } else {
            print("Somthing Wrong.")
        }
    }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag {
            finishRecording(success: false)
        }
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        
        UIView.animate(withDuration: 0.2, animations: { () -> Void in
            self.PlayPauseButton.setImage(UIImage.init(named: "play"), for: UIControl.State.normal)
            self.view.layoutIfNeeded()
            self.meterTimer.invalidate()
            self.timerSeekBar.progress = 0.0
        })
        self.PlayPauseButton.isSelected = false
    }
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        print("Error while playing audio \(error!.localizedDescription)")
    }
    
}
