//
//  UIImageView+Extensions.swift
//  NopCommerce
//
//  Created by Mubassher on 10/25/16.
//  Copyright © 2016 bs23. All rights reserved.
//

import UIKit
import SDWebImage

extension UIImageView {
    
    func setImageUsingUrl(_ imageUrl: String?) {
        if let url = imageUrl {
            self.sd_setImage(with: URL(string: url), placeholderImage:nil)
        }
    }
    
}
