//
//  HomeBannerCollectionViewCell.swift
//  CollectionInTable
//
//  Created by Shobhit Singhal on 10/11/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit

class HomeBannerCollectionViewCell: UICollectionViewCell {
    
    
    var ProdOrCatId: String?
    var CategoryName: String?
    
    @IBOutlet var bannerImageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //    func updateCellWithImage(name:String) {
    //        self.cellImageName = name
    //        self.cellImageView.image = UIImage(named: name)
    //    }
    
    func updateData(object: HomeBanner){
        if let img = object.ImageUrl{
            print(img)
            self.bannerImageView.sd_setImage(with: URL(string: img))
            //            if let imgX = self.imageResize(image: self.bannerImageView.image ?? UIImage(), sizeChange: bannerImageView.frame.size) as? UIImage {
            //                self.bannerImageView.image = imgX
            //            }
            
            //            AppUtility.NKPlaceholderImage(image: UIImage(named: img), imageView: self.bannerImageView, imgUrl: img) { (image) in }
            
            
            //            AppUtility.getData(from: URL(string: img)!) { data, response, error in
            //                guard let data = data, error == nil else { return }
            //                print("Download Finished")
            //                DispatchQueue.main.async() {
            //                    self.bannerImageView.image = UIImage(data: data)
            //                }
            //            }
        }
        
        self.ProdOrCatId = object.ProdOrCatId
        self.CategoryName = object.CategoryName
        
    }
    
    
    func imageResize (image :UIImage, sizeChange :CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        guard let scaledImage = UIGraphicsGetImageFromCurrentImageContext() else { return UIImage() }
        return scaledImage
        
    }
    
    
}
