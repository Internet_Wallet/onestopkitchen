//
//  CategoryRowsTableViewCell.swift
//  CollectionInTable
//
//  Created by Shobhit Singhal on 10/12/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit
import Alamofire

protocol ProductsCollectionCellDelegate:class {
    func tapOnProductCollectionView(collectioncell:ProductsCollectionViewCell?)
    func tapOnManufacturedProductstCollectionView(collectioncell:ProductsCollectionViewCell?)
    func tapOnRecentViwedProductstCollectionView(collectioncell:ProductsCollectionViewCell?)
}

class CategoryRowsTableViewCell: UITableViewCell {
    
    weak var cellDelegate:ProductsCollectionCellDelegate?
    var request: Alamofire.Request?
    var collectionData: NSArray!
    var apiManagerClient            = APIManagerClient()
    var homePageProductArray        = [GetHomePageProducts]()
    var homePageMenuFactureArray    = [HomePageMenuFacture]()
    var homePageCategoriesModels    = [HomePageCategoryModel]()
    var recentViewProductArray        = [GetHomePageProducts]()
    var array = NSArray()
    var sectionIndex = NSInteger()
    
    
    @IBOutlet weak var productsCollectionView: UICollectionView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if let layout = productsCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.sectionInset = UIEdgeInsets(
                top: HomeConstant.offset,    // top
                left: HomeConstant.offset,    // left
                bottom: HomeConstant.offset,    // bottom
                right: HomeConstant.offset     // right
            )
            layout.minimumInteritemSpacing = HomeConstant.minItemSpacing
            layout.minimumLineSpacing = HomeConstant.minLineSpacing
        }
        productsCollectionView.isScrollEnabled = false
    }
    
    func getCategoryArray(dataArray: NSArray , sectionIndex:NSInteger) -> Void {
        self.apiManagerClient = APIManagerClient.sharedInstance
        self.sectionIndex = sectionIndex
        self.array = dataArray
        self.productsCollectionView?.reloadData()
    }
}

extension CategoryRowsTableViewCell: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.sectionIndex == 0 {
            return self.array.count
        }
        else if self.sectionIndex == self.apiManagerClient.categoryArray.count{
            //            if self.apiManagerClient.categoryArray.count == 5{
            //                print(self.apiManagerClient.categoryArray.count)
            //                return self.array.count
            //            }
            //            if self.apiManagerClient.categoryArray.count > 4{
            //                return 4
            //            }
            //            else{
            //                return self.array.count
            //            }
            return self.array.count
        }
            
            
        else if self.sectionIndex == self.apiManagerClient.categoryArray.count-1 {
            return self.array.count
        } else {
            if let arrayModel = array as? [HomePageCategoryModel] {
                self.homePageCategoriesModels = arrayModel
                return self.homePageCategoriesModels[0].products.count
            }
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = productsCollectionView.dequeueReusableCell(withReuseIdentifier: "ProductsCell", for: indexPath) as! ProductsCollectionViewCell
        if self.sectionIndex == 0 {
            if let arrayModel = array as? [GetHomePageProducts] {
                self.homePageProductArray = arrayModel
                cell.updateCellWithData(homeCollectionData: homePageProductArray, index : indexPath.item)
            }
        } else if self.sectionIndex == self.apiManagerClient.categoryArray.count-1 {
            if let arrayModel = array as? [HomePageMenuFacture] {
                self.homePageMenuFactureArray = arrayModel
                cell.updateCellWithData(homeCollectionData: homePageMenuFactureArray, index : indexPath.item)
            }
        }
            
        else if self.sectionIndex == self.apiManagerClient.categoryArray.count {
            if let arrayModel = array as? [GetHomePageProducts] {
                self.recentViewProductArray = arrayModel
                cell.updateCellWithData(homeCollectionData: self.recentViewProductArray, index : indexPath.item)
            }
        }
            
            
        else {
            if let arrayModel = array as? [HomePageCategoryModel] {
                self.homePageCategoriesModels = arrayModel
                cell.updateCellWithData(homeCollectionData: homePageCategoriesModels, index : indexPath.item)
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? ProductsCollectionViewCell
        if self.sectionIndex == 0 {
            self.cellDelegate?.tapOnProductCollectionView(collectioncell: cell)
        } else if self.sectionIndex == self.apiManagerClient.categoryArray.count-1 {
            self.cellDelegate?.tapOnManufacturedProductstCollectionView(collectioncell: cell)
        }
        else if self.sectionIndex == self.apiManagerClient.categoryArray.count{
            self.cellDelegate?.tapOnRecentViwedProductstCollectionView(collectioncell: cell)
        }
        else {
            self.cellDelegate?.tapOnProductCollectionView(collectioncell: cell)
        }
    }
}

extension CategoryRowsTableViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if sectionIndex == self.apiManagerClient.categoryArray.count-1 {
            let itemWidth = HomeConstant.getItemWidth(boundWidth: collectionView.bounds.size.width)
            return CGSize(width: itemWidth, height: itemWidth)
        }
        else if self.sectionIndex == self.apiManagerClient.categoryArray.count{
            let itemWidth = HomeConstant.getItemWidth(boundWidth: collectionView.bounds.size.width)
            return CGSize(width: itemWidth, height: itemWidth + 40)
        }
            
        else {
            let itemWidth = HomeConstant.getItemWidth(boundWidth: collectionView.bounds.size.width)
            return CGSize(width: itemWidth, height: itemWidth + 40)
        }
    }
}

