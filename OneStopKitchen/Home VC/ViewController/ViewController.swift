//
//  ViewController.swift
//  VMart
//
//  Created by Shobhit Singhal on 10/15/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit
import ZBarSDK
import Material
import Motion

class HomeConstant {
    static let totalItem: CGFloat = 4
    static let column: CGFloat = 2
    static let minLineSpacing: CGFloat = 1.0
    static let minItemSpacing: CGFloat = 1.0
    static let offset: CGFloat = 1.0 // TODO: for each side, define its offset
    
    static func getItemWidth(boundWidth: CGFloat) -> CGFloat {
        let totalWidth = boundWidth - (offset + offset) - ((column - 1) * minItemSpacing)
        return totalWidth / column
    }
}

extension ZBarSymbolSet: Sequence {
    public func makeIterator() -> NSFastEnumerationIterator {
        return NSFastEnumerationIterator(self)
    }
}

protocol KYDrawerCustomDelegate: class {
    
    func resetAllData()
}


class ViewController: MartBaseViewController, ZBarReaderDelegate {
    
    weak var KYDCDelegate: KYDrawerCustomDelegate?
    var ServerResponse : UpdationModel?
    var isLogoutTrue : Bool = false
    var alertVC : SAlertController?
    var ZBarReader: ZBarReaderViewController?
    let reader: ZBarReaderViewController = ZBarReaderViewController()
    var parentCategoryArray     = [GetAllCategories]()
    var pagehomeBanner          = [HomeBanner]()
    //var pageViewController: UIPageViewController!
    var pageImages              = [NSString]()
    var aPIManager = APIManager()
    var apiManagerClient: APIManagerClient?
    var homePageProductArray        = [GetHomePageProducts]()
    var recentViewProductArray        = [GetHomePageProducts]()
    var homePageMenuFactureArray    = [HomePageMenuFacture]()
    var homePageCategoriesModels    = [HomePageCategoryModel]()
    
    var pagerControl        = UIPageControl()
    var isFirstTime : Bool  = true
    var firstTimeSliderList : [String] = []
    var hasProductInFeatureProductsArray : Bool = true
    let group = DispatchGroup()
    var bannerCollectionView: UICollectionView! = nil
    var pageControl: UIPageControl? = nil
    var refreshControl: UIRefreshControl!
    var categoryId: NSInteger!
    var categoryName: String = ""
    var timer:Timer?
    var productId = 0
    var productName = ""
    var isTapOnManufactureProductFlag: Bool  = true
    let appDel = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var contentTableView:    UITableView!
    @IBOutlet weak var searchContainerView: UIView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet var menuBarButton: UIBarButtonItem!
    @IBOutlet var cartBarButton: UIBarButtonItem!
    @IBOutlet weak var searchLbl: UILabel!{
        didSet {
            self.searchLbl.font = UIFont(name: appFont, size: 15.0)
            self.searchLbl.text = self.searchLbl.text?.localized
            
        }
    }
    @IBOutlet weak var scanButton: UIButton!
    //        {
    //        didSet {
    //            scanButton.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
    //        }
    //    }
    @IBOutlet weak var contentTableViewBottomConstraint: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewDidLoadCall()
        self.setNavigationTitle()
        self.navigationItem.title = "1 Stop Mart".localized
        let uuid = UUID().uuidString
        print("UUID String is : %@", uuid)
        let (networkCode, carrierName) = appDel.getNetworkCode()
        UserDefaults.standard.set(networkCode, forKey: "MobileNetworkCode")
        UserDefaults.standard.set(carrierName, forKey: "MobileCarrierName")
        
        NotificationCenter.default.addObserver(self, selector: #selector(reloadAllApiCall), name: NSNotification.Name("AppLanguageChange"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(shortCutMenuAction), name: Notification.Name("ShortCutMenuAction"), object: nil)
        self.getDataFromApi()
        DispatchQueue.main.async {
            self.aPIManager.getTheAppUpdationStatus("", onSuccess: { [weak self] responseData in
                if let dataModel = responseData  as? UpdationModel {
                    self?.ServerResponse = dataModel
                    self?.appForceUpdate()
                }
                }, onError: { [weak self] message in
                    if let viewLoc = self?.view {
                        AppUtility.showToast(message,view: viewLoc)
                    }})
        }
    }
    
    
    func OneTimeLoadData(){
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        if let viewController = storyBoard.instantiateViewController(withIdentifier: "Township") as? Township {
            viewController.delegate = self
            let NavCOntro = UINavigationController.init(rootViewController: viewController)
            NavCOntro.setStatusBar(backgroundColor: UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1))
            NavCOntro.navigationBar.isHidden = false
            NavCOntro.modalPresentationStyle = .overFullScreen
            //            viewController.selectedType = type
            //            if type == .state {
            //                viewController.countryObject = self.countryObject
            //            } else if type == .townShip {
            //                viewController.countryObject = self.selectedState
            //            }
            
            self.present(NavCOntro, animated: true, completion: nil)
        }
    }
    
    @objc func shortCutMenuAction() {
        DispatchQueue.main.async {
            self.decodeNavigationURL(UserDefaults.standard.value(forKey: "UIApplicationLaunchOptionsKey.shortcutItem") as? String ?? "")
        }
    }
    
    
    
    //MARK:- APP Force Update
    
    func appForceUpdate () {
        
        if let isUpdate = self.ServerResponse?.isUpdateRequired , isUpdate == true {
            
            if let isForceUpdate = self.ServerResponse?.isUpdateForceRequired , isForceUpdate == true {
                
                
            }
            else {
                
                
            }
        }
        else {
            
            
        }
    }
    
    //MARK:- Shortcut Navigation Decoder
    func decodeNavigationURL(_ strURL: String) -> Void {
        if strURL == "com.cgm.Search" {
            self.navigate(.Search)
        } else if strURL == "com.cgm.Barcode" {
            self.navigate(.Barcode)
        } else if strURL == "com.cgm.Wishlist" {
            self.navigate(.Wishlist)
        } else if strURL == "com.cgm.ScanQR" {
            self.navigate(.ScanQR)
        }else if strURL == "com.cgm.Cart"{
            self.navigate(.Cart)
        }
        UserDefaults.standard.removeObject(forKey: "UIApplicationLaunchOptionsKey.shortcutItem")
        UserDefaults.standard.synchronize()
    }
    
    func navigate(_ navEnum : HomeNavigationActions) {
        switch navEnum {
        case .Search:   self.presentController(storyboard: "Main", identifier: "SearchProducts")
        break;
        case .Barcode:  self.presentController(storyboard: "Main", identifier: "qrScanner")
        break;
        case .Wishlist: self.presentController(storyboard: "Main", identifier: "MyWishList_ID")
        break;
        case .ScanQR: self.presentController(storyboard: "Main", identifier: "qrScanner")
        break;
        case .Cart: self.cartAction(UIBarButtonItem())
        break;
        }
    }
    
    func presentController(storyboard:String,identifier:String){
        if identifier == "qrScanner" {
            let payto = UIStoryboard(name: storyboard, bundle: nil).instantiateViewController(withIdentifier: identifier) as! qrScanner
            payto.modalPresentationStyle = .fullScreen
            payto.isPresented = true
            let aObjNav = UINavigationController(rootViewController: payto)
            aObjNav.modalPresentationStyle = .fullScreen
             DispatchQueue.main.async {
                 self.getTopMostViewController()?.present(aObjNav, animated: true, completion: nil)
             }
        }
        else {
           let payto = UIStoryboard(name: storyboard, bundle: nil).instantiateViewController(withIdentifier: identifier)
           payto.modalPresentationStyle = .fullScreen
           let aObjNav = UINavigationController(rootViewController: payto)
           aObjNav.modalPresentationStyle = .fullScreen
           DispatchQueue.main.async {
               self.getTopMostViewController()?.present(aObjNav, animated: true, completion: nil)
           }
        }
       
    }
    
    func getTopMostViewController() -> UIViewController? {
        var topMostViewController = UIApplication.shared.keyWindow?.rootViewController
        
        while let presentedViewController = topMostViewController?.presentedViewController {
            topMostViewController = presentedViewController
        }
        
        return topMostViewController
    }
    
    
    @IBAction func unwindToHome(segue:UIStoryboardSegue) { }
    
    func setNavigationTitle () {
        self.navigationItem.titleView = nil
        let titleFrameView = UIView()
        titleFrameView.frame = CGRect(x: 0, y: -10, width: 140, height: 60)
        
        let titleImageView = UIImageView()
        titleImageView.frame = CGRect(x: 20, y: -10, width: 100, height: 60)
        titleImageView.contentMode = .scaleAspectFill
        titleImageView.image = UIImage(named: "logo_blue")
        
        titleFrameView.addSubview(titleImageView)
        self.navigationItem.titleView = titleFrameView
        
    }
    
    private func getDataFromApi() {
        phNumValidationsApi = []
        if AppUtility.isConnectedToNetwork() {
            var urlStr = ""
            let params = [String: String]() as AnyObject
            switch APIManagerClient.sharedInstance.serverUrl {
            case .testUrl:
                urlStr = "http://69.160.4.151:8001/RestService.svc/GetMobileTopupValidateJson"
            case .stagingUrl:
                urlStr = "http://69.160.4.151:8001/RestService.svc/GetMobileTopupValidateJson"
            case .productionUrl:
                urlStr = "https://www.okdollar.co/RestService.svc/GetMobileTopupValidateJson"
            }
            guard let validationUrl = URL(string: urlStr) else { return }
            let encoder = JSONEncoder()
            let request = PhNumValidationReq(mobileNumber: "00959953460459", msId: "000000000000000", simId: "00000000-0000-0000-0000-000000000000")
            do {
                let jsonData = try encoder.encode(request)
                aPIManager.genericClass(url: validationUrl, param: params, httpMethod: "POST", header: false, addAddress: nil, hbData: jsonData) {(response, success, data) in
                    guard let safeData = data else { return }
                    if success {
                        do {
                            let decoder = JSONDecoder()
                            let response = try decoder.decode(MobNumValidationResponse.self, from: safeData)
                            if let responseStr = response.data {
                                println_debug(responseStr)
                                if responseStr == "null" {
                                    println_debug("Null string from server")
                                } else {
                                    guard let strData = responseStr.data(using: .utf8) else { return }
                                    let finalResponse = try decoder.decode(ValidationListResponse.self, from: strData)
                                    if let code = finalResponse.code, code == 200 {
                                        if let validations = finalResponse.validations, validations.count > 0 {
                                            phNumValidationsApi = validations
                                        }
                                    }
                                }
                            } else {
                                println_debug("No specified key from server")
                            }
                        } catch _ {
                            println_debug("Parse error")
                        }
                    }
                }
            } catch {
                println_debug("Error in mobile validation api")
            }
        } else {
            println_debug("No Internet")
        }
    }
    
    private func viewDidLoadCall() {
        UIApplication.shared.applicationIconBadgeNumber = 0
        searchView.layer.cornerRadius = 2
        self.menuBarButton.isEnabled = false
        refreshControl = UIRefreshControl()
        refreshControl.tintColor = UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1)
        //        refreshControl.attributedTitle = NSAttributedString(string: "Pull down to refresh...")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        self.contentTableView.addSubview(self.refreshControl)
        //        if appDelegate.langStr != "en" {
        //            contentTableView.transform = CGAffineTransform(scaleX: -1.0,y: 1.0)
        //            //            self.pager_View.transform = CGAffineTransform(scaleX: -1.0,y: 1.0)
        //        }
        if UIDevice.current.hasNotch {
            println_debug("Notqch iphone")
            contentTableViewBottomConstraint.constant = -50
        } else {
            println_debug("don't have to consider notch")
        }
        self.timer = Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(self.scrollAutomatically), userInfo: nil, repeats: true)
        self.apiManagerClient = APIManagerClient.sharedInstance
        let dummyViewHeight = CGFloat(50)
        self.contentTableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: self.contentTableView.bounds.size.width, height: dummyViewHeight))
        self.contentTableView.contentInset = UIEdgeInsets(top: -dummyViewHeight, left: 0, bottom: 0, right: 0)
        if AppUtility.isConnectedToNetwork() {
            self.reloadAllApiCall()
        } else {
            AppUtility.showInternetErrorToast(AppConstants.InternetErrorText.message, view: self.view)
        }
    }
    
    @objc func reloadAllApiCall() {
        DispatchQueue.main.async {
            //            self.setNavigationTitle()
            //self.navigationItem.title = "1 Stop Mart".localized
            self.searchLbl.text = "Search for product".localized
            self.searchLbl.font = UIFont(name: appFont, size: 15.0)
            
            if AppUtility.isConnectedToNetwork() {
                self.initializeAndDownloadData()
            } else {
                AppUtility.showInternetErrorToast(AppConstants.InternetErrorText.message, view: self.view)
            }
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if UserDefaults.standard.bool(forKey: "isLogin") {
            if self.isLogoutTrue  {
                self.logout()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.OneTimeConfig()
        self.title = "1 Stop Mart".localized
        DispatchQueue.main.async {
            //            self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(badge: UserDefaults.standard.value(forKey: "badge") as? String, title: "", target: self, action: #selector(self.cartAction(_:)))
            self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(badge: UserDefaults.standard.string(forKey: "badge") ?? "", title: "", target: self, action: #selector(self.cartAction(_:)))
        }
        //        self.view.applyViewnGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
        self.view.applyViewnGradient(colors: [UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1), UIColor.init(red: 20.0/255.0, green: 185.0/255.0, blue: 85.0/255.0, alpha: 1.0)])
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.view.backgroundColor = .clear
        
        self.navigationController?.navigationBar.applyNavigationGradient(colors: [UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1), UIColor.init(red: 20.0/255.0, green: 185.0/255.0, blue: 85.0/255.0, alpha: 1.0)])
        //        self.navigationController?.navigationBar.applyNavigationGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
        //        self.cartBarButton = UIBarButtonItem.init(badge: UserDefaults.standard.value(forKey: "badge") as? String, title: "", target: self, action: #selector(self.cartAction(_:)))
        self.cartBarButton = UIBarButtonItem.init(badge: UserDefaults.standard.string(forKey: "badge") ?? "", title: "", target: self, action: #selector(self.cartAction(_:)))
        self.navigationItem.rightBarButtonItem  = self.cartBarButton
        //        self.searchContainerView.applyViewnGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
        self.searchContainerView.applyViewnGradient(colors: [UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1), UIColor.init(red: 20.0/255.0, green: 185.0/255.0, blue: 85.0/255.0, alpha: 1.0)])
        VMLoginModel.wrapModel()
        self.alertVC = SAlertController()
        
    }
    
    func logout(){
        DispatchQueue.main.async {
            self.alertVC?.ShowSAlert(title: "Information".localized, withDescription: "Are you sure you want to logout?".localized, onController: self)
            let YesAction = SAlertAction()
            YesAction.action(name: "Yes".localized, AlertType: .defualt, withComplition: {
                UserDefaults.standard.set(false, forKey: "LoginStatus")
                UserDefaults.standard.set(false, forKey: "isLogin")
                UserDefaults.standard.set(false, forKey: "REGITRATION_OPENED")
                UserDefaults.standard.set(false, forKey: "EnabledLogOut")
                UserDefaults.standard.synchronize()
            })
            let cancelAction = SAlertAction()
            cancelAction.action(name: "Cancel".localized, AlertType: .defualt, withComplition: {
                UserDefaults.standard.set(false, forKey: "isLogin")
            })
            self.alertVC?.addAction(action: [YesAction,cancelAction])
            self.isLogoutTrue = false
        }
    }
    
    
    @IBAction func scanBtnAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: "navigateToBarCodeScan", sender: self)
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.shadowImage = nil
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
        //        self.navigationController?.navigationBar.applyNavigationGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.backItem?.title = ""
        if !self.isLogoutTrue  {
            self.alertVC?.hideAlert()
        }
    }
    
    @objc func refresh(_ refreshControl: UIRefreshControl) {
        self.initializeAndDownloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.refreshControl.endRefreshing()
        }
    }
    
    @IBAction func searchTapAction(_ sender: Any) {
        self.performSegue(withIdentifier: "HomeToSearchProductsSegue", sender: self)
    }
    
    @objc func scrollAutomatically() {
        if let coll  = bannerCollectionView {
            for cell in coll.visibleCells {
                let indexPath: IndexPath? = coll.indexPath(for: cell)
                if ((indexPath?.row)!  <= pageImages.count - 1) {
                    //                    println_debug("index path row=" + "\(indexPath?.row ?? 0)" + "page count=" + "\(pageImages.count)")
                    let indexPath1: IndexPath?
                    indexPath1 = IndexPath.init(row: (indexPath?.row)! + 1, section: (indexPath?.section)!)
                    
                    if indexPath1?.row == pageImages.count {
                        let indexPath1: IndexPath?
                        indexPath1 = IndexPath.init(row: 0, section: (indexPath?.section)!)
                        //                        println_debug("index path row=" + "\(indexPath1?.row ?? 0)" + "page count=" + "\(pageImages.count)")
                        
                        coll.scrollToItem(at: indexPath1!, at: .left, animated: true)
                        if let pageCntr = self.pageControl {
                            pageCntr.currentPage = indexPath1?.row ?? 0
                        }
                        break
                    }
                    
                    coll.scrollToItem(at: indexPath1!, at: .right, animated: true)
                    
                    if let pageCntr = self.pageControl {
                        pageCntr.currentPage = indexPath1?.row ?? 0
                    }
                }
            }
        }
    }
    
    @IBAction func menuAction(_ sender: UIBarButtonItem) {
        
        if let drawerController = self.navigationController?.parent as? KYDrawerController {
            drawerController.setDrawerState(.opened, animated: true)
        }
        
        
        
        //        DispatchQueue.main.asyncAfter(deadline: .now() + 3.0, execute: {
        //            if let DrawerTableViewController = self.navigationController?.parent as? KYDrawerController{
        //                DrawerTableViewController.setDrawerState(.closed, animated: true)
        //            }
        //        })
        
        
        
    }
    
    @IBAction func cartAction(_ sender: UIBarButtonItem) {
        let viewController = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "CartRoot")
        //        let viewController = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "NearByService")
        DispatchQueue.main.async {
                      self.getTopMostViewController()?.present(viewController, animated: true, completion: nil)
                  }
       // self.present(, animated: true, completion: nil)
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if scrollView is UITableView {
            if(velocity.y>0) {
                UIView.animate(withDuration: 0.1, delay: 0, options: UIView.AnimationOptions(), animations: {
                    self.navigationController?.setNavigationBarHidden(true, animated: true)
                }, completion: nil)
            } else {
                UIView.animate(withDuration: 0.1, delay: 0, options: UIView.AnimationOptions(), animations: {
                    self.navigationController?.setNavigationBarHidden(false, animated: true)
                }, completion: nil)
            }
        }
    }
    
    deinit {
        if self.view != nil {
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "com.notification.BarcodeReader"), object: nil)
            NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "AppLanguageChange"), object: nil)
        }
    }
    
    func initializeAndDownloadData() {
        AppUtility.showLoading(self.view)
        group.enter()
        loadHomePageBanner()
        group.enter()
        loadHomePageProducts()
        group.enter()
        loadHomePageCategories()
        group.enter()
        loadHomePageMenuFacture()
        group.enter()
        loadAllCategories()
        group.enter()
        loadRecentViewProduct()
        group.notify(queue: .main) {
            println_debug("\n I am in main queue")
            AppUtility.hideLoading(self.view)
        }
    }
    
    
    func loadRecentViewProduct() {
        self.aPIManager.loadHomePageRecentlyViewedProduct(onSuccess:{ [weak self] getHomeProducts in
            self?.recentViewProductArray.removeAll()
            self?.recentViewProductArray = getHomeProducts
            self?.contentTableView.reloadData()
            self?.group.leave()
            },  onError: { [weak self] message in
                self?.group.leave()
        })
    }
    
    
    func loadHomePageBanner() {
        self.aPIManager.loadHomePageBanner( onSuccess:{ [weak self] getHomePageBanner in
            self?.pageImages.removeAll()
            self?.pageImages = []
            self?.pagehomeBanner = getHomePageBanner
            if(self?.pagehomeBanner.count != 0) {
                if let pageHmBanner = self?.pagehomeBanner {
                    for banner in pageHmBanner {
                        self?.pageImages.append((banner.ImageUrl ?? "") as NSString )
                    }
                }
            }
            self?.contentTableView.reloadData()
            if let pageCntr = self?.pageControl {
                pageCntr.currentPage = self?.pageImages.count ?? 0
            }
            self?.group.leave()
            },  onError: { [weak self] message in
                self?.group.leave()
        })
    }
    
    func loadHomePageProducts() {
        self.aPIManager.loadHomePageProducts(onSuccess:{ [weak self] getHomeProducts in
            self?.apiManagerClient?.homePageProductArray.removeAll()
            self?.homePageProductArray = getHomeProducts
            if let hmPageProductArr = self?.homePageProductArray {
                self?.apiManagerClient?.homePageProductArray = hmPageProductArr
            }
            self?.contentTableView.reloadData()
            self?.group.leave()
            },  onError: { [weak self] message in
                self?.group.leave()
        })
    }
    
    func loadHomePageCategories() {
        self.aPIManager.loadHomePageCategories( onSuccess:{ [weak self] getHomePageCategoryModels in
            self?.apiManagerClient?.categoryArray.removeAllObjects()
            if (self?.homePageProductArray.count != 0) {
                self?.hasProductInFeatureProductsArray = true
            } else {
                self?.hasProductInFeatureProductsArray = false
            }
            if let hmPageProductArr = self?.homePageProductArray {
                self?.apiManagerClient?.categoryArray.add(hmPageProductArr)
            }
            for homePageCategory in getHomePageCategoryModels {
                if(homePageCategory.products.count != 0) {
                    let homePageCategoryModelArray = [homePageCategory]
                    self?.apiManagerClient?.categoryArray.add(homePageCategoryModelArray)
                }
            }
            if let hmPageMenuFactureArr = self?.homePageMenuFactureArray {
                self?.apiManagerClient?.categoryArray.add(hmPageMenuFactureArr)
            }
            self?.contentTableView.reloadData()
            self?.group.leave()
            NotificationCenter.default.post(name: NSNotification.Name("CategoryReload"), object: nil, userInfo: nil)
            
            if let del = self?.KYDCDelegate {
                del.resetAllData()
            }
            
            
            }, onError: { [weak self] message in
                self?.group.leave()
                if let viewLoc = self?.view {
                    AppUtility.showToast(message, view: viewLoc)
                }
        })
    }
    
    func loadHomePageMenuFacture() {
        self.aPIManager.loadHomePageMenuFacture( onSuccess:{ [weak self] getHomePageMenuFacture in
            self?.homePageMenuFactureArray.removeAll()
            self?.homePageMenuFactureArray = getHomePageMenuFacture
            self?.contentTableView.reloadData()
            self?.group.leave()
            }, onError: { [weak self] message in
                self?.group.leave()
        })
    }
    
    func loadAllCategories() {
        self.aPIManager.loadAllCategories( onSuccess:{ [weak self] (getAllCategories,languageStr) in
            //                                self.customNavBarView.updateShoppingCartCount()
            self?.apiManagerClient?.leftMenuCategoriesArray.removeAll()
            for category in getAllCategories {
                category.subCategories = []
                for subCategory in getAllCategories {
                    if category.idd == subCategory.parentCategoryId {
                        category.subCategories.append(subCategory)
                    }
                }
            }
            for lowestCategory in getAllCategories {
                let parentOfLowestCategoryId = lowestCategory.parentCategoryId
                for subCategory in getAllCategories {
                    let subCategoryId = subCategory.idd
                    if parentOfLowestCategoryId == subCategoryId {
                        let parentOfSubCategory = subCategory.parentCategoryId
                        for parentCategory in getAllCategories {
                            let parentCategoryId = parentCategory.idd
                            if parentOfSubCategory == parentCategoryId {
                                if parentCategory.parentCategoryId == 0 {
                                    lowestCategory.isLowestItem = true
                                }
                            }
                        }
                    }
                }
            }
            //                if appDelegate.langStr == languageStr {
            self?.apiManagerClient?.leftMenuCategoriesArray = getAllCategories
            NotificationCenter.default.post(name: NSNotification.Name("CategoryReload"), object: nil, userInfo: nil)
            //                    NotificationCenter.default.post(name: Notification.Name(rawValue: "com.notification.reloadTableView"), object: 1);
            //                }else {
            //                    appDelegate.langStr = languageStr
            //                    if(APIManagerClient.sharedInstance.isRequestedToReloadOnlyOnce){
            //                        AppUtility.reloadInterface()
            //                        APIManagerClient.sharedInstance.isRequestedToReloadOnlyOnce = false
            //                    }
            //                }
            self?.contentTableView.reloadData()
            self?.menuBarButton.isEnabled = true
            self?.group.leave()
            }, onError: { [weak self] message in
                self?.group.leave()
        })
    }
    
    //    @IBAction func cartAction(_ sender: UIBarButtonItem) {
    //        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
    //        let shoppingCartView = mainStoryBoard.instantiateViewController(withIdentifier: "ShoppingCartViewController") as? ShoppingCartViewController
    //        centerControllersObject.pushNewController(controller: shoppingCartView!)
    //    }
    
    //        @objc func presentBarcodeReaderView(_ notification: Notification) {
    //            reader.readerDelegate = self
    //            let scanner: ZBarImageScanner = reader.scanner
    //            scanner.setSymbology(ZBAR_I25, config: ZBAR_CFG_ENABLE, to: 0)
    //            present(reader, animated: true, completion: nil)
    //        }
    
    // MARK: - UIImagePickerDelegate
    
    private func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let results: NSFastEnumeration = info["ZBarReaderControllerResults"] as! NSFastEnumeration
        var symbolFound : ZBarSymbol?
        for symbol in results as! ZBarSymbolSet {
            symbolFound = symbol as? ZBarSymbol
            break
        }
        _ = NSString(string: symbolFound!.data)
        reader.dismiss(animated: true) { () -> Void in
            //            let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
            //            let productDetailsViewController = mainStoryBoard.instantiateViewController(withIdentifier: "ProductDetailsViewController") as? ProductDetailsViewController
            //            productDetailsViewController?.productId  = resultString.integerValue
            //            centerControllersObject.pushNewController(controller: productDetailsViewController!)
        }
    }
    
    //    func reloadShoppingCart() {
    //        //self.reloadDropDownMenuList()
    //        APIManagerClient.sharedInstance.shoppingCartCount = 0
    //        self.customNavBarView.totalCart.isHidden = true
    //    }
    
    //    func roundView(_ aView: UIView, byRoundingCorners corners: UIRectCorner){
    //        let maskLayer = CAShapeLayer()
    //        maskLayer.path = UIBezierPath(roundedRect: aView.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: 8, height: 8)).cgPath
    //        aView.layer.mask = maskLayer
    //    }
    //
    //    func roundRectToAllView(_ view: UIView )->Void{
    //        view.layer.cornerRadius = 8
    //        view.layer.shadowOffset = CGSize(width: 0.5, height: 1)
    //        view.layer.shadowOpacity = 0.3
    //        view.layer.shadowRadius = 1
    //    }
    
    @objc func viewAllItems (sender: UIButton) {
        if let categoryModelArray = self.apiManagerClient?.categoryArray[sender.tag-1] as? [HomePageCategoryModel] {
            self.categoryId = categoryModelArray[0].idd
            self.categoryName = categoryModelArray[0].name ?? ""
            if categoryModelArray[0].subCategory.count > 0 {
                if self.categoryId > 0 {
                    self.performSegue(withIdentifier: "HomeCategoryToSubCategoriesSegue", sender: self)
                }
            }
        }
        self.isTapOnManufactureProductFlag = false
        self.performSegue(withIdentifier: "HomeToProductCollectionsSegue", sender: self)
    }
    
    @objc func viewAllItemsFeatureproducts (sender: UIButton) {
        self.categoryName = "Featured Products"//FEATURED PRODUCTS"
        self.isTapOnManufactureProductFlag = false
        self.categoryId = nil
        self.performSegue(withIdentifier: "HomeToProductCollectionsSegue", sender: self)
    }
    
    @objc func viewAllItemsRecentproducts (sender: UIButton) {
        self.categoryName = "Recently viewed".localized//FEATURED PRODUCTS"
        self.isTapOnManufactureProductFlag = false
        self.categoryId = nil
        self.performSegue(withIdentifier: "HomeToProductCollectionsSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "HomeToProductCollectionsSegue" {
            if let vc = segue.destination as? UINavigationController {
                if let rootViewController = vc.viewControllers.first as? ProductCollections {
                    rootViewController.categoryId = self.categoryId
                    rootViewController.categoryName = self.categoryName
                    if self.categoryName == "Featured Products" {
                        rootViewController.featuredProductsFlag = true
                    } else {
                        if self.isTapOnManufactureProductFlag {
                            rootViewController.manufacturerFlag = true
                        } else {
                            rootViewController.manufacturerFlag = false
                        }
                    }
                }
            }
        } else if segue.identifier == "HomeToProductDetailsSegue" {
            if let vc = segue.destination as? ProductDetails {
                vc.productId = self.productId
                vc.productName = self.productName
            }
        } else if segue.identifier == "HomeCategoryToSubCategoriesSegue" {
            if let vc = segue.destination as? UINavigationController {
                if let rootViewController = vc.viewControllers.first as? ProductSubCategories {
                    rootViewController.categoryId = self.categoryId
                    rootViewController.categoryName = self.categoryName
                    rootViewController.Root = "dashboard"
                }
            }
        }else if segue.identifier == "navigateToBarCodeScan" {
            if let vc = segue.destination as? qrScanner {
                vc.delegate = self
            }
        }
    }
}

extension ViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if pageImages.count == 0 {
            return self.apiManagerClient?.categoryArray.count ?? 0
        } else {
            print("count Val: \(self.apiManagerClient?.categoryArray.count)")
            return (self.apiManagerClient?.categoryArray.count ?? 0) //-Production
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "BannerCell", for: indexPath) as! HomeBannerTableViewCell
            cell.cellDelegate = self
            self.bannerCollectionView = cell.bannerCollectionView
            self.pageControl = cell.pageControl
            //            let bannerData = self.bannerData
            cell.updateCellWith(bannerData: pageImages, data: self.pagehomeBanner)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProductsCategoryCell", for: indexPath) as! CategoryRowsTableViewCell
            if pageImages.count == 0 {
                if let array = self.apiManagerClient?.categoryArray[indexPath.section] as? NSArray {
                    cell.getCategoryArray(dataArray: array, sectionIndex:indexPath.section)
                }
            }
                
            else if indexPath.section == (self.apiManagerClient?.categoryArray.count)!{
                cell.getCategoryArray(dataArray: self.recentViewProductArray as NSArray, sectionIndex: indexPath.section)
                print("condition last Index \(indexPath.section)")
            }
                
                
            else {
                if let array = self.apiManagerClient?.categoryArray[indexPath.section - 1] as? NSArray {
                    cell.getCategoryArray(dataArray: array, sectionIndex:indexPath.section - 1)
                }
            }
            cell.cellDelegate = self
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        
        if section == 0 {
            return 0
        } else {
            var array: NSArray?
            var sectionIndex:Int
            if pageImages.count == 0 {
                array = self.apiManagerClient?.categoryArray[section] as? NSArray
                sectionIndex = section
            }
                
            else if section == (self.apiManagerClient?.categoryArray.count)!{
                //                return 45
                if self.recentViewProductArray.count > 0{
                    return 45.0
                }else{
                    return 0
                }
            }
                
                
            else {
                array = self.apiManagerClient?.categoryArray[section - 1] as? NSArray
                sectionIndex = section - 1
            }
            if sectionIndex == 0 {
                if (array?.count ?? 0) > 0 {
                    return 0
                }
            } else {
                if let modelArray = array as? [HomePageCategoryModel] {
                    self.homePageCategoriesModels = modelArray
                    if self.homePageCategoriesModels.count > 0 {
                        if homePageCategoriesModels[0].products.count > 0 {
                            return 45
                        }
                    }
                }
            }
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = Bundle.main.loadNibNamed("HomeCategoriesHeaderView", owner: nil, options: nil)?[0] as! HomeCategoriesHeaderView
        if section == 1 {
            headerView.viewAllButton.isHidden = false
            headerView.headerNameLabel.text = "Featured Products"
            headerView.viewAllButton.tag = section
            headerView.viewAllButton.addTarget(self, action: #selector(ViewController.viewAllItemsFeatureproducts(sender:)), for: .touchUpInside)
        }
        else if section == (self.apiManagerClient?.categoryArray.count)! {
            
            headerView.viewAllButton.tag = section
            headerView.viewAllButton.isHidden = false
            headerView.headerNameLabel.text = "Recently Viewed".localized
            headerView.viewAllButton.addTarget(self, action: #selector(ViewController.viewAllItemsRecentproducts(sender:)), for: .touchUpInside)
            
        }
        else {
            headerView.viewAllButton.isHidden = false
            if let categoryModelArray = self.apiManagerClient?.categoryArray[section-1] as? [HomePageCategoryModel] {
                headerView.headerNameLabel.text = categoryModelArray[0].name?.capitalized
            }
            headerView.viewAllButton.tag = section
            headerView.viewAllButton.addTarget(self, action: #selector(ViewController.viewAllItems(sender:)), for: .touchUpInside)
        }
        return headerView
    }
}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        
        if indexPath.section == 0 {
            if pageImages.count == 0 {
                return 0
            } else {
                return SCREEN_WIDTH/2
            }
        } else {
            var array = NSArray()
            var sectionIndex:Int
            if pageImages.count == 0 {
                if let locArray = self.apiManagerClient?.categoryArray[indexPath.section] as? NSArray {
                    array = locArray
                }
                sectionIndex = indexPath.section
            } else {
                if let locArray = self.apiManagerClient?.categoryArray[indexPath.section - 1] as? NSArray {
                    array = locArray
                }
                sectionIndex = indexPath.section - 1
            }
            var doubleIteration = 0
            for i in 0..<array.count {
                if i % 2 == 0 {
                    doubleIteration += 1
                }
            }
            
            if sectionIndex == 0 {
                ////////////////////////////////////// Featured Products ////////////////////////////////////////////////////////
                var extraHeight: CGFloat = 40
                var doubleIteration = 0
                if array.count > 4 {
                    doubleIteration += 2
                } else {
                    for i in 0..<array.count {
                        if i % 2 == 0 {
                            doubleIteration += 1
                        }
                    }
                }
                extraHeight = extraHeight * CGFloat(doubleIteration)
                let itemHeight = HomeConstant.getItemWidth(boundWidth: tableView.bounds.size.width)
                var totalRow:CGFloat = 0
                if array.count > 4 {
                    totalRow = ceil(4 / HomeConstant.column)
                } else {
                    totalRow = ceil(CGFloat(array.count) / HomeConstant.column)
                }
                let totalTopBottomOffset = HomeConstant.offset + HomeConstant.offset
                let totalSpacing = CGFloat(totalRow - 1) * HomeConstant.minLineSpacing
                let totalHeight  = ((itemHeight * CGFloat(totalRow)) + totalTopBottomOffset + totalSpacing)
                //                return totalHeight + extraHeight
                return 0
            }
                
                //            else if sectionIndex == (self.apiManagerClient?.categoryArray.count)!{
                //
                //            }
                
            else if sectionIndex == (self.apiManagerClient?.categoryArray.count ?? 0)  {
                
                
                ////////////////////////////////////// Manufactured Products //////////////////////////////////////////////////////
                let itemHeight = HomeConstant.getItemWidth(boundWidth: tableView.bounds.size.width)
                let totalRow = ceil(CGFloat(array.count) / HomeConstant.column)
                let totalTopBottomOffset = HomeConstant.offset + HomeConstant.offset
                let totalSpacing = CGFloat(totalRow - 1) * HomeConstant.minLineSpacing
                let totalHeight  = ((itemHeight * CGFloat(totalRow)) + totalTopBottomOffset + totalSpacing)
                return totalHeight
                
                
            }
                
                //            if indexPath.section == (self.apiManagerClient?.categoryArray.count ?? 0) + 1{
                //                print("Condition coming")
                //                return 247.0
                //
                //            }
                
            else if indexPath.section == (self.apiManagerClient?.categoryArray.count)!{
                //                return 45
                if self.recentViewProductArray.count > 0{
                    return 247.0
                }else{
                    return 0
                }
            }
                
                
                
            else {
                ////////////////////////////////////// All Dynamic Products ////////////////////////////////////////////////////////
                if let modelArray = array as? [HomePageCategoryModel] {
                    self.homePageCategoriesModels = modelArray
                }
                var extraHeight2: CGFloat = 40
                var doubleIteration2 = 0
                if homePageCategoriesModels.count > 0 {
                    if homePageCategoriesModels[0].products.count > 4 {
                        doubleIteration2 += 2
                    } else {
                        for i in 0..<homePageCategoriesModels[0].products.count {
                            if i % 2 == 0 {
                                doubleIteration2 += 1
                            }
                        }
                    }
                }
                extraHeight2 = extraHeight2 * CGFloat(doubleIteration2)
                let itemHeight = HomeConstant.getItemWidth(boundWidth: tableView.bounds.size.width)
                var totalRow:CGFloat = 0
                if homePageCategoriesModels.count > 0 {
                    if homePageCategoriesModels[0].products.count > 4 {
                        totalRow = ceil(4 / HomeConstant.column)
                    } else {
                        totalRow = ceil(CGFloat(self.homePageCategoriesModels[0].products.count) / HomeConstant.column)
                    }
                }
                let totalTopBottomOffset = HomeConstant.offset + HomeConstant.offset
                let totalSpacing = CGFloat(totalRow - 1) * HomeConstant.minLineSpacing
                let totalHeight  = ((itemHeight * CGFloat(totalRow)) + totalTopBottomOffset + totalSpacing)
                return totalHeight + extraHeight2
            }
        }
    }
}

extension ViewController: ProductsCollectionCellDelegate {
    func tapOnRecentViwedProductstCollectionView(collectioncell: ProductsCollectionViewCell?) {
        if let cell = collectioncell {
            //            self.productId = cell.productId
            //            self.performSegue(withIdentifier: "HomeToProductDetailsSegue", sender: self)
            guard let vc = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: ProductDetails.self)) as? ProductDetails else { return }
            vc.productId = cell.productId
            vc.productName = cell.productName
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func tapOnManufacturedProductstCollectionView(collectioncell: ProductsCollectionViewCell?) {
        if let cell = collectioncell {
            self.categoryId = cell.productId
            self.categoryName = cell.categoryName
            self.isTapOnManufactureProductFlag = true
            self.performSegue(withIdentifier: "HomeToProductCollectionsSegue", sender: self)
        }
    }
    
    func tapOnProductCollectionView(collectioncell: ProductsCollectionViewCell?) {
        if let cell = collectioncell {
            //            self.productId = cell.productId
            //            self.performSegue(withIdentifier: "HomeToProductDetailsSegue", sender: self)
            guard let vc = self.storyboard?.instantiateViewController(withIdentifier: String.init(describing: ProductDetails.self)) as? ProductDetails else { return }
            vc.productId = cell.productId
            vc.productName = cell.productName
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
}

extension ViewController: BannerCollectionCellDelegate {
    func collectionView(collectioncell: HomeBannerCollectionViewCell?, didTappedInTableviewTableCell: HomeBannerTableViewCell) {
        if collectioncell != nil {
            self.categoryId = Int(didTappedInTableviewTableCell.categoryId!)
            self.categoryName = didTappedInTableviewTableCell.categoryName ?? "1 Stop Mart".localized
            if self.categoryId > 0 {
                self.performSegue(withIdentifier: "HomeCategoryToSubCategoriesSegue", sender: self)
            }
        }
    }
    func pushCustomNavigation(categoryId: String, categoryName: String) {
        
    }
}

extension ViewController : qrScannerDelegate {
    func navigateToProductDetails(product_ID: Int) {
        self.productId = product_ID
        self.performSegue(withIdentifier: "HomeToProductDetailsSegue", sender: self)
    }
    
}






extension ViewController : TownshipDelegate{
    func selectedTownshipMethod(type: tapGestureType, selectedText: String, MinOrderValue: String) {
        DispatchQueue.main.async {
            var message = ""
            if selectedText == "Others" {
                message = "We are coming soon for Other township.".localized
            }
            else {
                message = "Congratulation Your township fall into Free Deliver of Minimum buying of \(MinOrderValue) MMK".localized
            }
            let alertVC = SAlertController()
            alertVC.ShowSAlert(title: "".localized, withDescription: message, onController: self)
            let YesAction = SAlertAction()
            YesAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                
            })
            
            alertVC.addAction(action: [YesAction])
        }
    }
    
    func OneTimeConfig(){
        
        if !(isKeyPresentInUserDefaults(key: "OneTime")){
            self.OneTimeLoadData()
        }
    }
    
    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    
}


struct PhNumValidationReq: Codable {
    var appID: String?
    var limit = 0
    var mobileNumber: String?
    var msId: String?
    var offSet = 0
    var osType = 0
    var otp: String?
    var simId: String?
    
    init(mobileNumber: String, msId: String, simId: String) {
        self.mobileNumber = mobileNumber
        self.msId = msId
        self.simId = simId
    }
    
    private enum CodingKeys: String, CodingKey {
        case appID = "AppId"
        case limit = "Limit"
        case mobileNumber = "MobileNumber"
        case msId = "Msid"
        case offSet = "Offset"
        case osType = "Ostype"
        case otp = "Otp"
        case simId = "Simid"
    }
}
