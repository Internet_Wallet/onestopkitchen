//
//  VMMyAccountViewController.swift
//  VMart
//
//  Created by Kethan on 11/9/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit
import NaturalLanguage
import AVFoundation
import SwiftyJSON
import IQKeyboardManagerSwift

enum OtherMobilMumber {
    case Myanmar
    case Other
}
enum CountrySelected {
    case Phone
    case OtherPhone
}

class VMMyAccountViewController: VMLoginBaseViewController {
    var aPIManager = APIManager()
    
    var session: AVCaptureSession?
    var stillImageOutput: AVCaptureStillImageOutput?
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    let validObj  = PayToValidations()
    
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var previewView: UIImageView!
    @IBOutlet weak var cameraContainer: UIView!
    @IBOutlet weak var tbUpdate: UITableView!
    @IBOutlet weak var updateBtn: UIButton! {
        didSet {
            self.updateBtn.setTitle((self.updateBtn.titleLabel?.text ?? "").localized, for: .normal)
            self.updateBtn.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1), UIColor.init(red: 20.0/255.0, green: 185.0/255.0, blue: 85.0/255.0, alpha: 1.0)])
            self.updateBtn.setTitleColor(UIColor.white, for: .normal)
            self.updateBtn.isEnabled = false
            updateBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    
    
    var gender: genderType = .male
    var screenFrom: String?
    var imagePicker = UIImagePickerController()
    var userImage: UIImage?
    var profileImage : prifilePic = .notTaken
    var base64ImageData = ""
    private let EMAILCHARSET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890.@_-"
    var countryName: CountryName = .Myanmar
    private let mobileNumberAcceptedChars = "0123456789"
    
    var ProfilePictureUrl = ""
    var FirstName = ""
    var LastName = ""
    var Phone = ""
    var Email = ""
    var IsDisplayEmail = false
    var Gender = "M"
    var DateOfBirthDay = ""
    var DateOfBirthMonth = ""
    var DateOfBirthYear = ""
    var Password = ""
    var OtherMobileNumber = ""
    
    
    var c_codePhone = ""
    var c_codeOtherPhone = ""
    var otherNumber : OtherMobilMumber = .Other
    var countrySeleted : CountrySelected = .Phone
    let alertVC1 = SAlertController()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.extendedLayoutIncludesOpaqueBars = true
        IQKeyboardManager.shared.enable = true
        self.tbUpdate.tableFooterView = UIView()
        phNumValidationsFile = getDataFromJSONFile() ?? []
        self.navigationController?.navigationBar.applyNavigationGradient(colors: [UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1), UIColor.init(red: 20.0/255.0, green: 185.0/255.0, blue: 85.0/255.0, alpha: 1.0)])
        self.navigationItem.title = "My Account".localized
        
        let searchButton   = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(searchBarBtnAction))
        searchButton.tintColor = UIColor.white
        let cartButton = UIBarButtonItem.init(badge: UserDefaults.standard.string(forKey: "badge") ?? "", title: "", target: self, action: #selector(cartBtnAction))
        // navigationItem.rightBarButtonItems = [cartButton, searchButton]
        //  if let screenFrm = screenFrom, screenFrm == "DashBoard" {
        self.navigationController?.navigationBar.tintColor = UIColor.blue
        self.navigationController?.isNavigationBarHidden = false
        let button = UIButton()
        button.setImage(UIImage(named: "back"), for: .normal)
        button.imageEdgeInsets.left = -35
        button.addTarget(self, action: #selector(dismissScreen), for: .touchUpInside)
        let item = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItems = [item]
        
        self.getDetails()
        
        
        if Gender == "M" {
            gender = .male
        }else {
            gender = .female
        }
    }
    
    func cameraConfigure() {
        session = AVCaptureSession()
        session!.sessionPreset = AVCaptureSession.Preset.photo
        let frontCamera =  AVCaptureDevice.default(.builtInWideAngleCamera, for: AVMediaType.video, position: .front)
        var error: NSError?
        var input: AVCaptureDeviceInput!
        do {
            input = try AVCaptureDeviceInput(device: frontCamera!)
        } catch let error1 as NSError {
            error = error1
            input = nil
            print(error!.localizedDescription)
        }
        if error == nil && session!.canAddInput(input) {
            session!.addInput(input)
            stillImageOutput = AVCaptureStillImageOutput()
            stillImageOutput?.outputSettings = [AVVideoCodecKey:  AVVideoCodecJPEG]
            if session!.canAddOutput(stillImageOutput!) {
                session!.addOutput(stillImageOutput!)
                videoPreviewLayer = AVCaptureVideoPreviewLayer(session: session!)
                videoPreviewLayer!.videoGravity =    AVLayerVideoGravity.resizeAspectFill
                videoPreviewLayer!.connection?.videoOrientation =   AVCaptureVideoOrientation.portrait
                videoPreviewLayer!.frame = previewView.frame
                previewView.layer.addSublayer(videoPreviewLayer!)
                session!.startRunning()
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.session!.stopRunning()
    }
    
    
    @objc func searchBarBtnAction(sender: Any) {
        let viewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchProducts") as? SearchProducts
        self.navigationController?.pushViewController(viewController!, animated: true)
    }
    
    @objc func cartBtnAction(sender: Any) {
        let viewController = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "CartRoot")
        self.present(viewController, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.cameraConfigure()
    }
    
    private func getDetails() {
        if !self.isNetworkAvailable() {
            AppUtility.showToastlocal(message: "Internet connection is not available".localized, view: self.view)
            return
        }
        AppUtility.showLoading(self.view)
        self.aPIManager.GenericClassGet(url: "customer/info", successBlock: { (isSuccess,response) in
            AppUtility.hideLoading(self.view)
            if isSuccess {
                println_debug(response)
                self.ProfilePictureUrl = response["ProfilePictureUrl"].stringValue
                self.FirstName = response["FirstName"].stringValue
                self.LastName = response["LastName"].stringValue
                self.Phone = response["Phone"].stringValue
                self.Email = response["Email"].stringValue
                self.Gender = response["Gender"].stringValue
                if self.Gender == "M" {
                    self.gender = .male
                } else {
                    self.gender = .female
                }
                self.DateOfBirthDay = response["DateOfBirthDay"].stringValue
                self.DateOfBirthMonth = response["DateOfBirthMonth"].stringValue
                self.DateOfBirthYear = response["DateOfBirthYear"].stringValue
                self.Password = response["Password"].stringValue
                self.OtherMobileNumber = response["OtherMobileNumber"].stringValue
                
                let (code,number) = self.MobileNumberWithCountryCode(num: self.Phone)
                self.Phone = number
                self.c_codePhone = "+" + code
                
                if self.OtherMobileNumber == "" {
                    self.c_codeOtherPhone = "+95"
                }else{
                    let (code,number) = self.MobileNumberWithCountryCode(num: self.OtherMobileNumber)
                    self.OtherMobileNumber = number
                    self.c_codeOtherPhone = "+" + code
                }
                if self.ProfilePictureUrl != "" {
                    if let url = URL(string: self.ProfilePictureUrl) {
                        DispatchQueue.global().async {
                            guard let data = try? Data(contentsOf: url) else { return }
                            DispatchQueue.main.async {
                                self.userImage = UIImage(data: data)
                                self.profileImage = .taken
                                self.tbUpdate.reloadRows(at: [IndexPath.init(row: 0, section: 0)], with: .automatic)
                            }
                        }
                    }
                }
                self.tbUpdate.reloadData()
            }else{
                self.showAlert(alertTitle: "Error!".localized, description: "Request Failed. Please try again".localized)
            }
        })
        
    }
    
    @objc func dismissScreen() {
        if self.navigationItem.title == "Take your Selfie".localized {
            guard self.alertVC1 != nil else {
                return
            }
            self.alertVC1.removeFromParent()
            self.alertVC1.view.removeFromSuperview()
            self.navigationItem.title = "My Account".localized
            self.cameraContainer.isHidden = true
        }else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func updateButtonAction(_sender: UIButton) {
        self.view.endEditing(true)
        self.updateUserProfile()
    }
    
    @objc func submitFromToolbar() {
        self.view.endEditing(true)
        self.updateUserProfile()
    }
}


extension VMMyAccountViewController {
    //MARK:- Update api
    private func updateUserProfile() {
        
        self.checkValidFields(handler: { (isBool, message) in
            if isBool {
                if base64ImageData != "" {
                    self.uploadProfilePic(handler: {(isBool) in
                        if isBool {
                            self.updateDetails()
                        }else {
                            AppUtility.showToastlocal(message: "Profile Image uploading failed. Please try again".localized, view: self.view)
                        }
                    })
                }else {
                    // Send Old URL To server
                    self.updateDetails()
                }
            }else {
                //self.showAlert(alertTitle: "Error!".localized, description: "Missing required fields")
            }
            
        })
        
    }
    
    private func updateDetails() {
        if !self.isNetworkAvailable() {
            AppUtility.showToastlocal(message: "Internet connection is not available".localized, view: self.view)
            return
        }
        let param = self.getUpdateProfileParams()
        AppUtility.showLoading(self.view)
        aPIManager.GenericClassPost(url: "customer/info", params: param, successBlock: { (isSuccess,response) in
            AppUtility.hideLoading(self.view)
            if isSuccess {
                println_debug(response)
                if response["StatusCode"] == 200 {
                    self.tbUpdate.reloadData()
                    self.updateBtn.isEnabled = false
                    // AppUtility.showToastlocal(message: "Your details updated successfully".localized, view: self.view)
                    DispatchQueue.main.async {
                        self.dismiss(animated: true, completion: {
                        })
                    }
                }else {
                    AppUtility.showToastlocal(message: "Update your details failed. Please try again".localized, view: self.view)
                }
            }
        })
        
    }
    
    private func checkValidFields(handler: (_ valid: Bool, _ message: String)-> Void) {
        
        
        if c_codePhone == "+95" {
            let number = checkValidNumber(number:  Phone)
            if  !(Phone.count <= number.max && Phone.count >= number.min) {
                AppUtility.showToastlocal(message: "Please enter valid mobile number".localized, view: self.view)
                    return
               // handler(false, "")
            }
        }else {
            if !(Phone.count > 7 && Phone.count < 13) {
                AppUtility.showToastlocal(message: "Please enter valid mobile number".localized, view: self.view)
               // handler(false, "")
                    return
            }
        }
        
        if Email == "" {
            AppUtility.showToastlocal(message: "Please enter Email Id".localized, view: self.view)
           // handler(false, "")
                return
        }
        
        if !AppUtility.isValidEmail(Email) {
            AppUtility.showToastlocal(message: "Please enter valid Email Id".localized, view: self.view)
            return
            //handler(false, "")
        }
        
        if c_codeOtherPhone == "+95" {
            if OtherMobileNumber.count > 2 {
                let number1 = checkValidNumber(number:  OtherMobileNumber)
                if  !(OtherMobileNumber.count <= number1.max && OtherMobileNumber.count >= number1.min) &&  OtherMobileNumber != "" {
                    AppUtility.showToastlocal(message: "Please enter valid other mobile number".localized, view: self.view)
                  //  handler(false, "")
                        return
                }
            }
        }else {
            if !(OtherMobileNumber.count > 7 && OtherMobileNumber.count < 13) {
                AppUtility.showToastlocal(message: "Please enter valid mobile number".localized, view: self.view)
               // handler(false, "")
                    return
            }
        }
        handler(true, "")
        //   self.updateDetails()
    }
    
    
    private func showAlert(message: String) {
        if message == "Your details updated successfully".localized {
            DispatchQueue.main.async {
                let alertVC = SAlertController()
                alertVC.ShowSAlert(title: message, withDescription: "Updated successfully".localized, onController: self)
                let ok = SAlertAction()
                ok.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                    self.dismiss(animated: true, completion: nil)
                })
                alertVC.addAction(action: [ok])
            }
        } else {
            self.showAlert(alertTitle: "Warning!".localized, description: message)
        }
    }
    
    func getUpdateProfileParams() -> [String: AnyObject] {
        
        var paramDic   = [String:Any]()
        paramDic["ProfilePictureUrl"]  = ProfilePictureUrl
        paramDic["FirstName"]  = FirstName
        paramDic["LastName"]  = LastName
        paramDic["Phone"]  = convertMobileFormate(countryCode: c_codePhone, phoneNumber: Phone)
        paramDic["Email"]  = Email
        
        if gender == .male {
            paramDic["Gender"] =  "M"
        }else if gender == .female {
            paramDic["Gender"] = "F"
        }else {
            paramDic["Gender"] = ""
        }
        
        paramDic["DateOfBirthDay"]  = DateOfBirthDay
        paramDic["DateOfBirthMonth"]  = DateOfBirthMonth
        paramDic["DateOfBirthYear"]  = DateOfBirthYear
        paramDic["Password"]  = Password
        if OtherMobileNumber == "" {
            paramDic["OtherMobileNumber"] = ""
        }else {
            paramDic["OtherMobileNumber"]  = convertMobileFormate(countryCode: c_codeOtherPhone, phoneNumber: OtherMobileNumber)
        }
        if Email == "" {
            paramDic["IsDisplayEmail"]  = false
        }else {
            paramDic["IsDisplayEmail"]  = true
        }
        return paramDic as [String : AnyObject]
    }
    
    private func uploadProfilePic(handler: @escaping (_ success: Bool) -> Void) {
        let paramString: [String : Any] = [
            "MobileNumber" : VMLoginModel.shared.mobileNumber ?? "",
            "Base64String" : [base64ImageData,"","","",""],
        ]
        if !self.isNetworkAvailable() {
            AppUtility.showToastlocal(message: "Internet connection is not available".localized, view: self.view)
            return
        }
        AppUtility.showLoading(self.view)
        aPIManager.UploadImage(params: paramString , url: "https://www.okdollar.co/RestService.svc/GetMultiImageUrlByBase64String", successBlock: {(success, response) in
            DispatchQueue.main.async { AppUtility.hideLoading(self.view)}
            if success {
                
                if let firstUrl = response.first as? String {
                    self.ProfilePictureUrl = firstUrl.replacingOccurrences(of: " ", with: "%20")
                }
                
                handler(true)
            }else {
                handler(false)
            }
        })
    }
    
}

extension VMMyAccountViewController : CountryViewControllerDelegate {
    func countryViewController(_ list: CountryViewController, country: Country) {
        
        if countrySeleted == .Phone {
            let index = IndexPath(row: 1, section: 0)
            let cell = tbUpdate.cellForRow(at: index) as! UpdateMobileNumberCell
            cell.btnCountry.setImage(UIImage(named: country.code), for: .normal)
            cell.lblCode.text = "(" + country.dialCode + ")"
            c_codePhone = country.dialCode
            if country.dialCode == "+95" {
                countryName = .Myanmar
                cell.tfName.text = "09"
            }else {
                countryName = .Other
                cell.tfName.text = ""
            }
        }else {
            let index = IndexPath(row: 6, section: 0)
            let cell = tbUpdate.cellForRow(at: index) as! UpdateMobileNumberCell
            cell.btnCountry.setImage(UIImage(named: country.code), for: .normal)
            cell.lblCode.text = "(" + country.dialCode + ")"
            c_codeOtherPhone = country.dialCode
            if country.dialCode == "+95" {
                otherNumber = .Myanmar
                cell.tfName.text = "09"
            }else {
                otherNumber = .Other
                cell.tfName.text = ""
                
            }
        }
        list.dismiss(animated: true, completion: nil)
    }
    
    func countryViewControllerCloseAction(_ list: CountryViewController) {
        list.dismiss(animated: true, completion: nil)
    }
}

extension VMMyAccountViewController {
    @objc func tappedMe(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
                self.openCamera()
            } else {
                AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                    if granted {
                        self.openCamera()
                    } else {
                        self.showAlert()
                    }
                })
            }
        }
    }
    
    func openCamera() {
        self.cameraContainer.isHidden = false
        self.view.bringSubviewToFront(self.cameraContainer)
        
        guard previewView.bounds != nil else {
            return
        }
        videoPreviewLayer!.frame = previewView.frame
        self.navigationItem.title = "Take your Selfie".localized
        DispatchQueue.main.async {
            self.alertVC1.ShowSAlert(title: "", withDescription: "Please take a clear selfie. We will use this photo for 1 Stop Mart account security.".localized, onController: self)
            let ok = SAlertAction()
            ok.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                
            })
            self.alertVC1.addAction(action: [ok])
        }
    }
    
    private func showAlert() {
        DispatchQueue.main.async {
            let settingsAppURL = URL(string: UIApplication.openSettingsURLString)!
            let alertVC = SAlertController()
            alertVC.ShowSAlert(title: "Camera".localized, withDescription: "Camera access required to take image".localized, onController: self)
            let cancel = SAlertAction()
            cancel.action(name: "Cancel".localized, AlertType: .defualt, withComplition: {})
            let allow = SAlertAction()
            allow.action(name: "Allow".localized, AlertType: .defualt, withComplition: {
                UIApplication.shared.open(settingsAppURL, options: [:], completionHandler: nil)
            })
            alertVC.addAction(action: [cancel,allow])
        }
    }
    
    @objc func onSelectGender(_ sender: UIButton) {
        let index = IndexPath(row: 4, section: 0)
        let cell = tbUpdate.cellForRow(at: index) as! UpdateGenderCell
        self.updateBtn.isEnabled = true
        if sender.tag == 1 {
            cell.imgMale.image = UIImage(named: "act_radio")
            cell.imgFemale.image = UIImage(named: "radio")
            gender = .male
        }else {
            cell.imgMale.image = UIImage(named: "radio")
            cell.imgFemale.image = UIImage(named: "act_radio")
            gender = .female
        }
    }
    
    @objc func showCountryViewController(_ sender: UIButton) {
        
        if sender.tag == 1 {
            countrySeleted = .Phone
        }else {
            countrySeleted = .OtherPhone
        }
        println_debug("showCountryViewController")
        let countryVC = self.storyboard?.instantiateViewController(withIdentifier: "CountryViewController") as! CountryViewController
        countryVC.delegate = self
        present(countryVC, animated: true, completion: nil)
    }
    
    @objc func phoneNumerEdit(_ sender: UIButton) {
        let index = IndexPath(row: 1, section: 0)
        let cell = tbUpdate.cellForRow(at: index) as! UpdateMobileNumberCell
        cell.btnEdit.isHidden = true
        if c_codePhone == "+95" {
            cell.tfName.text = "09"
        }else {
            cell.tfName.text = ""
        }
        
        cell.tfName.isUserInteractionEnabled = true
        cell.btnCountry.isUserInteractionEnabled = true
        cell.tfName.becomeFirstResponder()
    }
    
    @objc func clearButtonAction(_ sender: UIButton) {
        
        if let cell = tbUpdate.cellForRow(at: IndexPath(row: 6, section: 0)) as? UpdateMobileNumberCell {
            self.updateBtn.isEnabled = true
            if c_codeOtherPhone == "+95" {
                cell.tfName.text = "09"
            }else {
                cell.tfName.text = ""
            }
            cell.tfName.isUserInteractionEnabled = true
            cell.btnCountry.isUserInteractionEnabled = true
            cell.btnClose.isHidden = true
            OtherMobileNumber = ""
        }
        //cell.tfName.becomeFirstResponder()
    }
    
    @objc func otherPhoneNumerEdit(_ sender: UIButton) {
        self.updateBtn.isEnabled = true

        let index = IndexPath(row: 6, section: 0)
        let cell = tbUpdate.cellForRow(at: index) as! UpdateMobileNumberCell
        cell.btnEdit.isHidden = true
        
    
        if c_codeOtherPhone == "+95" {
            if cell.tfName.text?.count ?? 0 > 2 {
             cell.btnClose.isHidden = false
            }else {
              cell.btnClose.isHidden = true
            }
        }else {
            cell.tfName.text = ""
        }
    
        cell.tfName.isUserInteractionEnabled = true
        cell.btnCountry.isUserInteractionEnabled = true
        cell.tfName.becomeFirstResponder()
    }
    
    @objc func emailEdit(_ sender: UIButton) {
        self.updateBtn.isEnabled = true
        let index = IndexPath(row: 2, section: 0)
        let cell = tbUpdate.cellForRow(at: index) as! UpdateNameCell
        cell.tfName.isUserInteractionEnabled = true
        cell.btnEdit.isHidden = true
        cell.tfName.becomeFirstResponder()
    }
    
    @objc func passwordEdit(_ sender: UIButton) {
        let index = IndexPath(row: 5, section: 0)
        let cell = tbUpdate.cellForRow(at: index) as! UpdateNameCell
        cell.btnEdit.isSelected = !cell.btnEdit.isSelected
        if cell.btnEdit.isSelected {
            cell.btnEdit.setImage(UIImage(named: "openEye"), for: .normal)
            cell.tfName.isSecureTextEntry = false
        }else {
            cell.btnEdit.setImage(UIImage(named: "eyes"), for: .normal)
            cell.tfName.isSecureTextEntry = true
        }
    }
}

extension VMMyAccountViewController: UINavigationControllerDelegate,UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        imagePicker.dismiss(animated: true, completion: nil)
        guard let selectedImage = info[.originalImage] as? UIImage else {
            print("Image not found!")
            return
        }
        
        let imageOptions =  NSDictionary(object: NSNumber(value: 5) as NSNumber, forKey: CIDetectorImageOrientation as NSString)
        let personciImage = CIImage.init(cgImage: selectedImage.cgImage!)
        let accuracy = [CIDetectorAccuracy: CIDetectorAccuracyHigh]
        let faceDetector = CIDetector(ofType: CIDetectorTypeFace, context: nil, options: accuracy)
        let faces = faceDetector?.features(in: personciImage, options: imageOptions as? [String : AnyObject])
        
        let resizedeImg = AppUtility.resize(selectedImage)
        
        if let faces = faces {
            if faces.count > 1 {
                self.showAlert(alertTitle: "Error!".localized, description: "More than one faces".localized)
                self.profileImage = .notTaken
            }else {
                if let face = faces.first as? CIFaceFeature {
                    println_debug("found bounds are \(face.bounds)")
                    base64ImageData = self.imageTobase64(image: resizedeImg)
                    self.userImage = nil
                    self.userImage = selectedImage
                    self.profileImage = .taken
                    self.tbUpdate.reloadData()
                }else {
                    self.showAlert(alertTitle: "Error!".localized, description: "Face did not detected".localized)
                    self.profileImage = .notTaken
                }
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
}


extension VMMyAccountViewController : UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag == 1  {
            Phone = textField.text!
        }else if textField.tag == 2 {
            Email = textField.text!
        }else if textField.tag == 6 {
            OtherMobileNumber = textField.text!
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if range.location == 0, string == " " {
            return false
        }
        guard let tfText   = textField.text else { return false }
        let text = (tfText as NSString).replacingCharacters(in: range, with: string)
        
        
        if let char = string.cString(using: String.Encoding.utf8) {
            let isBackSpace = strcmp(char, "\\b")
            if (isBackSpace == -92) {
                if textField.tag == 6 {
                    if textField.text == "09" {
                        return false
                    }
                    else if (textField.text?.count == 3) {
                        if let cell = tbUpdate.cellForRow(at: IndexPath(row: 6, section: 0)) as? UpdateMobileNumberCell {
                                if textField.tag == 6 { cell.btnClose.isHidden = true }
                         }
                    }
                }
                return true
            }
        }
        
        if textField.tag == 2 {
            let mobileNumberAcceptableCharacterSet = NSCharacterSet(charactersIn: EMAILCHARSET).inverted
            let filteredSet = string.components(separatedBy: mobileNumberAcceptableCharacterSet).joined(separator: "")
            if string != filteredSet { return false }
            
            let isNeedToUpdate = Validations.trimSuccessiveSpaces(textField: textField, range: range, string: string)
            if isNeedToUpdate {
                return false
            }
        }else if textField.tag == 1 || textField.tag == 6 {
            
            let text = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            let rangeCheck = PayToValidations().getNumberRangeValidation(text)
            if !text.hasPrefix("09") {
                return false
            }
            if rangeCheck.isRejected {
                textField.text = "09"
                AppUtility.showToastlocal(message: "Please enter valid other mobile number".localized, view: self.view!)
                return false
            }
            
            if let cell = tbUpdate.cellForRow(at: IndexPath(row: 6, section: 0)) as? UpdateMobileNumberCell {
                if text.count > 2 && textField.tag == 6 {
                    cell.btnClose.isHidden = false
                }else {
                    cell.btnClose.isHidden = true
                }
            }
            
            if textField.tag == 6 {
                let mobileNumberAcceptableCharacterSet = NSCharacterSet(charactersIn: mobileNumberAcceptedChars).inverted
                let filteredSet = string.components(separatedBy: mobileNumberAcceptableCharacterSet).joined(separator: "")
                if string != filteredSet { return false }
                
                if text.count >= rangeCheck.min {
                }
                
                if text.count == rangeCheck.max {
                    textField.text = text
                    textField.resignFirstResponder()
                    return false
                }else if text.count > rangeCheck.max {
                    return false
                }
            }
        }
        return true
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField.tag == 6 {
            if let text = textField.text, text.count < 3 {
                textField.text = "09"
            }
        }
        return true
    }
    
}


extension VMMyAccountViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "UpdateProfilePicCell") as! UpdateProfilePicCell
            return bindDataProfilePic(cell: cell)
        }else if indexPath.row == 1 ||  indexPath.row == 6  {
            let cell = tableView.dequeueReusableCell(withIdentifier: "UpdateMobileNumberCell") as! UpdateMobileNumberCell
            if indexPath.row == 1{
                cell.btnEdit.isHidden = true
            } else{
                cell.btnEdit.isHidden = false
            }
            return bindMobileNumberdata(index: indexPath, cell: cell)
        }else if indexPath.row == 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "UpdateGenderCell") as! UpdateGenderCell
            return bindDataGender(cell: cell)
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "UpdateNameCell") as! UpdateNameCell
            return bindData(index: indexPath, cell: cell)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 200
        }else {
            return 80
        }
    }
    
    private func bindDataProfilePic(cell: UpdateProfilePicCell) -> UpdateProfilePicCell {
        cell.selectionStyle = .none
        cell.lblName.text = FirstName
        cell.imgProfileIamge.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(VMMyAccountViewController.tappedMe))
        cell.imgProfileIamge.addGestureRecognizer(tap)
        
        if profileImage == .taken {
            cell.imgProfileIamge.image = self.userImage
        }else {
            cell.imgProfileIamge.image = UIImage(named: "avtargray")
        }
        return cell
    }
    
    private func bindDataGender(cell: UpdateGenderCell) -> UpdateGenderCell {
        cell.selectionStyle = .none
        if gender == .male {
            cell.imgMale.image = UIImage(named: "act_radio")
            cell.imgFemale.image = UIImage(named: "radio")
            
        }else {
            cell.imgMale.image = UIImage(named: "radio")
            cell.imgFemale.image = UIImage(named: "act_radio")
        }
        cell.btnMale.addTarget(self, action: #selector(self.onSelectGender(_:)), for: .touchUpInside)
        cell.btnFemale.addTarget(self, action: #selector(self.onSelectGender(_:)), for: .touchUpInside)
        cell.lblTitle.text = "Gender".localized
        return cell
    }
    
    private func bindMobileNumberdata(index: IndexPath, cell: UpdateMobileNumberCell) -> UpdateMobileNumberCell {
        cell.tfName.delegate = self
        cell.tfName.tag = index.row
        cell.selectionStyle = .none
        cell.tfName.isUserInteractionEnabled = false
        cell.btnCountry.tag = index.row
        cell.btnCountry.isUserInteractionEnabled = false
        if index.row == 1 {
            //number with edit
            cell.lblTitle.text = "Mobile Number".localized
            cell.btnEdit.setImage(UIImage(named: "editq"), for: .normal)
            cell.tfName.keyboardType = .numberPad
            cell.lblCode.text = "(" + c_codePhone + ") "
            cell.tfName.text = Phone
            
            let (_,flag) = identifyCountryByCode(withPhoneNumber: c_codePhone)
            cell.btnCountry.setImage(UIImage(named: flag), for: .normal)
            if flag == "myanmar" {
                countryName = .Myanmar
            }else {
                countryName = .Other
            }
            
            cell.btnCountry.addTarget(self, action: #selector(VMMyAccountViewController.showCountryViewController(_:)), for: .touchUpInside)
            cell.btnEdit.addTarget(self, action: #selector(self.phoneNumerEdit(_:)), for: .touchUpInside)
        }else if index.row == 6 {
            //Additional mobile number with edit
            cell.lblTitle.text = "Additional Mobile Number".localized
            cell.btnEdit.setImage(UIImage(named: "editq"), for: .normal)
            cell.tfName.placeholder = "Enter Other Number".localized
            cell.tfName.keyboardType = .numberPad
            cell.lblCode.text = "(" + c_codeOtherPhone + ") "
            cell.tfName.text = OtherMobileNumber
            
            let (_,flag) = identifyCountryByCode(withPhoneNumber: c_codeOtherPhone)
            cell.btnCountry.setImage(UIImage(named: flag), for: .normal)
            
            if flag == "myanmar" {
                otherNumber = .Myanmar
            }else {
                otherNumber = .Other
            }
            cell.btnCountry.addTarget(self, action: #selector(VMMyAccountViewController.showCountryViewController(_:)), for: .touchUpInside)
            cell.btnEdit.addTarget(self, action: #selector(self.otherPhoneNumerEdit(_:)), for: .touchUpInside)
            cell.btnClose.addTarget(self, action: #selector(self.clearButtonAction(_:)), for: .touchUpInside)
            
        }
        return cell
    }
    
    private func bindData(index: IndexPath, cell: UpdateNameCell) -> UpdateNameCell {
        cell.tfName.delegate = self
        cell.tfName.tag = index.row
        cell.selectionStyle = .none
        cell.tfName.isUserInteractionEnabled = false
        if index.row == 2 {
            // email id with edit
            cell.lblTitle.text = "Email ID".localized
            cell.btnEdit.setImage(UIImage(named: "editq"), for: .normal)
            cell.tfName.text = Email
            cell.tfName.keyboardType = .emailAddress
            cell.tfName.placeholder = "Enter Email ID".localized
            cell.btnEdit.addTarget(self, action: #selector(self.emailEdit(_:)), for: .touchUpInside)
        }else if index.row == 3 {
            //DOB
            cell.lblTitle.text = "Date of birth".localized
            cell.btnEdit.isHidden = true
            //  cell.tfName.text = DateOfBirthDay + "-" + DateOfBirthMonth + "-" + DateOfBirthYear
            
            if DateOfBirthMonth == "1"
            {
                cell.tfName.text = DateOfBirthDay + "-" + "JAN" + "-" + DateOfBirthYear
            }
            if DateOfBirthMonth == "2"
            {
                cell.tfName.text = DateOfBirthDay + "-" + "FEB" + "-" + DateOfBirthYear
            }
            if DateOfBirthMonth == "3"
            {
                cell.tfName.text = DateOfBirthDay + "-" + "MAR" + "-" + DateOfBirthYear
            }
            if DateOfBirthMonth == "4"
            {
                cell.tfName.text = DateOfBirthDay + "-" + "APR" + "-" + DateOfBirthYear
            }
            if DateOfBirthMonth == "5"
            {
                cell.tfName.text = DateOfBirthDay + "-" + "MAY" + "-" + DateOfBirthYear
            }
            if DateOfBirthMonth == "6"
            {
                cell.tfName.text = DateOfBirthDay + "-" + "JUN" + "-" + DateOfBirthYear
            }
            if DateOfBirthMonth == "7"
            {
                cell.tfName.text = DateOfBirthDay + "-" + "JUL" + "-" + DateOfBirthYear
            }
            if DateOfBirthMonth == "8"
            {
                cell.tfName.text = DateOfBirthDay + "-" + "AUG" + "-" + DateOfBirthYear
            }
            if DateOfBirthMonth == "9"
            {
                cell.tfName.text = DateOfBirthDay + "-" + "SEP" + "-" + DateOfBirthYear
            }
            if DateOfBirthMonth == "10"
            {
                cell.tfName.text = DateOfBirthDay + "-" + "OCT" + "-" + DateOfBirthYear
            }
            if DateOfBirthMonth == "11"
            {
                cell.tfName.text = DateOfBirthDay + "-" + "NOV" + "-" + DateOfBirthYear
            }
            if DateOfBirthMonth == "12"
            {
                cell.tfName.text = DateOfBirthDay + "-" + "DEC" + "-" + DateOfBirthYear
            }
            
            
        }else if index.row == 5 {
            //Password with eye
            cell.lblTitle.text = "Password".localized
            cell.btnEdit.setImage(UIImage(named: "eyes"), for: .normal)
            cell.tfName.text = Password
            cell.tfName.isSecureTextEntry = true
            cell.btnEdit.addTarget(self, action: #selector(self.passwordEdit(_:)), for: .touchUpInside)
        }
        return cell
    }
    
    
}

extension VMMyAccountViewController : PhValidationProtocol {
    
    private func checkValidNumber(number: String) ->(min: Int, max: Int, operator: String, isRejected: Bool, color: String) {
        let tuple = myanmarValidation(number)
        return tuple
    }
}



class UpdateProfilePicCell: UITableViewCell {
    @IBOutlet weak var imgProfileIamge: UIImageView!
    @IBOutlet weak var lblName: UILabel!{
        didSet {
            self.lblName.font = UIFont(name: appFont, size: 15.0)
            self.lblName.text = "".localized
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.imgProfileIamge.layer.masksToBounds = true
        self.imgProfileIamge.layer.borderWidth = 1.0
        self.imgProfileIamge.layer.cornerRadius = self.imgProfileIamge.frame.width / 2.0
        self.imgProfileIamge.layer.borderColor = #colorLiteral(red: 0.001474900637, green: 0.2465611696, blue: 0.9047214985, alpha: 1)
        self.imgProfileIamge.image = UIImage(named: "avtargray")
    }
}
class UpdateNameCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!{
        didSet {
             self.lblTitle.font = UIFont(name: appFont, size: 15.0)
            self.lblTitle.text = "".localized
           
        }
    }
    @IBOutlet weak var btnEdit: UIButton!{
        didSet {
            self.btnEdit.setTitle((btnEdit.titleLabel?.text ?? "").localized, for: .normal)
            btnEdit.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var tfName: UITextField!{
        didSet{
            self.tfName.font = UIFont(name: appFont, size: 15.0)
        }
    }
}

class UpdateGenderCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!{
        didSet {
            self.lblTitle.font = UIFont(name: appFont, size: 15.0)
            self.lblTitle.text = "".localized
            
        }
    }
    @IBOutlet weak var imgMale: UIImageView!
    @IBOutlet weak var lblMale: UILabel!{
        didSet {
            self.lblMale.font = UIFont(name: appFont, size: 15.0)
            self.lblMale.text = "Male".localized
            
        }
    }
    @IBOutlet weak var btnMale: UIButton!{
        didSet {
            self.btnMale.setTitle((btnMale.titleLabel?.text ?? "").localized, for: .normal)
            btnMale.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var imgFemale: UIImageView!
    @IBOutlet weak var lblFemale: UILabel!{
        didSet {
            self.lblFemale.font = UIFont(name: appFont, size: 15.0)
            self.lblFemale.text = "Female".localized
            
        }
    }
    @IBOutlet weak var btnFemale: UIButton!{
        didSet {
            self.btnFemale.setTitle((btnFemale.titleLabel?.text ?? "").localized, for: .normal)
            btnFemale.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
}

class UpdateMobileNumberCell: UITableViewCell {
    @IBOutlet weak var lblTitle: UILabel!{
        didSet {
            self.lblTitle.font = UIFont(name: appFont, size: 15.0)
            self.lblTitle.text = "".localized
            
        }
    }
    @IBOutlet weak var btnEdit: UIButton!{
        didSet {
            self.btnEdit.setTitle((btnEdit.titleLabel?.text ?? "").localized, for: .normal)
            btnEdit.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var tfName: UITextField!{
        didSet{
            self.tfName.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var btnCountry: UIButton!{
        didSet {
            self.btnCountry.setTitle((btnCountry.titleLabel?.text ?? "").localized, for: .normal)
            btnCountry.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var lblCode: UILabel!{
        didSet {
            self.lblCode.font = UIFont(name: appFont, size: 15.0)
            self.lblCode.text = "".localized
            
        }
    }
    
    @IBOutlet weak var btnClose: UIButton!
}

extension VMMyAccountViewController{
    @IBAction func closeCamera(){
        self.cameraContainer.isHidden = true
        self.navigationItem.title = "My Account".localized
    }
    
    @IBAction func captureImage() {
        
        if let videoConnection = stillImageOutput!.connection(with: AVMediaType.video) {
            self.btnCamera.isUserInteractionEnabled = false
            stillImageOutput?.captureStillImageAsynchronously(from: videoConnection) {
                (imageDataSampleBuffer, error) -> Void in
                if let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageDataSampleBuffer!) {
                    self.previewView.image = UIImage(data: imageData)
                    
                    if  self.faceDetection(selectedImage: (UIImage(data: imageData)!)) {
                        let indexpath = NSIndexPath(row: 0, section: 0)
                        let cell = self.tbUpdate.cellForRow(at: indexpath as IndexPath) as? UpdateProfilePicCell
                        cell?.imgProfileIamge.image = UIImage(data: imageData)
                        self.cameraContainer.isHidden = true
                        self.navigationItem.title = "My Account".localized
                        let paramString: [String : Any] = [
                            "MobileNumber" : VMLoginModel.shared.mobileNumber ?? "",
                            "Base64String" : [self.base64ImageData,"","","",""],
                        ]
                        self.aPIManager.UploadImage(params: paramString , url: "https://www.okdollar.co/RestService.svc/GetMultiImageUrlByBase64String", successBlock: {(success, response) in
                            if success {
                                if let firstUrl = response.first as? String {
                                    self.ProfilePictureUrl = firstUrl.replacingOccurrences(of: " ", with: "%20")
                                }
                            }
                            DispatchQueue.main.async {
                                self.btnCamera.isUserInteractionEnabled = true
                            }
                        })
                        self.updateBtn.isEnabled = true
                    }
                }
            }
        }
    }
    
    
    func faceDetection(selectedImage: UIImage) -> Bool{
        var FaceDetectFlag = false
        let imageOptions =  NSDictionary(object: NSNumber(value: 5) as NSNumber, forKey: CIDetectorImageOrientation as NSString)
        let personciImage = CIImage.init(cgImage: selectedImage.cgImage!)
        let accuracy = [CIDetectorAccuracy: CIDetectorAccuracyHigh]
        let faceDetector = CIDetector(ofType: CIDetectorTypeFace, context: nil, options: accuracy)
        let faces = faceDetector?.features(in: personciImage, options: imageOptions as? [String : AnyObject])
        let resizedeImg = AppUtility.resize(selectedImage)
        
        if let faces = faces {
            self.btnCamera.isUserInteractionEnabled = true
            if faces.count > 1 {
                AppUtility.showToastlocal(message: "More than one faces".localized, view: self.view)
                FaceDetectFlag = false
                self.profileImage = .notTaken
            }else {
                if let face = faces.first as? CIFaceFeature {
                    println_debug("found bounds are \(face.bounds)")
                    base64ImageData = self.imageTobase64(image: resizedeImg)
                    self.userImage = UIImage(named: "")
                    self.userImage = selectedImage
                    self.profileImage = .taken
                    FaceDetectFlag = true
                    self.tbUpdate.reloadData()
                }else {
                    FaceDetectFlag = false
                    AppUtility.showToastlocal(message: "No Face detected,Required Face.Please press capture button one more time to click your picture".localized, view: self.view)
                    self.profileImage = .notTaken
                }
            }
        }
        return FaceDetectFlag
    }
    
}
