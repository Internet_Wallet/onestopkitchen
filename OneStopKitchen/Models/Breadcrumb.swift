//
//  Breadcrumb.swift
//  NopCommerce
//
//  Created by BS-125 on 11/20/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit
import ObjectMapper

class Breadcrumb: Mappable {
    
    var Breadcrumb_CustomProperties :         NSString?
    var Enabled :                             Bool = false
    var Breadcrumb_ProductId :                Int = 0
    var ProductName :                         NSString?
    var ProductSeName :                       NSString?
    var categoryBreadCrumb:                   [CategoryBreadcrumb]?
    
    required init?(map: Map) {
        
    }
    func mapping(map: Map) {
        self.Breadcrumb_CustomProperties   <- map["CustomProperties"]
        self.Enabled                       <- map["Enabled"]
        self.Breadcrumb_ProductId          <- map["ProductId"]
        self.ProductName                   <- map["ProductName"]
        self.ProductSeName                 <- map["ProductSeName"]
        self.categoryBreadCrumb            <- map["CategoryBreadcrumb"]
    }
}

class CategoryBreadcrumb: Mappable {
    
    var CustomProperties :        NSDictionary?
    var idd  :                    Int = 0
    var IncludeInTopMenu :        Int = 0
    var Name :                    NSString?
    var NumberOfProducts :        NSString?
    var SeName :                  NSString?
    var SubCategories :           NSArray?
    
    required init?(map: Map) {
    }
    func mapping(map: Map) {
        self.CustomProperties       <- map["CustomProperties"]
        self.idd                    <- map["Id"]
        self.IncludeInTopMenu       <- map["IncludeInTopMenu"]
        self.Name                   <- map["Name"]
        self.NumberOfProducts       <- map["NumberOfProducts"]
        self.SeName                 <- map["SeName"]
        self.SubCategories          <- map["SubCategories"]
    }
}

