//
//  HomePageCategories.swift
//  NopCommerce
//
//  Created by BS-125 on 11/6/15.
//  Copyright © 2015 Jahid Hassan. All rights reserved.
//

import UIKit

open class HomePageCategories: NSObject {
    
    var name :              String
    var imageUrl :          String
    var idd :                NSInteger
    
    init(name : String, imageUrl : String , idd : NSInteger) {
        self.name = name
        self.imageUrl = imageUrl
        self.idd = idd
    }
    
}
