//
//  VMCheckoutDTO.swift
//  VMart
//
//  Created by Kethan on 12/6/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import Foundation

struct PaymentSuccessModel: Codable {
    let code: Int?
    let data: String?
    let message: String?
    
    enum CodingKeys: String, CodingKey {
        case code = "Code"
        case data = "Data"
        case message = "Msg"
    }
}

struct PaymentModel: Codable {
    let resultdescription: String?
    let source: String?
    let agentcode: String?
    let amount: Double?
    let transid: String?
    let requestcts: String?
    let responsects: String?
    let destination: String?
    let walletbalance: Double?
    let prewalletbalance: Double?
    let comments: String?
    let agentname: String?
    
    enum CodingKeys: String, CodingKey {
        case resultdescription = "resultdescription"
        case source = "source"
        case agentcode = "agentcode"
        case amount = "amount"
        case transid = "transid"
        case requestcts = "requestcts"
        case responsects = "responsects"
        case destination = "destination"
        case walletbalance = "walletbalance"
        case prewalletbalance = "prewalletbalance"
        case comments = "comments"
        case agentname = "agentname"
    }
}

//MARK:- CheckoutComplete
struct VMCheckoutCompleteModel: Codable {
    
    let orderID, orderGroupNumber: Int?
    let orderIDS: [Int]?
    let customOrderNumber: JSONNull?
    let customOrderNumbers: [String]?
    let completeOrder: Bool?
    let payPal: JSONNull?
    let paymentType: Int?
    let successMessage: JSONNull?
    let statusCode: Int?
    let errorList: [String]?
    
    enum CodingKeys: String, CodingKey {
        case orderID = "OrderId"
        case orderGroupNumber = "OrderGroupNumber"
        case orderIDS = "OrderIds"
        case customOrderNumber = "CustomOrderNumber"
        case customOrderNumbers = "CustomOrderNumbers"
        case completeOrder = "CompleteOrder"
        case payPal = "PayPal"
        case paymentType = "PaymentType"
        case successMessage = "SuccessMessage"
        case statusCode = "StatusCode"
        case errorList = "ErrorList"
    }
}

//MARK:- UpdatePaymentStatus
struct VMUpdatePaymentStatusModel: Codable {
    let orderGroupNumber, referenceNumber, shippingAddress, phoneNumber: String?
    let productAmount, shippingCharges, totalPaidAmount: String?
    let orders: [VMOrder]?
    let successMessage: String?
    let statusCode: Int?
    let errorList: [String]?
    
    enum CodingKeys: String, CodingKey {
        case orderGroupNumber = "OrderGroupNumber"
        case referenceNumber = "ReferenceNumber"
        case shippingAddress = "ShippingAddress"
        case phoneNumber = "PhoneNumber"
        case productAmount = "ProductAmount"
        case shippingCharges = "ShippingCharges"
        case totalPaidAmount = "TotalPaidAmount"
        case orders = "Orders"
        case successMessage = "SuccessMessage"
        case statusCode = "StatusCode"
        case errorList = "ErrorList"
    }
}

struct VMOrder: Codable {
    let id: Int?
    let printMode, pdfInvoiceDisabled: Bool?
    let customOrderNumber, createdOn, expectedDeliveryDate, orderStatus: String?
    let isReOrderAllowed, isReturnRequestAllowed, isShippable, pickUpInStore: Bool?
    let shippingStatus: String?
    let shippingAddress, pickupAddress: VMAddress?
    let shippingMethod: String?
    let shipments: [String]?
    let billingAddress: VMBillingAddress?
    let vatNumber: String?
    let paymentMethod, paymentMethodStatus: String?
    let canRePostProcessPayment: Bool?
    let customValues: VMCustom?
    let orderSubtotal: String?
    let orderSubTotalDiscount: Double?
    let orderShipping: String?
    let paymentMethodAdditionalFee: Double?
    let checkoutAttributeInfo: String?
    let pricesIncludeTax, displayTaxShippingInfo: Bool?
    let tax: String?
    let taxRates: [VMTaxRate]?
    let displayTax, displayTaxRates: Bool?
    let orderTotalDiscount: Double?
    let redeemedRewardPoints: Int?
    let redeemedRewardPointsAmount: Double?
    let orderTotal: String?
    let giftCards: [String]?
    let showSku: Bool?
    let items: [VMItem]?
    let orderNotes: [String]?
    let successMessage: String?
    let statusCode: Int?
    let errorList: [String]?
    
    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case printMode = "PrintMode"
        case pdfInvoiceDisabled = "PdfInvoiceDisabled"
        case customOrderNumber = "CustomOrderNumber"
        case createdOn = "CreatedOn"
        case expectedDeliveryDate = "ExpectedDeliveryDate"
        case orderStatus = "OrderStatus"
        case isReOrderAllowed = "IsReOrderAllowed"
        case isReturnRequestAllowed = "IsReturnRequestAllowed"
        case isShippable = "IsShippable"
        case pickUpInStore = "PickUpInStore"
        case shippingStatus = "ShippingStatus"
        case shippingAddress = "ShippingAddress"
        case pickupAddress = "PickupAddress"
        case shippingMethod = "ShippingMethod"
        case shipments = "Shipments"
        case billingAddress = "BillingAddress"
        case vatNumber = "VatNumber"
        case paymentMethod = "PaymentMethod"
        case paymentMethodStatus = "PaymentMethodStatus"
        case canRePostProcessPayment = "CanRePostProcessPayment"
        case customValues = "CustomValues"
        case orderSubtotal = "OrderSubtotal"
        case orderSubTotalDiscount = "OrderSubTotalDiscount"
        case orderShipping = "OrderShipping"
        case paymentMethodAdditionalFee = "PaymentMethodAdditionalFee"
        case checkoutAttributeInfo = "CheckoutAttributeInfo"
        case pricesIncludeTax = "PricesIncludeTax"
        case displayTaxShippingInfo = "DisplayTaxShippingInfo"
        case tax = "Tax"
        case taxRates = "TaxRates"
        case displayTax = "DisplayTax"
        case displayTaxRates = "DisplayTaxRates"
        case orderTotalDiscount = "OrderTotalDiscount"
        case redeemedRewardPoints = "RedeemedRewardPoints"
        case redeemedRewardPointsAmount = "RedeemedRewardPointsAmount"
        case orderTotal = "OrderTotal"
        case giftCards = "GiftCards"
        case showSku = "ShowSku"
        case items = "Items"
        case orderNotes = "OrderNotes"
        case successMessage = "SuccessMessage"
        case statusCode = "StatusCode"
        case errorList = "ErrorList"
    }
}

struct VMBillingAddress: Codable {
    let firstName, lastName, email: String?
    let companyEnabled, companyRequired: Bool?
    let company: String?
    let countryEnabled: Bool?
    let countryID: Int?
    let countryName: String?
    let stateProvinceEnabled: Bool?
    let stateProvinceID: Int?
    let stateProvinceName: String?
    let cityEnabled, cityRequired: Bool?
    let city: String?
    let cityID: Int?
    let streetAddressEnabled, streetAddressRequired: Bool?
    let address1: String?
    let streetAddress2Enabled, streetAddress2Required: Bool?
    let address2: String?
    let zipPostalCodeEnabled, zipPostalCodeRequired: Bool?
    let zipPostalCode: Int?
    let phoneEnabled, phoneRequired: Bool?
    let phoneNumber: String?
    let faxEnabled, faxRequired: Bool?
    let faxNumber: String?
    let availableCountries, availableStates, availableCities: [String]?
    let formattedCustomAddressAttributes: String?
    let customAddressAttributes: [String]?
    let isDeliveryAllowed: Bool?
    let houseNo, floorNo, roomNo: String?
    let isLiftOption: Bool?
    let ropeColor: String?
    let id: Int?
    let form: String?
    let customProperties: VMCustom?
    
    enum CodingKeys: String, CodingKey {
        case firstName = "FirstName"
        case lastName = "LastName"
        case email = "Email"
        case companyEnabled = "CompanyEnabled"
        case companyRequired = "CompanyRequired"
        case company = "Company"
        case countryEnabled = "CountryEnabled"
        case countryID = "CountryId"
        case countryName = "CountryName"
        case stateProvinceEnabled = "StateProvinceEnabled"
        case stateProvinceID = "StateProvinceId"
        case stateProvinceName = "StateProvinceName"
        case cityEnabled = "CityEnabled"
        case cityRequired = "CityRequired"
        case city = "City"
        case cityID = "CityId"
        case streetAddressEnabled = "StreetAddressEnabled"
        case streetAddressRequired = "StreetAddressRequired"
        case address1 = "Address1"
        case streetAddress2Enabled = "StreetAddress2Enabled"
        case streetAddress2Required = "StreetAddress2Required"
        case address2 = "Address2"
        case zipPostalCodeEnabled = "ZipPostalCodeEnabled"
        case zipPostalCodeRequired = "ZipPostalCodeRequired"
        case zipPostalCode = "ZipPostalCode"
        case phoneEnabled = "PhoneEnabled"
        case phoneRequired = "PhoneRequired"
        case phoneNumber = "PhoneNumber"
        case faxEnabled = "FaxEnabled"
        case faxRequired = "FaxRequired"
        case faxNumber = "FaxNumber"
        case availableCountries = "AvailableCountries"
        case availableStates = "AvailableStates"
        case availableCities = "AvailableCities"
        case formattedCustomAddressAttributes = "FormattedCustomAddressAttributes"
        case customAddressAttributes = "CustomAddressAttributes"
        case isDeliveryAllowed = "IsDeliveryAllowed"
        case houseNo = "HouseNo"
        case floorNo = "FloorNo"
        case roomNo = "RoomNo"
        case isLiftOption = "IsLiftOption"
        case ropeColor = "RopeColor"
        case id = "Id"
        case form = "Form"
        case customProperties = "CustomProperties"
    }
}

struct VMCustom: Codable {
}

struct VMItem: Codable {
    let orderItemGUID, sku: String?
    let productID: Int?
    let productName, productSEName, unitPrice, subTotal: String?
    let quantity: Int?
    let attributeInfo: String?
    let rentalInfo: String?
    let picture: VMPicture?
    let downloadID, licenseID: Int?
    let productReviewOverview: VMProductReviewOverview?
    let discountPercentage: Int?
    let discountOfferName, discountAmount, priceAfterDiscount: String?
    let id: Int?
    let form: String?
    let customProperties: VMCustom?
    
    enum CodingKeys: String, CodingKey {
        case orderItemGUID = "OrderItemGuid"
        case sku = "Sku"
        case productID = "ProductId"
        case productName = "ProductName"
        case productSEName = "ProductSeName"
        case unitPrice = "UnitPrice"
        case subTotal = "SubTotal"
        case quantity = "Quantity"
        case attributeInfo = "AttributeInfo"
        case rentalInfo = "RentalInfo"
        case picture = "Picture"
        case downloadID = "DownloadId"
        case licenseID = "LicenseId"
        case productReviewOverview = "ProductReviewOverview"
        case discountPercentage = "DiscountPercentage"
        case discountOfferName = "DiscountOfferName"
        case discountAmount = "DiscountAmount"
        case priceAfterDiscount = "PriceAfterDiscount"
        case id = "Id"
        case form = "Form"
        case customProperties = "CustomProperties"
    }
}

struct VMPicture: Codable {
    let imageURL: String?
    let fullSizeImageURL, title, alternateText, form: String?
    let customProperties: VMCustom?
    
    enum CodingKeys: String, CodingKey {
        case imageURL = "ImageUrl"
        case fullSizeImageURL = "FullSizeImageUrl"
        case title = "Title"
        case alternateText = "AlternateText"
        case form = "Form"
        case customProperties = "CustomProperties"
    }
}

struct VMProductReviewOverview: Codable {
    let productID, ratingSum: Int?
    let allowCustomerReviews: Bool?
    let totalReviews: Int?
    
    enum CodingKeys: String, CodingKey {
        case productID = "ProductId"
        case ratingSum = "RatingSum"
        case allowCustomerReviews = "AllowCustomerReviews"
        case totalReviews = "TotalReviews"
    }
}

struct VMAddress: Codable {
    let firstName, lastName, email: String?
    let companyEnabled, companyRequired: Bool?
    let company: String?
    let countryEnabled: Bool?
    let countryID, stateProvinceID, cityID: Int?
    let stateProvinceEnabled: Bool?
    let countryName, stateProvinceName, city: String?
    let cityEnabled, cityRequired: Bool?
    let streetAddressEnabled, streetAddressRequired: Bool?
    let address1, address2: String?
    let streetAddress2Enabled, streetAddress2Required: Bool?
    let zipPostalCodeEnabled, zipPostalCodeRequired: Bool?
    let zipPostalCode: Int?
    let phoneEnabled, phoneRequired: Bool?
    let phoneNumber: String?
    let faxEnabled, faxRequired: Bool?
    let faxNumber: Int?
    let availableCountries, availableStates, availableCities: [String]?
    let formattedCustomAddressAttributes: String?
    let customAddressAttributes: [String]?
    let isDeliveryAllowed: Bool?
    let houseNo, floorNo, roomNo: String?
    let isLiftOption: Bool?
    let ropeColor: String?
    let id: Int?
    let form: String?
    let customProperties: VMCustom?
    
    enum CodingKeys: String, CodingKey {
        case firstName = "FirstName"
        case lastName = "LastName"
        case email = "Email"
        case companyEnabled = "CompanyEnabled"
        case companyRequired = "CompanyRequired"
        case company = "Company"
        case countryEnabled = "CountryEnabled"
        case countryID = "CountryId"
        case countryName = "CountryName"
        case stateProvinceEnabled = "StateProvinceEnabled"
        case stateProvinceID = "StateProvinceId"
        case stateProvinceName = "StateProvinceName"
        case cityEnabled = "CityEnabled"
        case cityRequired = "CityRequired"
        case city = "City"
        case cityID = "CityId"
        case streetAddressEnabled = "StreetAddressEnabled"
        case streetAddressRequired = "StreetAddressRequired"
        case address1 = "Address1"
        case streetAddress2Enabled = "StreetAddress2Enabled"
        case streetAddress2Required = "StreetAddress2Required"
        case address2 = "Address2"
        case zipPostalCodeEnabled = "ZipPostalCodeEnabled"
        case zipPostalCodeRequired = "ZipPostalCodeRequired"
        case zipPostalCode = "ZipPostalCode"
        case phoneEnabled = "PhoneEnabled"
        case phoneRequired = "PhoneRequired"
        case phoneNumber = "PhoneNumber"
        case faxEnabled = "FaxEnabled"
        case faxRequired = "FaxRequired"
        case faxNumber = "FaxNumber"
        case availableCountries = "AvailableCountries"
        case availableStates = "AvailableStates"
        case availableCities = "AvailableCities"
        case formattedCustomAddressAttributes = "FormattedCustomAddressAttributes"
        case customAddressAttributes = "CustomAddressAttributes"
        case isDeliveryAllowed = "IsDeliveryAllowed"
        case houseNo = "HouseNo"
        case floorNo = "FloorNo"
        case roomNo = "RoomNo"
        case isLiftOption = "IsLiftOption"
        case ropeColor = "RopeColor"
        case id = "Id"
        case form = "Form"
        case customProperties = "CustomProperties"
    }
}

struct VMTaxRate: Codable {
    let rate, value: String?
    let form: String?
    let customProperties: VMCustom?
    
    enum CodingKeys: String, CodingKey {
        case rate = "Rate"
        case value = "Value"
        case form = "Form"
        case customProperties = "CustomProperties"
    }
}

// MARK: Encode/decode helpers

class JSONNull: Codable, Hashable {
    
    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }
    
    public var hashValue: Int {
        return 0
    }
    
    public init() {}
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}

class JSONCodingKey: CodingKey {
    let key: String
    
    required init?(intValue: Int) {
        return nil
    }
    
    required init?(stringValue: String) {
        key = stringValue
    }
    
    var intValue: Int? {
        return nil
    }
    
    var stringValue: String {
        return key
    }
}

class JSONAny: Codable {
    let value: Any
    
    static func decodingError(forCodingPath codingPath: [CodingKey]) -> DecodingError {
        let context = DecodingError.Context(codingPath: codingPath, debugDescription: "Cannot decode JSONAny")
        return DecodingError.typeMismatch(JSONAny.self, context)
    }
    
    static func encodingError(forValue value: Any, codingPath: [CodingKey]) -> EncodingError {
        let context = EncodingError.Context(codingPath: codingPath, debugDescription: "Cannot encode JSONAny")
        return EncodingError.invalidValue(value, context)
    }
    
    static func decode(from container: SingleValueDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if container.decodeNil() {
            return JSONNull()
        }
        throw decodingError(forCodingPath: container.codingPath)
    }
    
    static func decode(from container: inout UnkeyedDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if let value = try? container.decodeNil() {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer() {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }
    
    static func decode(from container: inout KeyedDecodingContainer<JSONCodingKey>, forKey key: JSONCodingKey) throws -> Any {
        if let value = try? container.decode(Bool.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Int64.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Double.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(String.self, forKey: key) {
            return value
        }
        if let value = try? container.decodeNil(forKey: key) {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer(forKey: key) {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }
    
    static func decodeArray(from container: inout UnkeyedDecodingContainer) throws -> [Any] {
        var arr: [Any] = []
        while !container.isAtEnd {
            let value = try decode(from: &container)
            arr.append(value)
        }
        return arr
    }
    
    static func decodeDictionary(from container: inout KeyedDecodingContainer<JSONCodingKey>) throws -> [String: Any] {
        var dict = [String: Any]()
        for key in container.allKeys {
            let value = try decode(from: &container, forKey: key)
            dict[key.stringValue] = value
        }
        return dict
    }
    
    static func encode(to container: inout UnkeyedEncodingContainer, array: [Any]) throws {
        for value in array {
            if let value = value as? Bool {
                try container.encode(value)
            } else if let value = value as? Int64 {
                try container.encode(value)
            } else if let value = value as? Double {
                try container.encode(value)
            } else if let value = value as? String {
                try container.encode(value)
            } else if value is JSONNull {
                try container.encodeNil()
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer()
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }
    
    static func encode(to container: inout KeyedEncodingContainer<JSONCodingKey>, dictionary: [String: Any]) throws {
        for (key, value) in dictionary {
            let key = JSONCodingKey(stringValue: key)!
            if let value = value as? Bool {
                try container.encode(value, forKey: key)
            } else if let value = value as? Int64 {
                try container.encode(value, forKey: key)
            } else if let value = value as? Double {
                try container.encode(value, forKey: key)
            } else if let value = value as? String {
                try container.encode(value, forKey: key)
            } else if value is JSONNull {
                try container.encodeNil(forKey: key)
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer(forKey: key)
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }
    
    static func encode(to container: inout SingleValueEncodingContainer, value: Any) throws {
        if let value = value as? Bool {
            try container.encode(value)
        } else if let value = value as? Int64 {
            try container.encode(value)
        } else if let value = value as? Double {
            try container.encode(value)
        } else if let value = value as? String {
            try container.encode(value)
        } else if value is JSONNull {
            try container.encodeNil()
        } else {
            throw encodingError(forValue: value, codingPath: container.codingPath)
        }
    }
    
    public required init(from decoder: Decoder) throws {
        if var arrayContainer = try? decoder.unkeyedContainer() {
            self.value = try JSONAny.decodeArray(from: &arrayContainer)
        } else if var container = try? decoder.container(keyedBy: JSONCodingKey.self) {
            self.value = try JSONAny.decodeDictionary(from: &container)
        } else {
            let container = try decoder.singleValueContainer()
            self.value = try JSONAny.decode(from: container)
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        if let arr = self.value as? [Any] {
            var container = encoder.unkeyedContainer()
            try JSONAny.encode(to: &container, array: arr)
        } else if let dict = self.value as? [String: Any] {
            var container = encoder.container(keyedBy: JSONCodingKey.self)
            try JSONAny.encode(to: &container, dictionary: dict)
        } else {
            var container = encoder.singleValueContainer()
            try JSONAny.encode(to: &container, value: self.value)
        }
    }
}
