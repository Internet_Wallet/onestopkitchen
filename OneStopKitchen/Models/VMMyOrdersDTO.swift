//
//  VMMyOrdersDTO.swift
//  VMart
//
//  Created by Kethan on 12/12/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import Foundation


struct VMMyOrderModel: Codable {
    let orders: [VMMyOrder]?
    let recurringOrders, cancelRecurringPaymentErrors: [String]?
    let successMessage: String?
    let statusCode: Int?
    let errorList: [String]?
    
    enum CodingKeys: String, CodingKey {
        case orders = "Orders"
        case recurringOrders = "RecurringOrders"
        case cancelRecurringPaymentErrors = "CancelRecurringPaymentErrors"
        case successMessage = "SuccessMessage"
        case statusCode = "StatusCode"
        case errorList = "ErrorList"
    }
}

struct VMMyOrder: Codable {
    let expectedDeliveryDate, customOrderNumber, orderDeliveryCode, orderTotal: String?
    let isReturnRequestAllowed: Bool?
    let orderStatusEnum: Int?
    let orderStatus, paymentStatus, shippingStatus, createdOn: String?
    let items: [VMMyItem]?
    let id: Int?
    let form: String?
    let customProperties: VMCustomProperties?
    
    enum CodingKeys: String, CodingKey {
        case expectedDeliveryDate = "ExpectedDeliveryDate"
        case customOrderNumber = "CustomOrderNumber"
        case orderDeliveryCode = "OrderDeliveryCode"
        case orderTotal = "OrderTotal"
        case isReturnRequestAllowed = "IsReturnRequestAllowed"
        case orderStatusEnum = "OrderStatusEnum"
        case orderStatus = "OrderStatus"
        case paymentStatus = "PaymentStatus"
        case shippingStatus = "ShippingStatus"
        case createdOn = "CreatedOn"
        case items = "Items"
        case id = "Id"
        case form = "Form"
        case customProperties = "CustomProperties"
    }
}

struct VMCustomProperties: Codable {
}

struct VMMyItem: Codable {
    let orderItemGUID, sku: String?
    let productID: Int?
    let productName, productSEName, unitPrice, subTotal: String?
    let quantity: Int?
    let attributeInfo: String?
    let rentalInfo: String?
    let picture: VMMyPicture?
    let downloadID, licenseID, id: Int?
    let form: String?
    let customProperties: VMCustomProperties?
    
    enum CodingKeys: String, CodingKey {
        case orderItemGUID = "OrderItemGuid"
        case sku = "Sku"
        case productID = "ProductId"
        case productName = "ProductName"
        case productSEName = "ProductSeName"
        case unitPrice = "UnitPrice"
        case subTotal = "SubTotal"
        case quantity = "Quantity"
        case attributeInfo = "AttributeInfo"
        case rentalInfo = "RentalInfo"
        case picture = "Picture"
        case downloadID = "DownloadId"
        case licenseID = "LicenseId"
        case id = "Id"
        case form = "Form"
        case customProperties = "CustomProperties"
    }
}

struct VMMyPicture: Codable {
    let imageURL: String?
    let fullSizeImageURL, title, alternateText, form: String?
    let customProperties: VMCustomProperties?
    
    enum CodingKeys: String, CodingKey {
        case imageURL = "ImageUrl"
        case fullSizeImageURL = "FullSizeImageUrl"
        case title = "Title"
        case alternateText = "AlternateText"
        case form = "Form"
        case customProperties = "CustomProperties"
    }
}




//MyOrderDetail
struct VMMyOrderDetail: Codable {
    let id: Int?
    let printMode, pdfInvoiceDisabled: Bool?
    let customOrderNumber, createdOn, expectedDeliveryDate, orderStatus: String?
    let isReOrderAllowed, isReturnRequestAllowed, isShippable, pickUpInStore: Bool?
    let shippingStatus: String?
    let shippingAddress, pickupAddress: VMAddress?
    let shippingMethod: String?
    let shipments: [String]?
    let billingAddress: VMBillingAddress?
    let vatNumber: String?
    let paymentMethod, paymentMethodStatus: String?
    let canRePostProcessPayment: Bool?
    let customValues: VMCustom?
    let orderSubtotal: String?
    let orderSubTotalDiscount: String?
    let orderShipping: String?
    let paymentMethodAdditionalFee: String?
    let checkoutAttributeInfo: String?
    let pricesIncludeTax, displayTaxShippingInfo: Bool?
    let tax: String?
    let taxRates: [VMTaxRate]?
    let displayTax, displayTaxRates: Bool?
    let orderTotalDiscount: String?
    let redeemedRewardPoints: Int?
    let redeemedRewardPointsAmount: String?
    let orderTotal: String?
    let giftCards: [String]?
    let showSku: Bool?
    let items: [VMItem]?
    let orderNotes: [String]?
    let successMessage: String?
    let statusCode: Int?
    let errorList: [String]?
    
    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case printMode = "PrintMode"
        case pdfInvoiceDisabled = "PdfInvoiceDisabled"
        case customOrderNumber = "CustomOrderNumber"
        case createdOn = "CreatedOn"
        case expectedDeliveryDate = "ExpectedDeliveryDate"
        case orderStatus = "OrderStatus"
        case isReOrderAllowed = "IsReOrderAllowed"
        case isReturnRequestAllowed = "IsReturnRequestAllowed"
        case isShippable = "IsShippable"
        case pickUpInStore = "PickUpInStore"
        case shippingStatus = "ShippingStatus"
        case shippingAddress = "ShippingAddress"
        case pickupAddress = "PickupAddress"
        case shippingMethod = "ShippingMethod"
        case shipments = "Shipments"
        case billingAddress = "BillingAddress"
        case vatNumber = "VatNumber"
        case paymentMethod = "PaymentMethod"
        case paymentMethodStatus = "PaymentMethodStatus"
        case canRePostProcessPayment = "CanRePostProcessPayment"
        case customValues = "CustomValues"
        case orderSubtotal = "OrderSubtotal"
        case orderSubTotalDiscount = "OrderSubTotalDiscount"
        case orderShipping = "OrderShipping"
        case paymentMethodAdditionalFee = "PaymentMethodAdditionalFee"
        case checkoutAttributeInfo = "CheckoutAttributeInfo"
        case pricesIncludeTax = "PricesIncludeTax"
        case displayTaxShippingInfo = "DisplayTaxShippingInfo"
        case tax = "Tax"
        case taxRates = "TaxRates"
        case displayTax = "DisplayTax"
        case displayTaxRates = "DisplayTaxRates"
        case orderTotalDiscount = "OrderTotalDiscount"
        case redeemedRewardPoints = "RedeemedRewardPoints"
        case redeemedRewardPointsAmount = "RedeemedRewardPointsAmount"
        case orderTotal = "OrderTotal"
        case giftCards = "GiftCards"
        case showSku = "ShowSku"
        case items = "Items"
        case orderNotes = "OrderNotes"
        case successMessage = "SuccessMessage"
        case statusCode = "StatusCode"
        case errorList = "ErrorList"
    }
}


