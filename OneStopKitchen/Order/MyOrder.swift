//
//  MyOrder.swift
//  VMart
//
//  Created by ANTONY on 29/10/2018.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit

class MyOrder: MartBaseViewController {
    @IBOutlet var MyOrderTableView: UITableView!
    
    @IBOutlet weak var noRecordFoundlbl: UILabel!{
        didSet {
            self.noRecordFoundlbl.font = UIFont(name: appFont, size: 15.0)
            self.noRecordFoundlbl.text = self.noRecordFoundlbl.text?.localized
            
        }
    }
    @IBOutlet weak var noRecordView: UIView! {
        didSet {
            self.noRecordView.isHidden = true
        }
    }
    @IBOutlet var cartBarButton: UIBarButtonItem!
    
    @IBOutlet weak var continueShopingBtn: UIButton! {
        didSet {
            //            continueBtn.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
            continueShopingBtn.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1), UIColor.init(red: 20.0/255.0, green: 185.0/255.0, blue: 85.0/255.0, alpha: 1.0)])
            self.continueShopingBtn.setTitle("Continue Shopping".localized, for: .normal)
            self.continueShopingBtn.layer.cornerRadius = 4
            self.continueShopingBtn.layer.masksToBounds = true
            continueShopingBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
            
        }
    }
    
    
    @IBOutlet var infoLbl: UILabel!{
        didSet {
            self.infoLbl.font = UIFont(name: appFont, size: 15.0)
            self.infoLbl.text = self.infoLbl.text?.localized
            self.infoLbl.textColor = UIColor.black
            
        }
    }
    
    
    var searchButton: UIBarButtonItem!
    
    var ordersArray = [VMMyOrder]()
    var aPIManager = APIManager()
    var selectedProduct = -1
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         self.navigationItem.title = "My Orders".localized
        //        self.cartBarButton = UIBarButtonItem.init(badge: UserDefaults.standard.value(forKey: "badge") as? String, title: "", target: self, action: #selector(self.cartAction(_:)))
        self.cartBarButton = UIBarButtonItem.init(badge: (UserDefaults.standard.string(forKey: "badge") ?? ""), title: "", target: self, action: #selector(self.cartAction(_:)))
        self.searchButton = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonItem.SystemItem.search, target: self, action: #selector(self.searchAction(_:)))
        self.navigationItem.rightBarButtonItems = [ self.cartBarButton, self.searchButton]
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        MyOrderTableView.tableFooterView = UIView()
       
        getOrderDetails()
    }
    
    @IBAction func cartAction(_ sender: UIBarButtonItem) {
        let viewController = UIStoryboard(name: "Cart", bundle: Bundle.main).instantiateViewController(withIdentifier: "CartRoot")
        self.present(viewController, animated: true, completion: nil)
    }
    
    @IBAction func backAction(_ sender: UIBarButtonItem) {
        //        self.dismissController()
        self.performSegue(withIdentifier: "unwindToHome", sender: self)
    }
    
    @IBAction func continueShopAction(_ sender: UIButton) {
        self.performSegue(withIdentifier: "unwindToHome", sender: self)
        //        self.dismissController()
    }
    
    func dismissController(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func searchAction(_ sender: UIBarButtonItem) {
        
        let viewController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SearchProducts")
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    private func parseItems(model: VMMyOrderModel) {
        DispatchQueue.main.async {
            if let orderArray = model.orders, orderArray.count > 0 {
                self.ordersArray = orderArray
                self.MyOrderTableView.reloadData()
            } else {
                self.noRecordView.isHidden = false
            }
        }
    }
    
    func showOrderDetailViewController() {
        self.performSegue(withIdentifier: "MyOrderDetailSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? MyOrderDetailViewController {
            vc.selectedProductId = ordersArray[selectedProduct].id ?? 0
        }
    }
    
    private func getOrderDetails() {
        let urlString = String(format: "%@/order/customerorders", APIManagerClient.sharedInstance.base_url)
        guard let url = URL(string: urlString) else { return }
        let params = Dictionary<String, Any>()
        AppUtility.showLoading(self.view)
        aPIManager.genericClass(url: url, param: params as AnyObject, httpMethod: "GET", header: true) { [weak self] (response, success, data) in
            DispatchQueue.main.async {
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                }
            }
            if success {
                do {
                    let decoder = JSONDecoder()
                    guard let dataVal = data else { return }
                    let model = try decoder.decode(VMMyOrderModel.self, from: dataVal)
                    if let statusCode = model.statusCode, statusCode == 200 {
                        self?.parseItems(model: model)
                    } else {
                        println_debug("Error")
                    }
                } catch _ {
                    println_debug("Parse error")
                }
            } else {
                DispatchQueue.main.async {
                    self?.noRecordView.isHidden = false
                }
            }
        }
    }
}

extension MyOrder: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.ordersArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyOrderCell", for: indexPath) as! MyOrderTableViewCell
        cell.wrapData(order: self.ordersArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedProduct = indexPath.row
        self.showOrderDetailViewController()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

class MyOrderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var itemName: UILabel!{
        didSet {
            self.itemName.font = UIFont(name: appFont, size: 15.0)
            self.itemName.text = self.itemName.text?.localized
            
        }
    }
    @IBOutlet weak var itemDescription: UILabel!{
        didSet {
            self.itemDescription.font = UIFont(name: appFont, size: 15.0)
            self.itemDescription.text = self.itemDescription.text?.localized
            
        }
    }
    @IBOutlet weak var itemPrice: UILabel!{
        didSet {
            self.itemPrice.font = UIFont(name: appFont, size: 15.0)
            self.itemPrice.text = self.itemPrice.text?.localized
            
        }
    }
    @IBOutlet weak var qtyLabel: UILabel!{
        didSet {
            self.qtyLabel.font = UIFont(name: appFont, size: 15.0)
            self.qtyLabel.text = self.qtyLabel.text?.localized
            
        }
    }
    
    @IBOutlet weak var orderNumberLabel: UILabel!{
        didSet {
            self.orderNumberLabel.font = UIFont(name: appFont, size: 15.0)
            self.orderNumberLabel.text = self.orderNumberLabel.text?.localized
            
        }
    }
    
    let imageCache = NSCache<AnyObject, AnyObject>()
    var images = [UIImage]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func wrapData(order: VMMyOrder) {
        
        var nameTxt = ""
        var imageArray = [String]()
        itemImageView.animationImages = []
        self.images = []
        if let items = order.items {
            if items.count > 3 {
                for (index, item) in items.enumerated() {
                    nameTxt = "\(nameTxt), \(item.productName ?? "")"
                    imageArray.append(item.picture?.imageURL ?? "")
                    if index == 2 {
                        break
                    }
                }
            } else {
                for item in items {
                    nameTxt = "\(nameTxt), \(item.productName ?? "")"
                    imageArray.append(item.picture?.imageURL ?? "")
                }
            }
        }
        
        let _ = nameTxt.remove(at: nameTxt.startIndex)
        itemName.text = String(nameTxt)
        itemPrice.text = order.orderTotal ?? "0.0 MMK"
        itemPrice.amountAttributedString()
        self.orderNumberLabel.text = "Order Number: \(order.customOrderNumber ?? "1 Stop Mart")"
        
        if let items = order.items {
            var totalQty = 0
            for  item in items {
                
                totalQty += item.quantity!
                if let qty = item.quantity, qty >= 1 {
                    qtyLabel.text = "Quantity: \(totalQty)"
                    self.qtyLabel.isHidden = false
                } else {
                    self.qtyLabel.isHidden = true
                }
            }
        }
        
        if imageArray.count > 0 {
            for (_, imageUrl) in imageArray.enumerated() {
                cacheImage(urlString: imageUrl) { (image) in
                    if let img = image {
                        self.images.append(img)
                    }
                    if imageArray.count == 1 {
                        self.itemImageView.image = self.images.first
                    } else {
                        self.itemImageView.animationImages = self.images
                        self.itemImageView.animationDuration = 3.0
                        self.itemImageView.startAnimating()
                    }
                }
            }
        }
        if UIDevice().screenType == .iPhones_4_4S || UIDevice().screenType == .iPhones_5_5s_5c_SE {
            itemDescription.font = UIFont(name: appFont, size: 11.0)!
            itemName.font = UIFont(name: appFont, size: 16.0)!
        }
        
        let orderStatus = order.orderStatus ?? ""
        
        if orderStatus == "Complete"
        {
            itemDescription.text = "Delivery on:\(order.expectedDeliveryDate ?? "")"
            itemDescription.textColor = UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1)
        }
        else{
            itemDescription.textColor = .red
            itemDescription.text = order.orderStatus ?? ""
        }
    }
    
    func cacheImage(urlString: String, _ completion: @escaping(_ image: UIImage?) -> Void) {
        guard let url = URL(string: urlString) else { return }
        if let imageFromCache = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            completion(imageFromCache)
        } else {
            URLSession.shared.dataTask(with: url) {
                data, response, error in
                if let dataVal = data {
                    DispatchQueue.main.async {
                        if let imageToCache = UIImage(data: dataVal) {
                            self.imageCache.setObject(imageToCache, forKey: urlString as AnyObject)
                            completion(imageToCache)
                        }
                    }
                }
            }.resume()
        }
    }
}
