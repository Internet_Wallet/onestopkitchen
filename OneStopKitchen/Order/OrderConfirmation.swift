//
//  OrderConfirmation.swift
//  VMart
//
//  Created by Shobhit Singhal on 11/15/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit
import CoreTelephony
import WebKit
import CryptoSwift

class OrderConfirmation: MartBaseViewController {
    
    var aPIManager = APIManager()
    var checkOutOrderInfo : CheckOutOrderInfo? = nil
    var flag = Bool()
    var sectionArr = [String]()
    var successDict = Dictionary<String,Any>()
    var paymentVC: UIViewController?
    var paymenType = ""
    var subPaymentType = ""
    var my2c2pSDK: My2c2pSDK?
    var paymentForm: my2c2pPaymentFormViewController?
    var warningCheckArr = [Bool]()
    var productAvailbilityCheck : [[String : Any]] = [[String : Any]]()
    
    
    @IBOutlet weak var orderConfirmTableView: UITableView!
    @IBOutlet weak var ContinueBtn: UIButton! {
        didSet {
            self.ContinueBtn.setTitle((ContinueBtn.titleLabel?.text ?? "").localized, for: .normal)
            //            self.ContinueBtn.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
            self.ContinueBtn.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1), UIColor.init(red: 20.0/255.0, green: 185.0/255.0, blue: 85.0/255.0, alpha: 1.0)])
            ContinueBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var ApplyBtn: UIButton! {
        didSet {
            ApplyBtn.setTitle((ApplyBtn.titleLabel?.text ?? "").localized, for: .normal)
            ApplyBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(self.paymenType)
        my2c2pSDK = My2c2pSDK(privateKey: APIManagerClient.sharedInstance.ProductionAndStagingPrivateKeyfor2c2p)
        self.navigationItem.title = "Order Confirmation".localized
        sectionArr = [UserDefaults.standard.value(forKey: "shipadd") as? String ?? "", "Products", ""]
        let dummyViewHeight = CGFloat(50)
        self.orderConfirmTableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: self.orderConfirmTableView.bounds.size.width, height: dummyViewHeight))
        self.orderConfirmTableView.contentInset = UIEdgeInsets(top: -dummyViewHeight, left: 0, bottom: 0, right: 0)
        
        //        let footerView = UIView()
        //        footerView.frame = CGRect(x: 20, y: 0, width: orderConfirmTableView.frame.size.width, height: 1)
        //        footerView.backgroundColor = orderConfirmTableView.separatorColor
        //        orderConfirmTableView.tableFooterView = footerView
        
        
        let button = UIButton()
        button.setImage(UIImage(named: "back"), for: .normal)
        button.addTarget(self, action: #selector(dismissScreen), for: .touchUpInside)
        button.imageEdgeInsets.left = -35
        let item = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = item
        
        
        orderConfirmTableView.tableFooterView = UIView()
        
        self.getCheckoutOrderInfo()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    
    
    
    @objc func dismissScreen() {
        //           if let screenFrm = screenFrom {
        //               switch screenFrm {
        //               case "Cart":
        //                   DispatchQueue.main.async {
        self.navigationController?.popViewController(animated: true)
        //                   }
        //               default:
        //                   DispatchQueue.main.async {
        //                       self.navigationController?.popViewController(animated: true)
        //                   }
        //               }
        //           }
    }
    
    func customize2C2PUI(){
        paymentForm = my2c2pPaymentFormViewController.init(nibName: "myPaymentFormViewController", bundle: nil)
        paymentForm?.modalPresentationStyle = .fullScreen
        paymentForm?.delegateVC = self
        //nav bar color
        paymentForm?.navBarColor = UIColor.red
        //nav button color
        paymentForm?.navButtonTintColor = UIColor.white
        //nav title color
        paymentForm?.navTitleColor = UIColor.white
        //nav title
        paymentForm?.navTitle = "MyPayment"
        //nav title image
        paymentForm?.navLogo = UIImage(named: "iconapp")
    }
    
    
    func randomUniqueTransactionCode() -> String? {
        return "\("\(Int(roundf(Float(Date().timeIntervalSince1970))))")\(RAND_FROM_TO(min: 0, max: 9))"
    }
    
    
    
    func getCheckoutOrderInfo() {
        AppUtility.showLoading(self.view)
        self.aPIManager.loadCheckOutOrderInformation(onSuccess: { [weak self] gotcheckOutOrder in
            self?.checkOutOrderInfo = gotcheckOutOrder
            if (self?.checkOutOrderInfo?.Items.count)! > 0{
                self?.warningCheckArr = Array(repeating: false, count: (self?.checkOutOrderInfo?.Items.count)!)
            }
            
            self?.flag = true
            self?.orderConfirmTableView.delegate = self
            self?.orderConfirmTableView.dataSource = self
            self?.orderConfirmTableView.reloadData()
            if let viewLoc = self?.view {
                AppUtility.hideLoading(viewLoc)
            }
            }, onError: { [weak self] message in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    AppUtility.showToast(message,view: viewLoc)
                }
        })
    }
    
    
    func  finalPaymentAction() {
            
            print("wwwwwww -\n",self.warningCheckArr)
            var checkWarning = false
            for obj in warningCheckArr {
                if obj {
                    checkWarning = true
                }
            }
            
            var availableSIM: Bool {
                return (CTTelephonyNetworkInfo().subscriberCellularProvider?.mobileNetworkCode == nil ) ? false : true // sim detection
            }
            
            if  !availableSIM{
                AppUtility.showToastlocal(message: "Insert your registered sim", view: self.view)
                self.navigationController?.popToRootViewController(animated: true)
                return
            }
            
            
            if checkWarning {
                DispatchQueue.main.async {
                    let alertVC = SAlertController()
                    alertVC.ShowSAlert(title: "Information", withDescription: "Buying is disabled for some product", onController: self)
                    let tryAgain = SAlertAction()
                    tryAgain.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                        DispatchQueue.main.async {
                            AppUtility.showLoading(self.view)
                            let dicArray = NSMutableArray ()
                            
                            for (index,obj) in self.warningCheckArr.enumerated() {
                                if obj {
                                    print("iiiiiiiiiiiiii - ",index)
                                    self.warningCheckArr.remove(at: index)
                                    let parameters:Dictionary<String,AnyObject> = ["value": self.checkOutOrderInfo?.Items[index].Id as AnyObject,"key":"removefromcart" as AnyObject]
                                    dicArray.add(parameters)
                                }
                            }
                            print(dicArray)
                            self.removeProduct(dicArray: dicArray)
                        }
                    })
                    alertVC.addAction(action: [tryAgain])
                }
                return
            }
            
            let totalAmount = Double(AppUtility.getTheAmountFromString(amount: self.checkOutOrderInfo?.OrderTotal! ?? "1.0"))//Double(self.checkOutOrderInfo?.OrderTotal! ??  "1.0")
            print(totalAmount)
            if self.paymenType == "OK Dollar"{
                //self.checkOutOrderInfo?.Merchent_Number as! String
                DispatchQueue.main.async {
                    
                   // web apyment scenerio *********************
                self.performSegue(withIdentifier: "OKPaymentSegue", sender: self)
                    
                    
                  // SDK Payment Scenerio  **************************
                    
                    
    //                var model : OKPayModel?
    //                if APIManagerClient.sharedInstance.serverUrl == .productionUrl{
    //                    model = OKPayModel(code: "+95", destination: (self.checkOutOrderInfo?.Merchent_Number as! NSString) as String , amount: totalAmount , key: PaymentKey.MartPayKey, paymentTouple: (url: URLType.production, app: ApplicationType.okEcommerce , pay: PaymentType.okWallet), geoTuple: (lat: "0.0", long: "0.0"))
    //                }else{
    //                    model = OKPayModel(code: "+95", destination: (self.checkOutOrderInfo?.Merchent_Number as! NSString) as String , amount: totalAmount , key: PaymentKey.MartPayKey, paymentTouple: (url: URLType.testing, app: ApplicationType.okEcommerce, pay: PaymentType.okWallet), geoTuple: (lat: "0.0", long: "0.0"))
    //                }
    //                print(model)
    //
    //                if let val = model{
    //                    print(OKPayManager.pay.start(val, withDelegate: self))
    //                    if let vc = OKPayManager.pay.start(val, withDelegate: self) {
    //                        vc.modalPresentationStyle = .fullScreen
    //                        self.paymentVC = vc
    //                        self.present(vc, animated: true, completion: nil)
    //                    }
    //                }
                    
                    
                    
                    
                    
                }
            }
            else if (self.paymenType == "Cash On Delivery (COD)" || self.paymenType == "COD"){
                
                DispatchQueue.main.async {
                    print("Payment Success")
                    let transId = AppUtility.gettransID()
                    self.successDict = ["transactionStatusCode":"001","PaymentStatus":"Pending","transid":transId,"resultdescription":"transaction successful","amount":totalAmount]
                        if let paymentProcessingVC = UIStoryboard(name: "Cart", bundle: nil).instantiateViewController(withIdentifier: "PaymentProcessingViewController_ID") as? PaymentProcessingViewController {
                            paymentProcessingVC.navController = self.navigationController
                            paymentProcessingVC.paymenType = self.paymenType
                            paymentProcessingVC.paymentDict = self.successDict
                            paymentProcessingVC.totalAmount = "\(totalAmount)"
                            paymentProcessingVC.definesPresentationContext = true
                            paymentProcessingVC.modalPresentationStyle = .overCurrentContext
                            self.present(paymentProcessingVC, animated: true, completion: nil)
                        }
                }
            }
            else{
                // 2c2p code
                
                //            my2c2pSDK?.merchantID = "104104000000270"
                //            my2c2pSDK?.secretKey = "4C18A036BD22DC37F7412BB648E6D88C554EFBB8C4D9194FE3C19FAF64BAA519"//Production Key"524EE9FC87F9832814708365A49703407879BAFD3490BF099FFAD0D395BA55B4"
                
                my2c2pSDK?.merchantID = APIManagerClient.sharedInstance.merchantID
                my2c2pSDK?.currencyCode = APIManagerClient.sharedInstance.currencyCode
                my2c2pSDK?.secretKey = APIManagerClient.sharedInstance.secretKey
                my2c2pSDK?.productionMode = APIManagerClient.sharedInstance.productionMode
                my2c2pSDK?.uniqueTransactionCode = randomUniqueTransactionCode()
                
                my2c2pSDK?.desc = "Cart Product"
                my2c2pSDK?.paymentUI = true
                my2c2pSDK?.amount = totalAmount
                my2c2pSDK?.paymentOption = PaymentOption.creditCard.rawValue as NSString
                //my2c2pSDK?.promotion = self.subPaymentType
                my2c2pSDK?.paymentOptionEnumList()
                
                //            self.children[0].modalPresentationStyle = .fullScreen
                //            self.parent?.modalPresentationStyle = .fullScreen
                
                my2c2pSDK?.request(withTarget: self, onResponse: { response in
                    if (response?["respCode"] as? String == "00") {
                        print("Payment Success")
                        self.successDict = response as! [String : Any]
                        DispatchQueue.main.async{
                            if let paymentProcessingVC = UIStoryboard(name: "Cart", bundle: nil).instantiateViewController(withIdentifier: "PaymentProcessingViewController_ID") as? PaymentProcessingViewController {
                                paymentProcessingVC.modalPresentationStyle = .fullScreen
                                paymentProcessingVC.navController = self.navigationController
                                paymentProcessingVC.paymenType = self.paymenType
                                paymentProcessingVC.paymentDict = self.successDict
                                paymentProcessingVC.totalAmount = "\(totalAmount)"
                                paymentProcessingVC.definesPresentationContext = true
                                paymentProcessingVC.modalPresentationStyle = .overCurrentContext
                                self.present(paymentProcessingVC, animated: true, completion: nil)
                            }
                        }
                    } else {
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                            AppUtility.showToastlocal(message: response?["failReason"] as! String , view: self.view)
                        }
                        print("Fail reason: \(response?["failReason"] ?? "")")
                    }
                }, onFail: { error in
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                        AppUtility.showToastlocal(message: error?.localizedDescription ?? "" , view: self.view)
                    }
                    print("Error: \(error?.localizedDescription ?? "")")
                })
            }
            
        }
    
    
    /* */
    
    
    func createDictForCheckProduct() -> NSMutableArray {
        
        let finalDicArray = NSMutableArray ()
        for index  in 0..<(self.checkOutOrderInfo?.Items.count)! {
            let item = (self.checkOutOrderInfo?.Items[index])! as CheckOutItems
            var parameters = Dictionary<String, AnyObject>()
            if let cgmID = item.cGMItemID , !cgmID.isEmpty , cgmID != "NULL" {
                    let ticalTitle = item.Sku
                if (ticalTitle == "TIC") || ((ticalTitle?.contains("TIC")) != nil) {
                    if let attributes = item.AttributeInfo.components(separatedBy: ":") as? [String] , attributes.count > 1 {
                        if let ticalQuantity = attributes[1] as? String {
                            let ticalValue = ticalQuantity.replacingOccurrences(of: " ", with: "")
                            let totalTical = item.Quantity * (NSInteger(ticalValue) ?? 1)
                             parameters["qty"] = totalTical as AnyObject
                         }
                       else {
                              parameters["qty"] = "1" as AnyObject
                          }
                       }
                    else {
                         parameters["qty"] = item.Quantity as AnyObject
                       }
                     }
                    else {
                         parameters["qty"] = item.Quantity as AnyObject
                    }
                  parameters["productName"] = (item.ProductName ) as AnyObject
                  parameters["isAvailabe"] = false as AnyObject
                  parameters["availableQty"] = 0 as AnyObject
                  parameters["productBarcode"] = "\(item.ProductId)" as AnyObject
                  parameters["erpProductId"] = (item.cGMItemID ?? "") as AnyObject
                  finalDicArray.add(parameters)
                }
            }
        print("finalDicArray from add to cart : \(finalDicArray)")
        return finalDicArray
    }
    
    
    func checkForTheItems( itemsA : ECOMProduct) -> Bool {
        var isProductAvailable = false
        let availbleQuantity = itemsA.availableQty
        let productID = NSInteger.init(itemsA.productBarcode ?? "1")
        for index in 0..<(self.checkOutOrderInfo?.Items.count)! {
            if let product = self.checkOutOrderInfo?.Items[index] {
                if product.ProductId == productID{
                    if let productQuantity = itemsA.qty as? Int {
                            if productQuantity <= availbleQuantity {
                                isProductAvailable = true
                            }
                            else {
                                isProductAvailable = false
                            }
                       }
                  }
            }

        }
        return isProductAvailable
    }
    
    func checkForQuantityAvailvility() {
                    let finalDicArray = self.createDictForCheckProduct()
                    if finalDicArray.count == 0 {
                        self.finalPaymentAction()
                    }
                    else {
                    
                    AppUtility.showLoading(self.view)
                    self.aPIManager.checkProductsOnERPServer(parameters: finalDicArray, onSuccess: { (products) in
                        if let viewLoc = self.view {
                            AppUtility.hideLoading(viewLoc)
                        }
                        var isAllOk : Bool = true
                        for (_ , itemValue) in products.enumerated() {
                            if let productInfo = itemValue as? ECOMProduct {
                                let isProductAvailable = self.checkForTheItems ( itemsA : productInfo)
                                if isProductAvailable {
                                    let productInfo = ["isProductAvailable":true ,"productID": (itemValue.productBarcode) ?? 1 , "AvailableQuantity":productInfo.availableQty] as [String : Any]
                                    self.productAvailbilityCheck.append(productInfo)
                                }
                                else {
                                    let productInfo = ["isProductAvailable":false ,"productID":(itemValue.productBarcode) ?? 1 ,"AvailableQuantity":productInfo.availableQty] as [String : Any]
                                    self.productAvailbilityCheck.append(productInfo)
                                }
                            }
                        }
                        for (_ , itemsData) in self.productAvailbilityCheck.enumerated() {
                            if let isNotAvailable  = itemsData["isProductAvailable"] as? Bool , isNotAvailable  == false {
                                isAllOk = false
                            }
                        }
                        if isAllOk {
                            self.finalPaymentAction()
                        }
                        else {
                            self.sAlertController.ShowSAlert(title:  "Information".localized, withDescription: "One or more product Out Of Stock!".localized , onController: self)
                                   let okAlertAction = SAlertAction()
                                   okAlertAction.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                                     DispatchQueue.main.async {
                                      self.orderConfirmTableView.reloadData()
                                    }
                                   })
                                  self.sAlertController.addAction(action: [okAlertAction])
                           }
                    }) { (message) in
                        if let viewLoc = self.view {
                            AppUtility.hideLoading(viewLoc)
                            let msg = message?.components(separatedBy: "\n")
                            let errorMsg = msg?[0]
                            self.showAlert(alertTitle: "Information".localized, description: errorMsg ?? "")
                        }
                    }
                }
          }
    
    
    //MARK:- Payment Api Call
    @IBAction func confirmButtonAction(_ sender: UIButton) {
        
        self.productAvailbilityCheck.removeAll()
        //self.checkForQuantityAvailvility()
//        self.ContinueBtn.setBackgroundColor(color: UIColor.gray, forState: .normal)
//        self.ContinueBtn.isUserInteractionEnabled = false
        self.finalPaymentAction()
    }
    
    func RAND_FROM_TO(min: UInt32, max: UInt32) -> Int {
        return Int(min + arc4random_uniform(max - min + 1))
    }
    
    
    func enumerate(indexer: XMLIndexer) {
        for child in indexer.children {
            self.successDict[child.element!.name] = child.element!.text
            enumerate(indexer: child)
        }
    }
    
    func removeProduct(dicArray: NSMutableArray) {
        aPIManager.DeleteAndUpdate(ticalValue: "tusharLama", dicArray, onSuccess: { [weak self] updateCart in
            //self?.checkOutOrderInfo = updateCart
            self?.navigationController?.popToRootViewController(animated: true)
            if let viewLoc = self?.view {
                AppUtility.hideLoading(viewLoc)
                
            }
            
            }, onError: { [weak self] message in
                if let viewLoc = self?.view {
                    AppUtility.hideLoading(viewLoc)
                    AppUtility.showToast(message,view: viewLoc)
                }
        })
    }
    
//      web apyment scenerio *********************
    private func showPaymentProcessingVC(responseDict:[String:String]) {


//        if let payVC = paymentVC {
//            payVC.dismiss(animated: false) {
                if let paymentProcessingVC = UIStoryboard(name: "Cart", bundle: nil).instantiateViewController(withIdentifier: "PaymentProcessingViewController_ID") as? PaymentProcessingViewController {
                    paymentProcessingVC.navController = self.navigationController
                    paymentProcessingVC.paymenType = self.paymenType
                    paymentProcessingVC.paymentDict = responseDict
                    paymentProcessingVC.modalPresentationStyle = .fullScreen
                    self.present(paymentProcessingVC, animated: true, completion: nil)
                }
//            }
//        }
    }
    
//    private func showPaymentProcessingVC() {
//
//
//        if let payVC = paymentVC {
//            payVC.dismiss(animated: false) {
//                if let paymentProcessingVC = UIStoryboard(name: "Cart", bundle: nil).instantiateViewController(withIdentifier: "PaymentProcessingViewController_ID") as? PaymentProcessingViewController {
//                    paymentProcessingVC.navController = self.navigationController
//                    paymentProcessingVC.paymenType = self.paymenType
//                    paymentProcessingVC.paymentDict = self.successDict
//                    paymentProcessingVC.modalPresentationStyle = .fullScreen
//                    self.present(paymentProcessingVC, animated: true, completion: nil)
//                }
//            }
//        }
//    }
    
}

extension OrderConfirmation: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionArr.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            return self.checkOutOrderInfo?.Items.count ?? 0
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 2 {
            return 20
        }
        return 40
    }
    
    //    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    //        return sectionArr[section]
    //    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 2 {
            let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 2))
            headerView.backgroundColor = UIColor.colorWithRedValue(redValue: 240, greenValue: 240, blueValue: 240, alpha: 1)
            //            headerView.applyViewnGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
            headerView.applyViewnGradient(colors: [UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1), UIColor.init(red: 20.0/255.0, green: 185.0/255.0, blue: 85.0/255.0, alpha: 1.0)])
            return headerView
        }
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 40))
        let backView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.width, height: 40))
        //        backView.backgroundColor = UIColor.colorWithRedValue(redValue: 240, greenValue: 240, blueValue: 240, alpha: 1)
        //        backView.applyViewnGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
        backView.applyViewnGradient(colors: [UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1), UIColor.init(red: 20.0/255.0, green: 185.0/255.0, blue: 85.0/255.0, alpha: 1.0)])
        let label = UILabel(frame: CGRect(x: 10, y: 10, width: tableView.bounds.width-15, height: 20))
        label.font = UIFont(name: appFont, size: 15.0)
        label.textColor = UIColor.white
        //        label.textColor = UIColor.white
        label.text = sectionArr[section].localized
        headerView.addSubview(backView)
        headerView.addSubview(label)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell : OrderConfirmationTableViewCell?
        switch indexPath.section {
        case 0:
            cell = tableView.dequeueReusableCell(withIdentifier: "DeliveryAddressConfirmCell", for: indexPath) as? OrderConfirmationTableViewCell
            cell?.wrapAddressCell(orerInfo: self.checkOutOrderInfo)
        case 1:
            cell = tableView.dequeueReusableCell(withIdentifier: "ProductOrdersConfirmCell", for: indexPath) as? OrderConfirmationTableViewCell
            if (self.checkOutOrderInfo?.Items[indexPath.row].Warnings?.count)! > 0{
                self.warningCheckArr[indexPath.row] = true
            } else {
                self.warningCheckArr[indexPath.row] = false
            }
            
            cell?.wrapProductDetails(orderInfo: self.checkOutOrderInfo, row: indexPath.row, availabilityArray: self.productAvailbilityCheck)
        default:
            cell = tableView.dequeueReusableCell(withIdentifier: "TotalAmountOrderConfirmCell", for: indexPath) as? OrderConfirmationTableViewCell
            cell?.wrapAmountCell(orderInfo: self.checkOutOrderInfo)
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return UITableView.automaticDimension
        }
        if indexPath.section == 1 {
            if let attributeInfo = self.checkOutOrderInfo?.Items[indexPath.row].AttributeInfo as String? , attributeInfo.contains(find: "<br />") {
                return SCREEN_WIDTH/3.2 + 50
            }
            else {
                // return SCREEN_WIDTH/3.2 + 10
                return 161
            }
        }
        else if indexPath.section == 2 {
            return 210
        }
        return UITableView.automaticDimension
    }
}

class OrderConfirmationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lblTaxLabelConstraint: NSLayoutConstraint!{
        didSet {
            lblTaxLabelConstraint.constant = 22.0
        }
    }
    @IBOutlet weak var lblTaxValueConstraint: NSLayoutConstraint!{
        didSet {
            lblTaxValueConstraint.constant = 22.0
        }
    }
    @IBOutlet weak var lblDiscountLabelConstraint: NSLayoutConstraint!{
        didSet {
            lblDiscountLabelConstraint.constant = 22.0
        }
    }
    @IBOutlet weak var lblDiscountValueConstraint: NSLayoutConstraint!{
        didSet {
            lblDiscountValueConstraint.constant = 22.0
        }
    }
    @IBOutlet var productAmtHeaderLbl: UILabel! {
        didSet {
            self.productAmtHeaderLbl.font = UIFont(name: appFont, size: 15.0)
            productAmtHeaderLbl?.text = productAmtHeaderLbl?.text?.localized
            
        }
    }
    
    @IBOutlet var userNameLbl: UILabel! {
        didSet {
            self.userNameLbl.font = UIFont(name: appFont, size: 15.0)
            userNameLbl?.text = userNameLbl?.text?.localized
            
        }
    }
    @IBOutlet var emailLbl: UILabel! {
        didSet {
            self.emailLbl.font = UIFont(name: appFont, size: 15.0)
            emailLbl?.text = emailLbl?.text?.localized
            
        }
    }
    @IBOutlet var phNumberLbl: UILabel! {
        didSet {
            self.phNumberLbl.font = UIFont(name: appFont, size: 15.0)
            phNumberLbl?.text = phNumberLbl?.text?.localized
            
        }
    }
    @IBOutlet var addressAndCityLbl: UILabel! {
        didSet {
            self.addressAndCityLbl.font = UIFont(name: appFont, size: 15.0)
            addressAndCityLbl?.text = addressAndCityLbl?.text?.localized
            
        }
    }
    
    @IBOutlet var additionalChargeLable: UILabel! {
            didSet {
                self.additionalChargeLable.font = UIFont(name: appFont, size: 15.0)
                additionalChargeLable?.text = additionalChargeLable?.text?.localized
                
            }
        }
        
        @IBOutlet var additionalChargeValuelbl: UILabel! {
            didSet {
                self.additionalChargeValuelbl.font = UIFont(name: appFont, size: 15.0)
                additionalChargeValuelbl?.text = additionalChargeValuelbl?.text?.localized
            }
        }

    
    
    @IBOutlet var countryLbl: UILabel! {
        didSet {
            self.countryLbl.font = UIFont(name: appFont, size: 15.0)
            countryLbl?.text = countryLbl?.text?.localized
            
        }
    }
    @IBOutlet var productImgView: UIImageView!
    @IBOutlet var productNameLbl: UILabel! {
        didSet {
            self.productNameLbl.font = UIFont(name: appFont, size: 15.0)
            productNameLbl?.text = productNameLbl?.text?.localized
            productNameLbl?.numberOfLines = 2
            
        }
    }
    @IBOutlet var priceLabel: UILabel! {
        didSet {
            self.priceLabel.font = UIFont(name: appFont, size: 15.0)
            priceLabel?.text = "\("Price".localized):"
            
        }
    }
    @IBOutlet var quantityLabel: UILabel! {
        didSet {
            self.quantityLabel.font = UIFont(name: appFont, size: 15.0)
            quantityLabel?.text = "\("Quantity".localized):"
            
        }
    }
    
    @IBOutlet var deliverytimeLabel: UILabel! {
        didSet{
            self.deliverytimeLabel.font = UIFont(name: appFont, size: 15.0)
            deliverytimeLabel?.text = "\("Delivery Time".localized):"
            
        }
    }
    
    @IBOutlet var totalLabel: UILabel! {
        didSet {
            self.totalLabel.font = UIFont(name: appFont, size: 15.0)
            totalLabel?.text = "\("Total".localized):"
            
        }
    }
    
    @IBOutlet var outofstockLabel: UILabel!{
        didSet {
            self.outofstockLabel.font = UIFont(name: appFont, size: 15.0)
            
        }
    }
    
    
    @IBOutlet var attributeLabel: UILabel!{
        didSet {
            self.attributeLabel.font = UIFont(name: appFont, size: 15.0)
            attributeLabel?.text = attributeLabel?.text?.localized
        }
    }
    
    @IBOutlet var priceValueLbl: UILabel! {
        didSet {
            self.priceValueLbl.font = UIFont(name: appFont, size: 15.0)
            priceValueLbl?.text = priceValueLbl?.text?.localized
        }
    }
    @IBOutlet var quantityValueLbl: UILabel! {
        didSet {
            self.quantityValueLbl.font = UIFont(name: appFont, size: 15.0)
            quantityValueLbl?.text = quantityValueLbl?.text?.localized
            
        }
    }
    @IBOutlet var totalValueLbl: UILabel! {
        didSet {
            self.totalValueLbl.font = UIFont(name: appFont, size: 15.0)
            totalValueLbl?.text = totalValueLbl?.text?.localized
            
        }
    }
    @IBOutlet var lineLbl: UILabel! {
        didSet {
            self.lineLbl.font = UIFont(name: appFont, size: 15.0)
            lineLbl?.text = lineLbl?.text?.localized
            
        }
    }
    
    @IBOutlet var subTotallLabel: UILabel! {
        didSet {
            self.subTotallLabel.font = UIFont(name: appFont, size: 15.0)
            subTotallLabel?.text = subTotallLabel?.text?.localized
            
        }
    }
    @IBOutlet var shippingLabel: UILabel! {
        didSet {
            self.shippingLabel.font = UIFont(name: appFont, size: 15.0)
            shippingLabel?.text = shippingLabel?.text?.localized
            
        }
    }
    @IBOutlet var taxLabel: UILabel! {
        didSet {
            self.taxLabel.font = UIFont(name: appFont, size: 15.0)
            taxLabel?.text = taxLabel?.text?.localized
            
        }
    }
    @IBOutlet var discountLabel: UILabel! {
        didSet {
            self.discountLabel.font = UIFont(name: appFont, size: 15.0)
            discountLabel?.text = discountLabel?.text?.localized
            
        }
    }
    @IBOutlet var grandTotalLabel: UILabel! {
        didSet {
             self.grandTotalLabel.font = UIFont(name: appFont, size: 15.0)
            grandTotalLabel?.text = grandTotalLabel?.text?.localized
           
        }
    }
    @IBOutlet var subTotallValueLbl: UILabel! {
        didSet {
            self.subTotallValueLbl.font = UIFont(name: appFont, size: 15.0)
            subTotallValueLbl?.text = subTotallValueLbl?.text?.localized
            
        }
    }
    @IBOutlet var shippingValueLbl: UILabel! {
        didSet {
             self.shippingValueLbl.font = UIFont(name: appFont, size: 15.0)
            shippingValueLbl?.text = shippingValueLbl?.text?.localized
           
        }
    }
    @IBOutlet var taxValueLbl: UILabel! {
        didSet {
            self.taxValueLbl.font = UIFont(name: appFont, size: 15.0)
            taxValueLbl?.text = taxValueLbl?.text?.localized
            
        }
    }
    @IBOutlet var discountValueLbl: UILabel! {
        didSet {
            self.discountValueLbl.font = UIFont(name: appFont, size: 15.0)
            discountValueLbl?.text = discountValueLbl?.text?.localized
            
        }
    }
    @IBOutlet var grandTotalValueLbl: UILabel! {
        didSet {
            self.grandTotalValueLbl.font = UIFont(name: appFont, size: 15.0)
            grandTotalValueLbl?.text = grandTotalValueLbl?.text?.localized
            
        }
    }
    
    func wrapAddressCell(orerInfo: CheckOutOrderInfo?) {
        
        let userName = "\(orerInfo?.ShippingAddress_FirstName ?? "")"
        //  let address2 = orerInfo?.ShippingAddress_Address2 ?? ""
        
        if let address = UserDefaults.standard.value(forKey: "shipadd") as? String{
    
            if address == "Delivery Address" {
                
                var houseNo = orerInfo?.BillingAddress_HouseNo?.replacingOccurrences(of: "House No:", with: "").replacingOccurrences(of: "House No ", with: "") ?? ""
                var floorNo = orerInfo?.BillingAddress_FloorNo?.replacingOccurrences(of: "Floor No:", with: "").replacingOccurrences(of: "Floor No ", with: "") ?? ""
                var roomNo = orerInfo?.BillingAddress_RoomNo?.replacingOccurrences(of: "Room No:", with: "").replacingOccurrences(of: "Room No ", with: "") ?? ""
                
                var address1 = orerInfo?.BillingAddress_Address1 ?? ""
                var address2 = orerInfo?.BillingAddress_Address2 ?? ""
                var city = orerInfo?.BillingAddress_City ?? ""
                var state = orerInfo?.BillingAddress_StateProvinceName ?? ""
                var country = orerInfo?.ShippingAddress_CountryName as String? ?? ""
            
                      if (houseNo != "" && houseNo != nil) || (roomNo != "" && roomNo != nil) || (floorNo != "" && floorNo != nil) {
                            var initialString = ""
                                    if houseNo != "" {
                                        houseNo = ""
                                        houseNo.append("House No ".localized + ": \(orerInfo?.BillingAddress_HouseNo?.replacingOccurrences(of: "House No ", with: "").replacingOccurrences(of: "အိမ္ယာနံပါတ္", with: "").replacingOccurrences(of: "အိမ်အမှတ်", with: "") ?? "")")
                                        houseNo.append(", ")
                                        initialString.append(houseNo)
                                    }
                                    if roomNo != "" {
                                        roomNo = ""
                                        roomNo.append("Room No ".localized + ": \(orerInfo?.BillingAddress_RoomNo?.replacingOccurrences(of: "Room No ", with: "").replacingOccurrences(of: "အခန္းနံပါတ္", with: "").replacingOccurrences(of: "အခန်းအမှတ်", with: "") ?? "")")
                                        roomNo.append(", ")
                                        initialString.append(roomNo)
                                    }
                                    if floorNo != "" {
                                        floorNo = ""
                                        floorNo.append("Floor No ".localized + ": \(orerInfo?.BillingAddress_FloorNo?.replacingOccurrences(of: "Floor No ", with: "").replacingOccurrences(of: "အထပ္နံပါတ္", with: "").replacingOccurrences(of: "အထပ်နံပါတ်", with: "") ?? "")")
                                        floorNo.append(", ")
                                        initialString.append(floorNo)
                                    }
                                    if address1 != "" {
                                        address1.appending(", ")
                                    }
                                    if address2 != "" {
                                        address2.appending(", ")
                                    }
                                    if city != "" {
                                        city.appending("\(String(describing: orerInfo?.BillingAddress_City))")
                                        city.appending(", ")
                                    }
                                    if state != "" {
                                        state.appending("\(String(describing: orerInfo?.BillingAddress_StateProvinceName))")
                                    }
                                    if country != "" {
                                        country.appending("\(String(describing: orerInfo?.ShippingAddress_CountryName))")
                                    }
                                    self.addressAndCityLbl.text = "\(initialString)\n\(address1) , \(address2)\n\(city) , \(state)\n\(country)"
                                }
                                else{
                                    self.addressAndCityLbl.text = "\(address1) , \(address2)\n\(city), \(state)\n\(country)"
                                }
                           }
            }
        else {
                var houseNo = (orerInfo?.ShippingAddress_HouseNo?.replacingOccurrences(of: "House No:", with: "") ?? "").replacingOccurrences(of: "House No ", with: "")
                var floorNo = (orerInfo?.ShippingAddress_FloorNo?.replacingOccurrences(of: "Floor No:", with: "") ?? "").replacingOccurrences(of: "Floor No ", with: "")
                var roomNo = (orerInfo?.ShippingAddress_RoomNo?.replacingOccurrences(of: "Room No:", with: "") ?? "").replacingOccurrences(of: "Room No ", with: "")
                
                var address1 = orerInfo?.ShippingAddress_Address1 ?? ""
                var address2 = orerInfo?.ShippingAddress_Address2 ?? ""
                var city = orerInfo?.ShippingAddress_City ?? ""
                var state = orerInfo?.ShippingAddress_StateProvinceName ?? ""
                var country = orerInfo?.ShippingAddress_CountryName as String? ?? ""
            
                              if (houseNo != "" && houseNo != nil) || (roomNo != "" && roomNo != nil) || (floorNo != "" && floorNo != nil) {
                                var initialString = ""
                                if houseNo != "" {
                                    houseNo = ""
                                    houseNo.append("House No ".localized + ": \(orerInfo?.ShippingAddress_HouseNo?.replacingOccurrences(of: "House No ", with: "").replacingOccurrences(of: "အိမ္ယာနံပါတ္", with: "").replacingOccurrences(of: "အိမ်အမှတ်", with: "") ?? "")")
                                    houseNo.append(", ")
                                    initialString.append(houseNo)
                                 }
                                if roomNo != ""
                                       {
                                            roomNo = ""
                                            roomNo.append("Room No ".localized + ": \(orerInfo?.ShippingAddress_RoomNo?.replacingOccurrences(of: "Room No ", with: "").replacingOccurrences(of: "အခန္းနံပါတ္", with: "").replacingOccurrences(of: "အခန်းအမှတ်", with: "") ?? "")")
                                            roomNo.append(", ")
                                            initialString.append(roomNo)
                                        }
                                    if floorNo != "" {
                                        floorNo = ""
                                        floorNo.append("Floor No ".localized + ": \(orerInfo?.ShippingAddress_FloorNo?.replacingOccurrences(of: "Floor No ", with: "").replacingOccurrences(of: "အထပ္နံပါတ္", with: "").replacingOccurrences(of: "အထပ်နံပါတ်", with: "") ?? "")")
                                        floorNo.append(", ")
                                        initialString.append(floorNo)
                                    }
                                   if address1 != "" {
                                       address1 = address1.appending(", ") as NSString
                                   }
                                   if address2 != "" {
                                       address2 = address2.appending(", ") as NSString
                                   }
                                   if city != "" {
                                       city.appending("\(String(describing: orerInfo?.ShippingAddress_City))")
                                       city.appending(", ")
                                   }
                                   if state != "" {
                                       state.appending("\(String(describing: orerInfo?.ShippingAddress_StateProvinceName))")
                                   }
                                   if country != "" {
                                       country.appending("\(String(describing: orerInfo?.ShippingAddress_CountryName))")
                                   }
                                
                                    self.addressAndCityLbl.text = "\(initialString)\n\(address1),\(address2)\n\(city) , \(state)\n\(country)"
                                }
                                else{
                                    self.addressAndCityLbl.text = "\(address1),\(address2)\n\(city) , \(state)\n\(country)"
                                }
            
                    }
        self.userNameLbl.text = userName
        self.emailLbl.text = orerInfo?.ShippingAddress_Email as String? ?? ""
        phNumberLbl.text = orerInfo?.ShippingAddress_PhoneNumber as String? ?? ""
        //  let newString = string.replacingOccurrences(of: "0095", with: "+95", options: .regularExpression, range: nil)
        
        if let number = phNumberLbl.text {
            if number.hasPrefix("00950") {
                //lblPhoneNumber.text = (number as NSString).replacingCharacters(in: NSRange(location: 0, length: 5), with: "+95")
                self.phNumberLbl.text = number.replacingOccurrences(of: "00950", with: "+95")
            }
            else {
                //lblPhoneNumber.text = (number as NSString).replacingCharacters(in: NSRange(location: 0, length: 4), with: "+95")
                self.phNumberLbl.text = number.replacingOccurrences(of: "0095", with: "+95")
            }
        }
        
        
        // self.phNumberLbl.text = newString
        //  self.addressAndCityLbl.text = address
        
    }
    
    func wrapProductDetails(orderInfo: CheckOutOrderInfo?, row: Int , availabilityArray : [[String : Any]]) {
        self.productImgView.sd_setImage(with: URL(string: orderInfo?.Items[row].ImageUrl ?? ""))
        
        
        
        //        AppUtility.getData(from: URL(string: orderInfo?.Items[row].ImageUrl ?? "")!) { data, response, error in
        //            guard let data = data, error == nil else { return }
        //            print("Download Finished")
        //            DispatchQueue.main.async() {
        //                self.productImgView.image = UIImage(data: data)
        //            }
        //        }
        
        
        
        //        AppUtility.NKPlaceholderImage(image: UIImage(named: orderInfo?.Items[row].ImageUrl ?? ""), imageView: self.productImgView, imgUrl: orderInfo?.Items[row].ImageUrl ?? "") { (image) in }
        
        if availabilityArray.count > 0 {
             let currentProductID = orderInfo?.Items[row].ProductId
             var productInfoCheck : Dictionary<String,Any> = Dictionary<String,Any>()
             for (_, item) in availabilityArray.enumerated() {
                 if let productInfoData = item as? [String : Any] {
                     if let productID = productInfoData["productID"] as? String {
                         if currentProductID == NSInteger(productID) {
                             productInfoCheck.updateValue(productInfoData["isProductAvailable"] ?? false, forKey: "isProductAvailable")
                             productInfoCheck.updateValue(productInfoData["productID"] ?? 1, forKey: "productID")
                             productInfoCheck.updateValue(productInfoData["AvailableQuantity"] ?? 0 , forKey: "AvailableQuantity")
                         }
                     }
                 }
             }
             
             if let availableP = productInfoCheck["isProductAvailable"] as? Bool , !availableP {
                 let Quantity = (productInfoCheck["AvailableQuantity"] as? Int) ?? 0
                 if productInfoCheck.count > 0 && Quantity > 0 {
                    if orderInfo?.Items[row].Sku == "TIC"  || ((orderInfo?.Items[row].Sku?.contains("TIC")) != nil){
                        if let attributes = orderInfo?.Items[row].AttributeInfo.components(separatedBy: ":") as? [String] , attributes.count > 1 {
                            if let ticalQuantity = attributes[1] as? String {
                                        let ticalValue = ticalQuantity.replacingOccurrences(of: " ", with: "")
                                        let tical = NSInteger(ticalValue) ?? 1
                                        self.outofstockLabel.isHidden = false
                                        self.outofstockLabel.text = "Max Available Quantity : \(Quantity/tical)"
                                     }
                                 }
                             else {
                                    self.outofstockLabel.isHidden = false
                                    self.outofstockLabel.text = "Max Available Quantity : \(Quantity)"
                                }
                              }
                         else {
                               self.outofstockLabel.isHidden = false
                               self.outofstockLabel.text = "Max Available Quantity : \(Quantity)"
                         }
                   }
                 else {
                     self.outofstockLabel.isHidden = false
                     self.outofstockLabel.text = "Out of stock"
                 }
             }
             else {
                 self.outofstockLabel.isHidden = true
             }
         }
         else {
             self.outofstockLabel.isHidden = true
         }
        
        self.productNameLbl.text = orderInfo?.Items[row].ProductName as String? ?? ""
        self.priceValueLbl.text = orderInfo?.Items[row].UnitPrice as String? ?? ""
        self.priceValueLbl.amountAttributedString()
        self.quantityValueLbl.text = String(format: "%d", orderInfo?.Items[row].Quantity ?? 0)
         self.attributeLabel.text = (orderInfo?.Items[row].AttributeInfo)!.replacingOccurrences(of: "<br />", with: "\n")
        self.deliverytimeLabel.text = String.init(format: "%@: %@", "Delivery Time".localized, orderInfo?.Items[row].DeliveryDays as String? ?? "")
        //        self.totalValueLbl.text = orderInfo?.Items[row].SubTotal as String? ?? ""
        if row == (orderInfo?.Items.count ?? 0) - 1 {
            self.lineLbl.isHidden = true
        } else {
            self.lineLbl.isHidden = false
        }
    }
    
    func wrapAmountCell(orderInfo: CheckOutOrderInfo?) {
        
        if orderInfo?.SubTotal == nil || orderInfo?.SubTotal == "" {
            self.subTotallLabel.text = ""
        }
        if orderInfo?.Shipping == nil || orderInfo?.Shipping == "" {
            self.shippingLabel.text = ""
        }
        if orderInfo?.Tax == nil || orderInfo?.Tax == "" || orderInfo?.Tax == "0 MMK" {
            self.taxLabel.text = ""
            self.taxLabel.isHidden = true
            self.lblTaxLabelConstraint.constant = 0
            self.lblTaxValueConstraint.constant = 0
        }
        if orderInfo?.SubTotalDiscount == nil || orderInfo?.SubTotalDiscount == "" {
            if orderInfo?.OrderTotalDiscount == nil || orderInfo?.OrderTotalDiscount == "" {
                self.discountLabel.text = ""
                self.discountLabel.isHidden = true
                self.lblDiscountLabelConstraint.constant = 0
                self.lblDiscountValueConstraint.constant = 0
            }
        }
        if orderInfo?.OrderTotal == nil || orderInfo?.OrderTotal == "" {
            self.grandTotalLabel.text = ""
        }
        if orderInfo?.additionalCharge == nil || orderInfo?.additionalCharge == "" {
            self.additionalChargeLable.text = ""
        }
        
        self.subTotallValueLbl.text = orderInfo?.SubTotal ?? ""
        self.subTotallValueLbl.amountAttributedString()
        if orderInfo?.Shipping == "0 MMK"{
            self.shippingValueLbl.text = "Free".localized
        }
        else{
            if let amountVal = orderInfo?.Shipping , Int(amountVal) ?? -1 > 0 {
                self.shippingValueLbl.text = orderInfo?.Shipping ?? ""
                self.shippingValueLbl.amountAttributedString()
            }
            else {
                
                self.shippingValueLbl.text = ""
            }
            
        }
        
        if let discountDetails = orderInfo?.SubTotalDiscount?.replacingOccurrences(of: " MMK", with: "").replacingOccurrences(of: "-", with: "") {
            self.discountValueLbl.text = discountDetails
            self.discountValueLbl.amountAttributedString()
        }else{
            self.discountValueLbl.isHidden = true
        }
        
        
        if let taxDetails = orderInfo?.Tax , taxDetails != nil{
            if taxDetails == "0 MMK"{
                self.taxValueLbl.isHidden = true
            }
            else{
                self.taxValueLbl.text = orderInfo?.Tax ?? ""
                self.taxValueLbl.amountAttributedString()
            }
        }
        else {
            self.taxValueLbl.isHidden = true
        }
        
        
        //        self.discountValueLbl.text = orderInfo?.SubTotalDiscount ?? ""
        
        if orderInfo?.OrderTotalDiscount != nil {
            self.discountValueLbl.isHidden = false
            self.discountValueLbl.text = orderInfo?.OrderTotalDiscount ?? ""
        }
        if orderInfo?.additionalCharge != nil {
            self.additionalChargeLable.isHidden = false
            self.additionalChargeValuelbl.text = (orderInfo?.additionalCharge ?? "" )
        }
        else {
            self.additionalChargeLable.isHidden = true
            self.additionalChargeValuelbl.text = ""
        }
        
        //        self.grandTotalValueLbl.text = orderInfo?.OrderTotal ?? ""
        //        self.grandTotalValueLbl.amountAttributedString()
        //
        
        
        
        var productPrice = orderInfo?.OrderTotal!.replacingOccurrences(of: "MMK", with: "")
        productPrice = productPrice! + mmkText
        
        let attrString = NSMutableAttributedString(string: productPrice!)
        let nsRange = NSString(string: productPrice!).range(of: mmkText, options: String.CompareOptions.caseInsensitive)
        attrString.addAttribute(kCTFontAttributeName as NSAttributedString.Key, value: mmkFont!, range: nsRange)
        self.grandTotalValueLbl.attributedText = attrString
        
        
    }
}


extension OrderConfirmation {
    func didEncounteredErrorWhilePayment(errMsg: NSError) {
        DispatchQueue.main.async {
            if let payVC = self.paymentVC {
                payVC.dismiss(animated: true, completion: nil)
            }
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            AppUtility.showToastlocal(message: errMsg.domain.localized , view: self.view)
        }
    }

    func paymentSuccessfull(raw: Data?) {
        guard let data = raw else {
            return
        }
        do {
            let decoder = JSONDecoder()
            let model = try decoder.decode(PaymentSuccessModel.self, from: data)
            guard let xmlString = model.data else { return }
            let xml = SWXMLHash.parse(xmlString)
            self.enumerate(indexer: xml)
            if model.code == 200 {
                DispatchQueue.main.async {
                   // self.showPaymentProcessingVC()
                }
            } else {
                DispatchQueue.main.async {
                    if let payVC = self.paymentVC {
                        payVC.dismiss(animated: true, completion: {
                            AppUtility.showToastlocal(message: model.message ?? "", view: self.view)
                        })
                    }
                }
            }
        } catch _ {
            // catche handling
            AppUtility.showInternetErrorToast(AppConstants.InternetErrorText.message, view: self.view)
        }
    }
}


extension OrderConfirmation: my2c2pPaymentFormViewControllerSourceDelegate{
    func paymentFormViewDidLoad() {
        
        //rounded corner
        paymentForm?.confirmbtn!.layer.cornerRadius = 5
        paymentForm?.storeCardConfirmbtn!.layer.cornerRadius = 5
        
        paymentForm?.useNewCardBtn!.layer.cornerRadius = 5
        
        //add border color for buttons
        paymentForm?.confirmbtn!.layer.borderWidth = 1
        paymentForm?.confirmbtn!.layer.borderColor = UIColor.gray.cgColor
        
        paymentForm?.storeCardConfirmbtn!.layer.borderWidth = 1
        paymentForm?.storeCardConfirmbtn!.layer.borderColor = UIColor.gray.cgColor
        
        paymentForm?.useNewCardBtn!.layer.borderWidth = 1
        paymentForm?.useNewCardBtn!.layer.borderColor = UIColor.gray.cgColor
        
    }
}

extension OrderConfirmation:OKPaymentWebViewDelegate{
    func success(response: [String: String]) {
        print(response)
        DispatchQueue.main.async {
            self.showPaymentProcessingVC(responseDict: response)
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "OKPaymentSegue" {
            if let vc = segue.destination as? OKPaymentWebViewController {
                vc.dict["Destination"] = ((self.checkOutOrderInfo?.Merchent_Number) ?? "") as String
                vc.dict["totalAmount"] = (self.checkOutOrderInfo?.OrderTotal! ?? "1.0")
                vc.OKPaymentDelegate = self
            }
        }
    }
    
     
}
