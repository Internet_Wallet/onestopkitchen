
//
//  Payment.swift
//  VMart
//
//  Created by Shobhit Singhal on 11/6/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit

class Payment: MartBaseViewController {
    @IBOutlet weak var paymentTableView: UITableView!
    @IBOutlet weak var continueBtn: UIButton!{
        didSet {
            self.continueBtn.setTitle("CONTINUE".localized, for: .normal)
            //            self.continueBtn.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 32, greenValue: 89, blueValue: 235, alpha: 1), UIColor.magenta])
            self.continueBtn.applyButtonGradient(colors: [UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1), UIColor.init(red: 20.0/255.0, green: 185.0/255.0, blue: 85.0/255.0, alpha: 1.0)])
            continueBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    var aPIManager         = APIManager()
    var paymentMethodArray = [PaymentMethod]()
    var selectedPaymentMethod = ""
    var selectedSubPaymentMethod = ""
    var screenFrom: String? = "Other"
    var errorFromServer: Bool?
    
    var checkedLeftTable = [Bool]()
    
    var rowcollapse = false
    var cardselection = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        paymentTableView.estimatedRowHeight = 50
        paymentTableView.rowHeight = UITableView.automaticDimension
        self.getPaymentMethod()
        
        self.navigationController?.navigationBar.tintColor = .white
        
        if let screenFrm = screenFrom, screenFrm == "Cart" {
            let button = UIButton()
            button.setImage(UIImage(named: "back"), for: .normal)
            button.addTarget(self, action: #selector(dismissScreen), for: .touchUpInside)
            button.imageEdgeInsets.left = -35
            let item = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem?.tintColor = UIColor.white
            self.navigationItem.leftBarButtonItem = item
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Payment".localized
        
       rowcollapse = false
        cardselection = -1
        for (index,_) in checkedLeftTable.enumerated() {
            checkedLeftTable[index] = false
        }
        paymentTableView.reloadData()
    }
    
    func makeLeftCheckArrayToFalse() {
        checkedLeftTable = Array(repeating: false, count: self.paymentMethodArray.count)
        if self.paymentMethodArray.count > 0 {
            // checkedLeftTable[0] = true
        }
    }
    
    @objc func dismissScreen() {
        if let screenFrm = screenFrom {
            switch screenFrm {
            case "Cart":
                DispatchQueue.main.async {
                    self.navigationController?.popToRootViewController(animated: true)
                }                
            default:
                DispatchQueue.main.async {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
    
    func getPaymentMethod() {
        self.aPIManager.getPaymentMethod(onSuccess: { [weak self] gotShippingMethod in
            self?.paymentMethodArray = gotShippingMethod
            self?.errorFromServer = false
            self?.paymentTableView.delegate = self
            self?.paymentTableView.dataSource = self
            self?.makeLeftCheckArrayToFalse()
            self?.paymentTableView.reloadData()
            if (self?.paymentMethodArray.count ?? 0) > 0 {
                self?.selectedPaymentMethod = self?.paymentMethodArray[0].Name as String? ?? ""
            }
            if let viewLoc = self?.view {
                AppUtility.hideLoading(viewLoc)
            }
            }, onError: { [weak self] message in
                self?.errorFromServer = true
                if let viewLoc = self?.view {
                    AppUtility.showToastlocal(message: message!, view: viewLoc)
                    AppUtility.hideLoading(viewLoc)
                }
        })
    }
    
    
    
    @IBAction func continueBtnAction(_ sender: UIButton) {
        
        if self.errorFromServer!{
            AppUtility.showToastlocal(message: "Payment method not coming from server side", view: self.view)
        }
        else{
            
            // changes for cash on delivery option
            var text = ""
            switch selectedPaymentMethod {
            case "2C2P":
                text =  "Payments.2C2P"
            case "Cash On Delivery (COD)":
                text = "Payments.CashOnDelivery"
            case "COD":
                text = "Payments.COD"
            default:
                text = "Payments.OKDollar"
            }
            // let text = (selectedPaymentMethod.contains("2C2P")) ? "Payments.2C2P" : "Payments.OKDollar"
            self.aPIManager.setPaymentMethods(text as NSString, subPaymentType: "", onSuccess: {
                
                self.performSegue(withIdentifier: "PaymentMethodsToOrderConfirmation", sender: self)
                
                //                if self.selectedPaymentMethod.contains("Money Order") {
                //                    self.performSegue(withIdentifier: "PaymentMethodsToCardDetails", sender: self)
                //                }
                //                else {
                //                    self.performSegue(withIdentifier: "PaymentMethodsToOrderConfirmation", sender: self)
                //                }
                
            }) { (error) in
                
            }
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PaymentMethodsToOrderConfirmation" {
            if let rootViewController = segue.destination as? OrderConfirmation {
                rootViewController.paymenType = self.selectedPaymentMethod
                rootViewController.subPaymentType = self.selectedSubPaymentMethod
            }
        }
    }
}

extension Payment: UITableViewDelegate, UITableViewDataSource {
  
    func headerAction(textVal:String) {
        
        for (index,_) in checkedLeftTable.enumerated() {
            checkedLeftTable[index] = false
        }
        // changes for cash on delivery option
        var text = ""
        switch textVal {
        case "2C2P":
            text =  "Payments.2C2P"
        case "Cash On Delivery (COD)":
            text = "Payments.CashOnDelivery"
        case "COD":
            text = "Payments.COD"
        default:
            text = "Payments.OKDollar"
        }
        
        
        
        if textVal != "2C2P" {
            // let text = (selectedPaymentMethod.contains("2C2P")) ? "Payments.2C2P" : "Payments.OKDollar"
            self.aPIManager.setPaymentMethods(text as NSString, subPaymentType: "", onSuccess: {
                self.selectedPaymentMethod = textVal
                self.performSegue(withIdentifier: "PaymentMethodsToOrderConfirmation", sender: self)
                
                self.paymentTableView.reloadData()
                
            }) { (error) in
                
            }
        }
        
    }
    
    @objc func BtnHeaderCLick(_ sender: UIButton) {
      
        if sender.tag == 0 {
            rowcollapse = true
        } else {
            self.headerAction(textVal: paymentMethodArray[sender.tag].Name as String)
        }
        checkedLeftTable[sender.tag] = true
        paymentTableView.reloadData()
    }
    
    @objc func cardCLick(_ sender: UIButton) {
        self.selectedPaymentMethod = paymentMethodArray[0].subPaymentMethodNames[sender.tag]["Name"] as? String ?? ""
       
        self.cardselection = sender.tag
        //checkedLeftTable[indexPath.row] = true
        paymentTableView.reloadData()
    
        self.payMentType(paymentSubType: self.selectedPaymentMethod)
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let identifier = "PaymentCell"
        let cell: paymentTableViewCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? paymentTableViewCell
        cell.radioImg.tag = section
        cell.paymentOptionData(str: paymentMethodArray[section].Name as String)
        cell.radioImg.image = UIImage(named: "radio")
        if checkedLeftTable[section] {
            cell.radioImg.image = UIImage(named: "act_radio")
        } else {
            cell.radioImg.image = UIImage(named: "radio")
        }
        cell.btnHeader.tag = section
        cell.btnHeader.addTarget(self, action: #selector(BtnHeaderCLick(_:)), for: .touchUpInside)
        cell.selectionStyle = .none
        cell.Delegate = self
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            if rowcollapse {
                return paymentMethodArray[0].subPaymentMethodNames.count
            }
        return 0
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "paymentCellTwo", for: indexPath) as! PaymentCellTwo
            cell.radioImgButton.tag = indexPath.row
            //cell.paymentOptionData(str: paymentMethodArray[indexPath.row].Name as String, isSelected: checkedLeftTable[indexPath.row])
            
            cell.radioImgButton.setImage(UIImage(named: "radio"), for: .normal)
            if cardselection == indexPath.row {
                cell.radioImgButton.setImage(UIImage(named: "act_radio"), for: .normal)//image = UIImage(named: "act_radio")
            }
            
            cell.cardnameLabel.text =  paymentMethodArray[indexPath.section].subPaymentMethodNames[indexPath.row]["Name"] as? String ?? ""
            
            cell.radioImgButton.addTarget(self, action: #selector(cardCLick(_:)), for: .touchUpInside)
            
            //cell.paymentOptionData(str: cell.cardnameLabel.text ?? "")
            cell.selectionStyle = .none
            
            cell.DelegateX = self
            return cell
        }
        else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "PaymentCell", for: indexPath) as! paymentTableViewCell
            cell.radioImg.tag = indexPath.row
            cell.paymentOptionData(str: paymentMethodArray[indexPath.row].Name as String)
            cell.radioImg.image = UIImage(named: "radio")
            if checkedLeftTable[indexPath.row] {
                cell.radioImg.image = UIImage(named: "act_radio")
            } else {
                cell.radioImg.image = UIImage(named: "radio")
            }
            cell.selectionStyle = .none
            cell.Delegate = self
            return cell
        }
    }
    
    func payMentType(paymentSubType : String) {
        DispatchQueue.main.async {
            let alertVC = SAlertController()
            alertVC.ShowSAlert(title: "Information".localized, withDescription: "Transaction fees 4.5% (or) 500mmk (whichever is higher) will be deducted".localized, onController: self)
            let Ok = SAlertAction()
            Ok.action(name: "OK".localized, AlertType: .defualt, withComplition: {
                self.selectedSubPaymentMethod = paymentSubType
                self.aPIManager.setPaymentMethods("Payments.2C2P", subPaymentType: paymentSubType  , onSuccess: {
                    DispatchQueue.main.async {
                        self.performSegue(withIdentifier: "PaymentMethodsToOrderConfirmation", sender: self)
                        self.checkedLeftTable[0] = false
                        self.paymentTableView.reloadData()
                    }
                }) { (error) in
                    
                }
            })
            let CancelAction = SAlertAction()
            CancelAction.action(name: "Cancel".localized, AlertType: .defualt, withComplition: {
                DispatchQueue.main.async {
//                    self.checkedLeftTable[0] = false
//                    self.paymentTableView.reloadData()
                }
            })
            alertVC.addAction(action: [Ok , CancelAction])
           }
        }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //paymentTableView.deselectRow(at: indexPath, animated: true)
        
        self.selectedPaymentMethod = paymentMethodArray[0].subPaymentMethodNames[indexPath.row]["Name"] as? String ?? ""
       
        self.cardselection = indexPath.row
        //checkedLeftTable[indexPath.row] = true
        paymentTableView.reloadData()
    
        self.payMentType(paymentSubType: self.selectedPaymentMethod)
        
        
        /*if selectedPaymentMethod != "2C2P" {
            // let text = (selectedPaymentMethod.contains("2C2P")) ? "Payments.2C2P" : "Payments.OKDollar"
            self.aPIManager.setPaymentMethods(text as NSString, subPaymentType: "", onSuccess: {
                
                self.performSegue(withIdentifier: "PaymentMethodsToOrderConfirmation", sender: self)
                
                //                if self.selectedPaymentMethod.contains("Money Order") {
                //                    self.performSegue(withIdentifier: "PaymentMethodsToCardDetails", sender: self)
                //                }
                //                else {
                //                    self.performSegue(withIdentifier: "PaymentMethodsToOrderConfirmation", sender: self)
                //                }
                
                self.checkedLeftTable[indexPath.row] = false
                self.paymentTableView.reloadData()
                
            }) { (error) in
                
            }
        }*/
        //                let cell = tableView.cellForRow(at: indexPath) as? paymentTableViewCell
        //                cell?.radioImg.image = UIImage(named: "act_radio")
        
        //        if indexPath.row == 0{
        //            for checkSelectedIndex in self.paymentMethodArray{
        //                checkSelectedIndex.Selected = false
        //            }
        //            self.paymentMethodArray[indexPath.row].Selected = true
        //            self.paymentTableView.reloadData()
        //            self.selectedPaymentMethod = paymentMethodArray[indexPath.row].Name as String
        //        }
        //        else{
        //            self.performSegue(withIdentifier: "PaymentMethodsToCardDetails", sender: self)
        //        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
                if indexPath.row == 0{
                    return UITableView.automaticDimension
                }
                else{
                    return 70
                }
        }
}

extension Payment:paymentTableViewCellDelegate{
    
//    func payMentType(paymentSubType : String) {
//        DispatchQueue.main.async {
//            let alertVC = SAlertController()
//            alertVC.ShowSAlert(title: "Information".localized, withDescription: "Transaction fees 4.5% (or) 500mmk (whichever is higher) will be deducted".localized, onController: self)
//            let Ok = SAlertAction()
//            Ok.action(name: "OK".localized, AlertType: .defualt, withComplition: {
//                self.selectedSubPaymentMethod = paymentSubType
//                self.aPIManager.setPaymentMethods("Payments.2C2P", subPaymentType: paymentSubType  , onSuccess: {
//                    DispatchQueue.main.async {
//                        self.performSegue(withIdentifier: "PaymentMethodsToOrderConfirmation", sender: self)
//                        self.checkedLeftTable[0] = false
//                        self.paymentTableView.reloadData()
//                    }
//                }) { (error) in
//                    
//                }
//            })
//            let CancelAction = SAlertAction()
//            CancelAction.action(name: "Cancel".localized, AlertType: .defualt, withComplition: {
//                DispatchQueue.main.async {
//                    self.checkedLeftTable[0] = false
//                    self.paymentTableView.reloadData()
//                }
//            })
//            alertVC.addAction(action: [Ok , CancelAction])
//           }
//        }
    
    func NearOkServiceBtnActionClick(sender: UIButton) {
        self.performSegue(withIdentifier: "NearBySeque", sender: self)
    }
    
    
}

protocol paymentTableViewCellDelegate: class {
    func NearOkServiceBtnActionClick(sender: UIButton)
    func payMentType(paymentSubType : String)
}

class paymentTableViewCell: UITableViewCell {
    weak var Delegate: paymentTableViewCellDelegate?
    @IBOutlet var btnHeader: UIButton!
    @IBOutlet var nearByBtn: UIButton!{
        didSet {
            self.nearByBtn.setTitle("Nearby OK$ Service".localized, for: .normal)
            nearByBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    
    @IBOutlet var underlineLbl: UILabel!
    @IBOutlet var payOtionLbl: UILabel!{
        didSet {
            self.payOtionLbl.font = UIFont(name: appFont, size: 15.0)
            self.payOtionLbl.text = self.payOtionLbl.text?.localized
            
        }
    }
    @IBOutlet var radioImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func paymentOptionData(str:String){
        
        self.payOtionLbl.font = UIFont(name: appFont, size: 15.0)
        if str == "OK Dollar"{
            self.nearByBtn.isHidden = false
            self.payOtionLbl.text = "OK Dollar Payment".localized
        }
        else if (str == "Cash On Delivery (COD)") {
            
            self.nearByBtn.isHidden = true
            self.payOtionLbl.text = "Cash On Delivery (COD)".localized
        }
        else if (str == "COD") {
            
            self.nearByBtn.isHidden = true
            self.payOtionLbl.text = "Cash On Delivery (COD)".localized
        }
        else{
            self.nearByBtn.isHidden = true
            self.payOtionLbl.text = "Select Payment Type".localized
        }
    }
    
    @IBAction func NearOkServiceBtnAction(_ sender: UIButton) {
        self.Delegate?.NearOkServiceBtnActionClick(sender: sender)
    }
}


class PaymentCellTwo: UITableViewCell {
    weak var DelegateX: paymentTableViewCellDelegate?
    
//    @IBOutlet var imageType : UIStackView!
//
//    @IBOutlet var payMentOptionView : UIStackView!
//    @IBOutlet var payMentOptionHeight : NSLayoutConstraint!
//
//    @IBOutlet var underlineLbl: UILabel!
//    @IBOutlet var payOtionLbl: UILabel!{
//        didSet {
//            self.payOtionLbl.font = UIFont(name: appFont, size: 15.0)
//            self.payOtionLbl.text = self.payOtionLbl.text?.localized
//
//        }
//    }
    @IBOutlet var radioImgButton: UIButton!
    @IBOutlet var cardnameLabel: UILabel!
//    @IBOutlet var lableOne: UILabel!
//    @IBOutlet var labelTwo: UILabel!
//    @IBOutlet var labelThree: UILabel!
//
//    @IBOutlet var radioImg: UIImageView!
//
//    @IBOutlet var btnOne : UIButton!
//    @IBOutlet var btnTwo : UIButton!
//   // @IBOutlet var btnThree : UIButton!
//    @IBOutlet var btnFour : UIButton!
    
    var subPaymentType : String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func paymentOptionData(str:String){
        subPaymentType = str
        self.DelegateX?.payMentType(paymentSubType: self.subPaymentType ?? "")
//
//            self.payOtionLbl.font = UIFont(name: appFont, size: 15.0)
//            self.payOtionLbl.text = "Select Payment Type".localized
//
//            self.btnOne.addTarget(self, action: #selector(PayOptionCLick(_:)), for: .touchUpInside)
//            self.btnTwo.addTarget(self, action: #selector(PayOptionCLick(_:)), for: .touchUpInside)
//            //self.btnThree.addTarget(self, action: #selector(PayOptionCLick(_:)), for: .touchUpInside)
//            self.btnFour.addTarget(self, action: #selector(PayOptionCLick(_:)), for: .touchUpInside)
//
//        let tap = UITapGestureRecognizer(target: self, action: #selector(LabeloneTapped(tapGestureRecognizer:)))
//        lableOne.isUserInteractionEnabled = true
//        lableOne.addGestureRecognizer(tap)
//
//        let tap1 = UITapGestureRecognizer(target: self, action: #selector(LabelTwoTapped(tapGestureRecognizer:)))
//        labelTwo.isUserInteractionEnabled = true
//        labelTwo.addGestureRecognizer(tap1)
//
//        let tap2 = UITapGestureRecognizer(target: self, action: #selector(LabelThreeTapped(tapGestureRecognizer:)))
//        labelThree.isUserInteractionEnabled = true
//        labelThree.addGestureRecognizer(tap2)
//
//        let paymentstr = str
//
//        lableOne.text = str
//
//        if isSelected {
//            payMentOptionView.isHidden = false
//            payMentOptionHeight.constant = 105
//        }
//        else {
//            payMentOptionView.isHidden = true
//            payMentOptionHeight.constant = 0
//            self.btnOne.setImage(UIImage(named: "radio"), for: .normal)
//            self.btnTwo.setImage(UIImage(named: "radio"), for: .normal)
//            //self.btnThree.setImage(UIImage(named: "radio"), for: .normal)
//            self.btnFour.setImage(UIImage(named: "radio"), for: .normal)
//        }
    }
    
//   @objc func PayOptionCLick(_ sender: UIButton) {
//    switch sender.tag {
//    case 11:
//        self.btnOne.setImage(UIImage(named: "act_radio"), for: .normal)
//        self.btnTwo.setImage(UIImage(named: "radio"), for: .normal)
//        //self.btnThree.setImage(UIImage(named: "radio"), for: .normal)
//        self.btnFour.setImage(UIImage(named: "radio"), for: .normal)
//        self.subPaymentType = "VISA"
//    case 12:
//        self.btnOne.setImage(UIImage(named: "radio"), for: .normal)
//        self.btnTwo.setImage(UIImage(named: "act_radio"), for: .normal)
//        //self.btnThree.setImage(UIImage(named: "radio"), for: .normal)
//        self.btnFour.setImage(UIImage(named: "radio"), for: .normal)
//        self.subPaymentType = "MASTERCARD"
////    case 13:
////        self.btnOne.setImage(UIImage(named: "radio"), for: .normal)
////        self.btnTwo.setImage(UIImage(named: "radio"), for: .normal)
////        self.btnThree.setImage(UIImage(named: "act_radio"), for: .normal)
////        self.btnFour.setImage(UIImage(named: "radio"), for: .normal)
////        self.subPaymentType = "MPU"
//    case 14:
//        self.btnOne.setImage(UIImage(named: "radio"), for: .normal)
//        self.btnTwo.setImage(UIImage(named: "radio"), for: .normal)
//        //self.btnThree.setImage(UIImage(named:"radio"), for: .normal)
//        self.btnFour.setImage(UIImage(named: "act_radio"), for: .normal)
//        self.subPaymentType = "JCB"
//    default:
//          return
//    }
//    self.DelegateX?.payMentType(paymentSubType: self.subPaymentType ?? "")
//    }
//
    
//    @objc func LabeloneTapped(tapGestureRecognizer: UITapGestureRecognizer)
//    {
//        self.btnOne.setImage(UIImage(named: "act_radio"), for: .normal)
//        self.btnTwo.setImage(UIImage(named: "radio"), for: .normal)
//        //self.btnThree.setImage(UIImage(named: "radio"), for: .normal)
//        self.btnFour.setImage(UIImage(named: "radio"), for: .normal)
//        self.subPaymentType = "VISA"
//        self.DelegateX?.payMentType(paymentSubType: self.subPaymentType ?? "")
//    }
//    @objc func LabelTwoTapped(tapGestureRecognizer: UITapGestureRecognizer)
//    {
//        self.btnOne.setImage(UIImage(named: "radio"), for: .normal)
//        self.btnTwo.setImage(UIImage(named: "act_radio"), for: .normal)
//        //self.btnThree.setImage(UIImage(named: "radio"), for: .normal)
//        self.btnFour.setImage(UIImage(named: "radio"), for: .normal)
//        self.subPaymentType = "MASTERCARD"
//        self.DelegateX?.payMentType(paymentSubType: self.subPaymentType ?? "")
//    }
//    @objc func LabelThreeTapped(tapGestureRecognizer: UITapGestureRecognizer)
//    {
//        self.btnOne.setImage(UIImage(named: "radio"), for: .normal)
//        self.btnTwo.setImage(UIImage(named: "radio"), for: .normal)
//        //self.btnThree.setImage(UIImage(named:"radio"), for: .normal)
//        self.btnFour.setImage(UIImage(named: "act_radio"), for: .normal)
//        self.subPaymentType = "JCB"
//        self.DelegateX?.payMentType(paymentSubType: self.subPaymentType ?? "")
//    }
    
    
}
