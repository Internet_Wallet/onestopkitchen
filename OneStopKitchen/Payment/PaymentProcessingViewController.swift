//
//  PaymentProcessingViewController.swift
//  VMart
//
//  Created by Kethan on 12/13/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit

class PaymentProcessingViewController: MartBaseViewController {
    
    @IBOutlet var PaymentProcessLabel: UILabel!{
        didSet {
            self.PaymentProcessLabel.font = UIFont(name: appFont, size: 15.0)
            self.PaymentProcessLabel.text = self.PaymentProcessLabel.text?.localized
            
        }
    }
    
    var paymentDict: Dictionary<String, Any>?
    var paymentResponse: Dictionary<String, Any>?
    var aPIManager = APIManager()
    var navController: UINavigationController?
    var paymenType = ""
    var totalAmount = ""
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(paymenType)
        self.PaymentProcessLabel.text = "Your payment is processing. Do not refresh or go back from the page.".localized
        if (self.paymenType == "Cash On Delivery (COD)") || (self.paymenType == "COD") {
            self.PaymentProcessLabel.text = "".localized
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.savePaymentTransactionHistory()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    private func savePaymentTransactionHistory() {
        var payStatusDict = Dictionary<String, Any>()
        if self.paymenType == "OK Dollar"{
            payStatusDict["TransactionId"] = "0"
            payStatusDict["TransactionStatus"] = "Transaction Failure"
            payStatusDict["PaymentMethod"] = "1"
            payStatusDict["TransactionAmount"] = 0.0
            
            //  web apyment scenerio *********************
            
            if let model = paymentDict, let description = model["ResponseJson"] as? String {
                //  if description.lowercased() == "Transaction Successful".lowercased() {
                guard let responseData = AppUtility.convertToDictionary(text: description) else { return }
                print(responseData["TransactionId"])
                payStatusDict["TransactionId"] = responseData["TransactionId"] as? String ?? ""
                payStatusDict["TransactionDescription"] = "Transaction Successful"
                payStatusDict["TransactionStatusCode"] = "000"
                payStatusDict["PaymentMethod"] =  "Payments.OKDollar"//self.paymenType//"4"
                //                    payStatusDict["TransactionStatusCode"] =   model["failReason"] as? String ?? ""  //description
                payStatusDict["TransactionAmount"] = responseData["Amount"] as? String ?? ""
                //  }
            }
            //                 }
            
            //  SDK apyment scenerio *********************
            //            if let model = paymentDict, let description = model["resultdescription"] as? String {
            //                if description.lowercased() == "Transaction Successful".lowercased() {
            //                    payStatusDict["TransactionId"] = model["transid"] as? String ?? ""
            //                    payStatusDict["TransactionDescription"] = "Transaction Successful"
            //                    payStatusDict["TransactionStatusCode"] = "000"
            //                    payStatusDict["PaymentMethod"] =  "Payments.OKDollar"//self.paymenType//"4"
            //                    //                    payStatusDict["TransactionStatusCode"] =   model["failReason"] as? String ?? ""  //description
            //                    payStatusDict["TransactionAmount"] = model["amount"] as? String ?? ""
            //                }
            //            }
        }
        else if (self.paymenType == "Cash On Delivery (COD)"){
            payStatusDict["TransactionId"] = "0"
            payStatusDict["TransactionStatus"] = "Pending"
            payStatusDict["PaymentMethod"] = "1"
            payStatusDict["TransactionAmount"] = 0.0
            if let model = paymentDict, let description = model["resultdescription"] as? String {
                if description.lowercased() == "Transaction Successful".lowercased() {
                    payStatusDict["TransactionId"] = model["transid"] as? String ?? ""
                    payStatusDict["TransactionDescription"] = "Transaction Successful"
                    payStatusDict["TransactionStatusCode"] = "001"
                    payStatusDict["PaymentMethod"] =  "Payments.CashOnDelivery"//self.paymenType//"4"
                    payStatusDict["TransactionAmount"] = String(format: "%f", ((model["amount"] as? Double)!)) 
                }
            }
        }
        else if (self.paymenType == "COD"){
            payStatusDict["TransactionId"] = "0"
            payStatusDict["TransactionStatus"] = "Pending"
            payStatusDict["PaymentMethod"] = "1"
            payStatusDict["TransactionAmount"] = 0.0
            if let model = paymentDict, let description = model["resultdescription"] as? String {
                if description.lowercased() == "Transaction Successful".lowercased() {
                    payStatusDict["TransactionId"] = model["transid"] as? String ?? ""
                    payStatusDict["TransactionDescription"] = "Transaction Successful"
                    payStatusDict["TransactionStatusCode"] = "001"
                    payStatusDict["PaymentMethod"] =  "Payments.COD"//self.paymenType//"4"
                    payStatusDict["TransactionAmount"] = String(format: "%f", ((model["amount"] as? Double)!)) 
                }
            }
        }
        else{
            payStatusDict["TransactionId"] = "0"
            payStatusDict["TransactionStatus"] = "Transaction Failure"
            payStatusDict["PaymentMethod"] = "1"
            payStatusDict["TransactionAmount"] = 0.0
            if let model = paymentDict,let description = model["failReason"] as? String {
                if description.lowercased() == "APPROVED".lowercased() {
                    payStatusDict["TransactionId"] = model["tranRef"] as? String ?? ""
                    payStatusDict["TransactionDescription"] = "Transaction Successful"
                    payStatusDict["TransactionStatusCode"] =   "000"//model["failReason"] as? String ?? ""  //description
                    payStatusDict["PaymentMethod"] = "Payments.2C2P" //self.paymenType//"4"
                    payStatusDict["TransactionAmount"] = self.totalAmount
                }
            }
        }
         let params = AppUtility.JSONStringFromAnyObject(value: payStatusDict as AnyObject)
         self.checkoutComplete(payDictData: params)
    }
    
    private func checkoutComplete( payDictData : String) {
        
        let urlString = String(format: "%@/checkout/checkoutcomplete", APIManagerClient.sharedInstance.base_url)
        guard let url = URL(string: urlString) else { return }
        aPIManager.genericClass(url: url, param: payDictData as AnyObject, httpMethod: "POST", header: true) { [weak self](response, success, data) in
            if success {
                do {
                    let _ = JSONDecoder()
                    guard let dataVal = data else { return }
                    if let dict = try JSONSerialization.jsonObject(with: dataVal, options: .allowFragments) as? Dictionary<String, Any> {
                        //let model = try decoder.decode(VMUpdatePaymentStatusModel.self, from: dataVal)
                        self?.paymentResponse = dict
                        if let statusCode = dict["StatusCode"] as? Int, statusCode == 200 {
                            //                            UserDefaults.standard.setValue("", forKey: "badge")
                            UserDefaults.standard.set("", forKey: "badge")
                            UserDefaults.standard.synchronize()
                            DispatchQueue.main.async {
                                    self?.showReceiptScreen()
                                }
//                            let orders = self?.paymentResponse?["Orders"] as? [[String : Any]]
//                            let Items = orders?[0]["Items"] as? [[String : Any]]
//                            let orderID = Items?[0]["Id"] as? Int ?? 0
//                            let customOrdrNo = orders?[0]["CustomOrderNumber"] as? String
//                            let UpdateData = ["orderID":orderID ,"customOrderNumber":customOrdrNo ?? "1"] as [String : Any]
//                            self?.UpdatePaymentStatus(model: UpdateData)
                        } else {
                            DispatchQueue.main.async {
                                self?.dismiss(animated: false, completion: {
                                    DispatchQueue.main.async {
                                        if let viewLoc = UIApplication.topViewController() {
                                            if let errorList = dict["ErrorList"] as? [String], errorList.count > 0 {
                                                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                                    AppUtility.showToastlocal(message: errorList.first ?? "" , view: viewLoc.view)
                                                }
                                            }
                                        }
                                    }
                                })
                            }
                        }
                    } else {
                        DispatchQueue.main.async {
                            self?.dismiss(animated: false, completion: nil)
                        }
                    }
                } catch _ {
                    DispatchQueue.main.async {
                        self?.dismiss(animated: false, completion: nil)
                    }
                }
            }
        }
    }
    
    private func updatePayStatusParam(Dict : [String : Any]) -> Dictionary<String, Any> {
        var dict = Dictionary<String, Any>()
        dict["orderId"] = Dict["orderID"] ?? 0
        dict["orderNumber"] = Dict["customOrderNumber"] ?? ""
        dict["phoneNumber"] = ""
        dict["productAmount"] = ""
        dict["referenceNumber"] = ""
        dict["shippingAddress"] = ""
        dict["shippingCharges"] = ""
        dict["successMessage"] = self.paymentDict?["resultdescription"] as? String ?? ""
        dict["totalPaidAmount"] = self.paymentDict?["amount"] as? String ?? ""
        dict["transactionId"] = self.paymentDict?["transid"] as? String ?? ""
        dict["transactionStatus"] = "Success"
        dict["StatusCode"] = 0
        return dict
    }
    
    private func UpdatePaymentStatus(model: [String : Any]) {
        let urlString = String(format: "%@/checkout/UpdatePaymentStatus", APIManagerClient.sharedInstance.base_url)
        guard let url = URL(string: urlString) else { return }
        let params = AppUtility.JSONStringFromAnyObject(value: self.updatePayStatusParam(Dict: model) as AnyObject)
        aPIManager.genericClass(url: url, param: params as AnyObject, httpMethod: "POST", header: true) { [weak self](response, success, data) in
            if success {
                do {
                    let _ = JSONDecoder()
                    guard let dataVal = data else { return }
                    if let dict = try JSONSerialization.jsonObject(with: dataVal, options: .allowFragments) as? Dictionary<String, Any> {
                        //let model = try decoder.decode(VMUpdatePaymentStatusModel.self, from: dataVal)
                        self?.paymentResponse = dict
                        if let statusCode = dict["StatusCode"] as? Int, statusCode == 200 {
                            //                            UserDefaults.standard.setValue("", forKey: "badge")
                            UserDefaults.standard.set("", forKey: "badge")
                            UserDefaults.standard.synchronize()
                            DispatchQueue.main.async {
                                self?.showReceiptScreen()
                            }
                        } else {
                            DispatchQueue.main.async {
                            if let viewLoc = self?.view {
                                if let errorList = dict["ErrorList"] as? [String], errorList.count > 0 {
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                        AppUtility.showToastlocal(message: errorList.first ?? "" , view: viewLoc)
                                    }
                                }
                             }
                              self?.dismiss(animated: false, completion: nil)
                            }
                        }
                    } else {
                        DispatchQueue.main.async {
                            self?.dismiss(animated: false, completion: nil)
                        }
                    }
                } catch _ {
                    DispatchQueue.main.async {
                        self?.dismiss(animated: false, completion: nil)
                    }
                }
            }
        }
    }
    
    private func showReceiptScreen() {
        
        if let paymentSuccessVC = UIStoryboard(name: "Cart", bundle: nil).instantiateViewController(withIdentifier: "PaymentSuccess_ID") as? PaymentSuccess {
            paymentSuccessVC.paymentResponse = self.paymentResponse
            paymentSuccessVC.paymentType = self.paymenType
            if let nav = navController {
                nav.pushViewController(paymentSuccessVC, animated: true)
            }
            self.dismiss(animated: false, completion: nil)
        }
    }
}
