//
//  ProductDetailsCollectionViewCell2.swift
//  VMart
//
//  Created by Shobhit Singhal on 10/27/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit

class ProductDetailsCollectionViewCell2: UICollectionViewCell {
    @IBOutlet var productImageView: UIImageView!
    @IBOutlet var alphaView: UIView!
    @IBOutlet var moreImageCountLbl: UILabel!{
        didSet{
            self.moreImageCountLbl.font = UIFont(name: appFont, size: 15.0)
            self.moreImageCountLbl.text = self.moreImageCountLbl.text?.localized
        }
    }
}
