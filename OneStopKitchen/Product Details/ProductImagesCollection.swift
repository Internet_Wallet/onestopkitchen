//
//  ProductImagesCollection.swift
//  VMart
//
//  Created by Shobhit Singhal on 10/20/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit

@objc protocol TapOnWishlistBtnOnProductImageCollectionDelegate: class {
    func tappedOnWishlistBtn(check:Bool)
}

class ProductImagesCollection: MartBaseViewController {
    
    @IBOutlet weak var productCollectionView1: UICollectionView!
    @IBOutlet weak var productCollectionView2: UICollectionView!
    @IBOutlet weak var wishListBtn: UIButton!{
        didSet {
            wishListBtn.setTitle((wishListBtn.titleLabel?.text ?? "").localized, for: .normal)
            wishListBtn.titleLabel?.font = UIFont(name: appFont, size: 15.0)
        }
    }
    @IBOutlet weak var titleLbl: MarqueeLabel!{
        didSet {
            self.titleLbl.text = self.titleLbl.text?.localized
            self.titleLbl.font = UIFont(name: appFont, size: 18.0)
        }
    }
    @IBOutlet weak var pageControl: UIPageControl!
    
    var MINIMUM_SCALE: CGFloat = 1.0
    var MAXIMUM_SCALE: CGFloat = 3.0
    var fullImageUrlArray = [String]()
    var initialTouchPoint: CGPoint = CGPoint(x: 0,y: 0)
    var selectedIndex = 0
    var checked = [Bool]()
    var wishListSelected = false
    var productId = 0
    var productName = ""
    var selectedTical = ""
    var aPIManager = APIManager()
    var dictionaryArray = NSMutableArray()
    var isTappedWishlistBtn = false
    var delegate:TapOnWishlistBtnOnProductImageCollectionDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleLbl.text = productName
        self.titleLbl.font = UIFont(name: appFont, size: 18.0)
        if self.productName.count > 50 {
            self.titleLbl.speed = .duration(20)
        } else {
            self.titleLbl.speed = .duration(10)
        }
        //        self.navigationItem.setMarqueLabelInNavigation(TextStr: self.productName, count: self.productName.count)
        checked = Array(repeating: false, count: fullImageUrlArray.count)
        if !self.checked.isEmpty {
            checked[0] = true
        }
        if self.wishListSelected == true {
            self.wishListBtn.setImage(UIImage(named: "act_wishlist"), for: .normal)
            wishListSelected = true
        } else {
            self.wishListBtn.setImage(UIImage(named: "ic_wishList"), for: .normal)
            wishListSelected = false
        }
        if fullImageUrlArray.count == 1 {
            pageControl.isHidden = true
        }
        pageControl.numberOfPages = fullImageUrlArray.count
        productCollectionView1.reloadData()
        productCollectionView2.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        //        statusBarView.backgroundColor = UIColor.init(red: 36.0/255.0, green: 105.0/255.0, blue: 215.0/255.0, alpha: 1.0)
        //        view.addSubview(statusBarView)
        
        self.view.applyViewnGradient(colors: [UIColor.colorWithRedValue(redValue: 13, greenValue: 177, blueValue: 75, alpha: 1), UIColor.init(red: 20.0/255.0, green: 185.0/255.0, blue: 85.0/255.0, alpha: 1.0)])
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        productCollectionView1.scrollToItem(at: IndexPath(row: selectedIndex, section: 0), at: .right, animated: true)
        productCollectionView2.scrollToItem(at: IndexPath(row: selectedIndex, section: 0), at: .right, animated: true)
        let indexPath = IndexPath.init(row: selectedIndex, section: 0)
        scrollToIndexCollection(indexpath: indexPath)
    }
    
    @IBAction func dismissAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func panGestureRecognizerHandler(_ sender: UIPanGestureRecognizer) {
        let touchPoint = sender.location(in: self.view?.window)
        if sender.state == UIGestureRecognizer.State.began {
            initialTouchPoint = touchPoint
        } else if sender.state == UIGestureRecognizer.State.changed {
            if touchPoint.y - initialTouchPoint.y > 0 {
                self.view.frame = CGRect(x: 0, y: touchPoint.y - initialTouchPoint.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
            }
        } else if sender.state == UIGestureRecognizer.State.ended || sender.state == UIGestureRecognizer.State.cancelled {
            if touchPoint.y - initialTouchPoint.y > 110 {
                self.dismiss(animated: true, completion: nil)
            } else {
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                })
            }
        }
    }
    
    private func scrollToIndexCollection(indexpath: IndexPath) {
        for (index,_) in checked.enumerated() {
            checked[index] = false
        }
        checked[indexpath.row] = true
        pageControl.currentPage = indexpath.row
        DispatchQueue.main.async {
            self.productCollectionView2.reloadData()
            self.productCollectionView2.scrollToItem(at: IndexPath(row: indexpath.row, section: 0), at: .right, animated: true)
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == productCollectionView2 {
            return
        }
        DispatchQueue.main.async {
            for cell: UICollectionViewCell in self.productCollectionView1.visibleCells {
                let indexPath: IndexPath? = self.productCollectionView1.indexPath(for: cell)
                if let indexPath = indexPath {
                    self.scrollToIndexCollection(indexpath: indexPath)
                }
            }
        }
    }
    
    @IBAction func addToWishListAction(_ sender: UIButton) {
        isTappedWishlistBtn = true
        if !wishListSelected {
            self.addToWishList()
        } else {
            self.removeFromWishList()
        }
    }
    
    private func addToWishList() {
        self.wishListSelected = true
        UIView.animate(withDuration: 0.4, animations: {
            self.wishListBtn.transform = CGAffineTransform(scaleX: 1.4, y: 1.4) }, completion: { (finish: Bool) in
                UIView.animate(withDuration: 0.4, animations: {
                    self.wishListBtn.transform = CGAffineTransform.identity
                }, completion: ({finished in
                    if (finished) {
                        self.delegate?.tappedOnWishlistBtn(check: true)
                        self.wishListBtn.setImage(UIImage(named: "act_wishlist"), for: .normal)
                    }
                }))
        })
        
        self.aPIManager.loadAddProductToCart(2, productId: productId, tical: selectedTical, parameters: self.dictionaryArray, onSuccess:{ priceValue in
            //            if let viewLoc = self?.view, let vc = self {
            //            }
        }, onError: { [weak self] message in
            self?.wishListBtn.setImage(UIImage(named: "ic_wishList"), for: .normal)
            self?.showAlert(alertTitle: "", description: message ?? "")
            
        })
    }
    
    private func removeFromWishList() {
        self.wishListSelected = false
        UIView.animate(withDuration: 0.4, animations: {
            self.wishListBtn.transform = CGAffineTransform(scaleX: 1.4, y: 1.4) }, completion: { (finish: Bool) in
                UIView.animate(withDuration: 0.4, animations: {
                    self.wishListBtn.transform = CGAffineTransform.identity
                }, completion: ({finished in
                    if (finished) {
                        self.delegate?.tappedOnWishlistBtn(check: false)
                        self.wishListBtn.setImage(UIImage(named: "ic_wishList"), for: .normal)
                    }
                }))
        })
        self.aPIManager.removeProductFromWishList(2, productId: productId, parameters: self.dictionaryArray, onSuccess: { [weak self] in
            
            }, onError: { [weak self] message in
                self?.wishListBtn.setImage(UIImage(named: "act_wishlist"), for: .normal)
                self?.showAlert(alertTitle: "", description: message ?? "")
        })
    }
}

extension ProductImagesCollection: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.fullImageUrlArray.count
    }
    
    //    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    //        if collectionView == productCollectionView1 {
    //            pageControl.currentPage = indexPath.row
    //        }
    //    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == productCollectionView1 {
            let cell = productCollectionView1.dequeueReusableCell(withReuseIdentifier: "ProductImageZoomCell", for: indexPath) as! ProductImagesZoomCollectionCell
            cell.scrollviewCell.autoresizesSubviews = true
            cell.scrollviewCell.isMultipleTouchEnabled = true
            cell.scrollviewCell.minimumZoomScale = 1.0
            cell.scrollviewCell.maximumZoomScale = 4.0
            cell.scrollviewCell.clipsToBounds = true
            cell.scrollviewCell.delegate = self
            cell.scrollviewCell.zoomScale = 1.0
            cell.scrollviewCell.flashScrollIndicators()
            
            cell.productImageView.sd_setImage(with: URL(string: self.fullImageUrlArray[indexPath.row]))
            
            //            AppUtility.NKPlaceholderImage(image: UIImage(named: self.fullImageUrlArray[indexPath.row]), imageView: cell.productImageView, imgUrl: self.fullImageUrlArray[indexPath.row]) { (image) in }
            
            
            
            //            AppUtility.getData(from: URL(string: self.fullImageUrlArray[indexPath.row])!) { data, response, error in
            //                guard let data = data, error == nil else { return }
            //                print("Download Finished")
            //                DispatchQueue.main.async() {
            //                    cell.productImageView.image = UIImage(data: data)
            //                }
            //            }
            
            return cell
        } else {
            let cell = productCollectionView2.dequeueReusableCell(withReuseIdentifier: "ProductDetailsCell2", for: indexPath) as! ProductDetailsCollectionViewCell2
            
            //            cell.productImageView.sd_setImage(with: URL(string: self.fullImageUrlArray[indexPath.row]))
            if let imageFromserver = self.fullImageUrlArray[indexPath.row] as? String , imageFromserver != "" {
                AppUtility.getData(from: URL(string: imageFromserver)!) { data, response, error in
                    guard let data = data, error == nil else { return }
                    print("Download Finished")
                    DispatchQueue.main.async() {
                        cell.productImageView.image = UIImage(data: data)
                    }
                }
            }else {
                AppUtility.getData(from: URL(string: "https://sisterhoodofstyle.com/wp-content/uploads/2018/02/no-image-1.jpg")!) { data, response, error in
                    guard let data = data, error == nil else { return }
                    print("Download Finished")
                    DispatchQueue.main.async() {
                        cell.productImageView.image = UIImage(data: data)
                    }
                }
            }

            if checked[indexPath.row] {
                cell.contentView.layer.borderColor = UIColor.colorWithRedValue(redValue: 32, greenValue: 120, blueValue: 235, alpha: 1).cgColor
            } else {
                cell.contentView.layer.borderColor = UIColor.colorWithRedValue(redValue: 200, greenValue: 200, blueValue: 200, alpha: 1).cgColor
            }
            cell.contentView.layer.borderWidth = 1
            return cell
        }
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        print(String(format: "%i", scrollView.subviews.count))
        let vv = UIView()
        for v: UIView in scrollView.subviews {
            if (v is UIImageView) {
                return v
            }
        }
        return vv
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == productCollectionView2 {
            scrollToIndexCollection(indexpath: indexPath)
            productCollectionView1.scrollToItem(at: IndexPath(row: indexPath.row, section: 0), at: .right, animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == productCollectionView1 {
            return CGSize(width: productCollectionView1.bounds.width, height: productCollectionView1.bounds.height)
        } else {
            return CGSize(width: 65, height: productCollectionView2.bounds.height)
        }
    }
}
