//
//  SortBy.swift
//  VMart
//
//  Created by Shobhit Singhal on 10/20/18.
//  Copyright © 2018 Shobhit. All rights reserved.
//

import UIKit

//MARK: step 1 Add Protocol here.
protocol SortByDelegate: class {
    func SortByTestMethod(sortByTableView: UITableView,indexPath: IndexPath,sortingType : String)
    func sortedCheckMarkArray(checked: [Bool])
}

class SortBy: MartBaseViewController {
    
    weak var SDelegate: SortByDelegate?
    var sortByListArr = [AvailableSortOptions]()
    var checked = [Bool]()
    var categoryDetailsInfo = CategoryDetailsInfo()
    
    
    @IBOutlet weak var sortByTableView: UITableView!
    @IBOutlet var sortTableViewHeightConstraint: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        sortByTableView.estimatedRowHeight = 50
        //        sortByTableView.rowHeight = UITableView.automaticDimension
        sortByListArr = categoryDetailsInfo.availableSortOptions
        var height = 40
        for _ in sortByListArr {
            height += 45
        }
        sortTableViewHeightConstraint.constant = CGFloat(height)
        self.sortByTableView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.sortByTableView.delegate = self
            self.sortByTableView.dataSource = self
            self.animate()
        }
        
        if self.checked.isEmpty {
            checked = Array(repeating: false, count: sortByListArr.count)
        }
    }
    
    func animate () {
        sortByTableView.reloadData()
        let cells = sortByTableView.visibleCells
        let tableHeight: CGFloat = sortByTableView.bounds.size.height
        for i in cells {
            let cell: UITableViewCell = i as UITableViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        var index = 0
        for a in cells {
            let cell: UITableViewCell = a as UITableViewCell
            UIView.animate(withDuration: 1, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: [], animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0);
            }, completion: nil)
            index += 1
        }
    }
    
    @IBAction func dismissTapAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension SortBy: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sortByListArr.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 2))
        headerView.backgroundColor = UIColor.white
        let label = UILabel(frame: CGRect(x: 5, y: 10, width: sortByTableView.bounds.width-10, height: 25))
        label.text = "SORT BY".localized
        label.font = UIFont(name: appFont, size: 15.0)
        label.textAlignment = .center
        label.textColor = UIColor.colorWithRedValue(redValue: 32, greenValue: 90, blueValue: 235, alpha: 1)
        headerView.addSubview(label)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SortByCell", for: indexPath) as! SortByTableViewCell
        let item = sortByListArr[indexPath.row]
        if let txt = item.Text {
            cell.sortNameLbl.text = txt as String
        }
        //configure you cell here.
        if checked[indexPath.row] == false {
            cell.accessoryType = .none
            cell.sortNameLbl.textColor = UIColor.black
        }else if checked[indexPath.row] {
            cell.accessoryType = .checkmark
            cell.sortNameLbl.textColor = UIColor.colorWithRedValue(redValue: 32, greenValue: 90, blueValue: 235, alpha: 1)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
            for (index,_) in checked.enumerated() {
                checked[index] = false
            }
            checked[indexPath.row] = true
            cell.accessoryType = .checkmark
        }
        let item = sortByListArr[indexPath.row]
        self.SDelegate?.SortByTestMethod(sortByTableView: tableView, indexPath: indexPath, sortingType: item.Text as String? ?? "")
        self.SDelegate?.sortedCheckMarkArray(checked: self.checked)
        self.dismissTapAction(tableView)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
}


class SortByTableViewCell: UITableViewCell {
    @IBOutlet var sortNameLbl: UILabel! {
        didSet {
            self.sortNameLbl.font = UIFont(name: appFont, size: 15.0)
            self.sortNameLbl.text = self.sortNameLbl.text?.localized
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
